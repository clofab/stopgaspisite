<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #f8f9fb;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
	float: left;
}

h1 {
	color: #0c6aa7;
	font-size: 30px;
	margin: 0 0 14px 0;
	padding: 40px 0px 0px 0px;
	text-transform: uppercase;
	font-family: "Open Sans",sans-serif;
	font-weight: 900;
	float: left;
	width:100%;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
    margin: 5% 10%;
    box-shadow: 0px 3px 5px rgba(38,20,62,0.08);
    background: #FFF;
    padding: 35px;
    border-radius: 0 0 5px 5px;
    border-top: 5px solid #b92126;
    float: left;
}

p {
	margin: 0;
	float: left;
	width:100%;
}
.btn {
    padding: 15px 30px;
    border-radius: 30px;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 1px;
    font-size: 12px;
    margin-bottom: 35px;
    margin-top: 35px;
    transition: all 0.5s ease 0s;
    border: solid 2px transparent;
    background-image: -webkit-linear-gradient(top, #337ab7 0, #265a88 100%);
    background-image: -o-linear-gradient(top, #337ab7 0, #265a88 100%);
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #337ab7), to(#265a88));
    background-image: linear-gradient(to bottom, #337ab7 0, #265a88 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff265a88', GradientType=0);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
    border-color: #245580;
    color: #fff;
    text-decoration:none;
    transition: all 0.2s ease;
    float: left;
}
.btn:hover{
    transition: all 0.2s ease;
    border: solid 2px #fff;
    color: #fff;
    background: rgba(29,39,35,0.27) !important;
    padding-left: 38px;
    padding-right: 25px;	
}
</style>
</head>
<body>
	<div id="container">
		<img src="<?php echo(SITE.'/img/logo.png'); ?>" alt="Logo-Stop-Gaspi" class="logo">
		<h1>ERREUR 404. La page demandée n'a pas été trouvée.</h1>
		<p>La page demandée est introuvable. Elle a peut-être été supprimée ou déplacée. N'hésitez pas à nous contacter pour nous le signaler.</p>
		<br>
		<a href="/" class="btn">Accéder à la page d'accueil</a>
		<br>
	</div>
</body>
</html>