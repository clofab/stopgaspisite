<section class="domaine_energetique_intro">
	<div class="container">
		<div class="row">
			<h1>Domaine énergétique concerné : <span><?= $typeEnergie->libelleTypeEnergie; ?></span></h1>
			<p><?= $typeEnergie->accrocheTypeEnergie; ?></p>
		</div>
	</div>
</section>
<section class="domaine_energetique_infos">
	<div class="container">
		<div class="row">
			<div class="col-md-8 description no-tiny">
				<?= $typeEnergie->contenuTypeEnergie; ?>
			</div>
			<div class="col-md-4">
				<div class="row partage">
					<h3 class="pull-left"><i class="glyphicon glyphicon-bullhorn pull-left"></i>Je partage l'info : </h3>

					<a class="share-btn share-btn-branded share-btn-twitter pull-right"
					href="https://twitter.com/share?url=<?=URL;?>/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"
					title="Partager sur Twitter">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Twitter</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-facebook pull-right"
					href="https://www.facebook.com/sharer/sharer.php?u=<?=URL;?>/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"
					title="Partager sur Facebook">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Facebook</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-googleplus pull-right"
					href="https://plus.google.com/share?url=<?=URL;?>/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"
					title="Partager sur Google+">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Google+</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-linkedin pull-right"
					href="https://www.linkedin.com/shareArticle?mini=true&url=<?=URL;?>/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"
					title="Partager sur LinkedIn">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">LinkedIn</span>
					</a>


				</div>
				<div class="row liste-fiches">
					<h3>Fiches conseils</h3>
					<div class="weswap-current">
						<?php
	                        foreach ($typeEnergie->fiches as $fiche){
	                            $dateAffichage = strtotime($fiche->dateCreation);
	                            $jour=date("d",$dateAffichage);
	                            $mois=date("m",$dateAffichage);
	                            $annee=date("y",$dateAffichage);
	                            ?>
	                            <a href="<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>" class="mini-fiche">
									<h4><?=$fiche->titre?></h4>
									<p><?= substr(strip_tags($fiche->description),0,150)."..." ?></p>
									<p class="auteur">Ecrit le <span><?= $jour;?>/<?= $mois;?>/<?= $annee;?></span> par <span><?= $fiche->prenomUtilisateur; ?> <?= $fiche->nomUtilisateur; ?></span></p>
								</a>
	                        <?php
	                    }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="domaine_energetique_videos">
	<div class="container">
		<div class="row">
			<div class="col-md-3 filtre">
				<h3 data-toggle="dropdown">Filtre de tri<i class="glyphicon glyphicon-chevron-down pull-right"></i></h3>
				<ul class="animated fadeInDown dropdown-menu">
					<li>
						<a href="#" id="sort-date">Par date de publication</a>
					</li>
					<li>
						<a href="#" id="sort-alpha">Par ordre alphabétique</a>
					</li>
					<li>
						<a href="#" id="sort-author">Par auteur</a>
					</li>
				</ul>
			</div>
			<div class="col-md-9 title">
				<h3>Vidéos en rapport avec le domaine énergétique : <span><?= $typeEnergie->libelleTypeEnergie; ?></span></h3>
			</div>
		</div>
		<div class="row video-list" data-order="0">
			<?php
                foreach ($typeEnergie->videos as $video){
                    $dateAffichage = strtotime($video->dateCreation);
                    $jour=date("d",$dateAffichage);
                    $mois=date("m",$dateAffichage);
                    $annee=date("y",$dateAffichage);
                    ?>
                    <a href="<?=URL;?>/Front/video/<?= $video->idVideo; ?>" class="video_container" data-date="<?=$dateAffichage?>" data-author="<?=$video->nomUtilisateur?>">
						<div class="infos">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" width="560" height="315" src="<?= $video->urlVideo;?>" frameborder="0" allowfullscreen></iframe>
							</div>
							<h4 class="title-video"><?= substr(strip_tags($video->titreVideo),0,30)?><?php if (strlen($video->titreVideo) > 30) echo "..." ?></h4>
							<p style="min-height:64px;"><?= substr(strip_tags($video->descriptionVideo),0,150) ?><?php if (strlen($video->descriptionVideo) > 150) echo "..." ?></p>
							<p class="auteur">Posté par : <?= $video->prenomUtilisateur; ?> <?= $video->nomUtilisateur; ?>, le <?= $jour;?>/<?= $mois;?>/<?= $annee;?></p>
						</div>
					</a>
                <?php
            }?>
		</div>
	</div>
</section>