	
	<section class="header">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<img src="<?php echo(SITE.'/img/logo.png'); ?>" alt="Logo-Stop-Gaspi" class="logo">
				</div>	
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<?php if($this->session->userdata('Entreprise') == true){ ?>
						<div class="col-lg-12 col-xs-12 connexion"><?= $this->session->userdata('nomEntreprise')?> /<a href="<?= URL."/Front/espace_entreprise";?>"> Mon compte </a> / <a href="<?= URL."/Front/deconnexion";?>">Se déconnecter</a></div>
					<?php } else{ ?>
						<div class="col-lg-12 col-xs-12 connexion"><a href="<?= URL."/Front/connexion";?>">Connexion</a> / <a href="<?= URL."/Front/inscription";?>">Inscription</a></div>
					<?php } ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="contact-rapide" data-toggle="dropdown">
							<span class="icon-conseiller">
								<?php echo file_get_contents(SITE."/img/icon-conseiller.svg"); ?>
							</span>
							<i class="glyphicon glyphicon-chevron-down pull-right"></i>
							<span class="title">Je prends contact</span>
							<span class="subtitle">Avec un conseiller</span> 
						</div>
						<ul class="animated fadeInDown dropdown-menu">
							<li>
								<a href="<?= URL."/Front/priseRdv"; ?>">
									<span class="title">Prendre rendez vous</span>
									<span class="text">Dans une préfecture près de chez moi</span>
								</a>
							</li>
							<li>
								<a href="<?= URL."/Front/visioconference"; ?>">
									<span class="title">Démarrer une visioconférence</span>
									<span class="text">Avec un conseiller</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section class="navigation">
		<div class="container">
		    <nav role="navigation">
		        <div class="navbar-header">
		            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		                <span>MENU</span>
		            </button>
		            <a href="#" class="navbar-brand"></a>
		        </div>
		        <div id="navbarCollapse" class="collapse navbar-collapse">
		            <ul class="nav navbar-nav">
		                <li class="active"><a href="/">Accueil</a></li>
		                <li><a href="<?= URL."/Front/actualites";?>">Actualités</a></li>
		                <li><a href="<?= URL."/Front/fiches";?>">Fiches conseils</a></li>
		                <li class="dropdown">
		                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Informations <i class="glyphicon glyphicon-chevron-down"></i></a>
		                    <ul role="menu" class="animated fadeInDown dropdown-menu">
		                    	
							<?php	
                            foreach ($pagesInfos as $pagesInfo){
                            	//	AFFICHAGE DES PAGES INFORMATIONS
                                if($pagesInfo->etatActu == 1){ ?>
			                        <li><a href="<?= URL."/Front/page_info/".$pagesInfo->idActu; ?>"><?= $pagesInfo->titre; ?></a></li>  <?php 
	                       		} 
	                         
	                         } ?>
		                        
		                        
		                    </ul>
		                </li>
		                <li class="dropdown">
		                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Domaines énergétiques <i class="glyphicon glyphicon-chevron-down"></i></a>
		                    <ul role="menu" class="animated fadeInDown dropdown-menu">
		                        <li><a href="<?= URL."/Front/domaineEnergie/2";?>">Carburants</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/1";?>">Thermique</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/3";?>">Impression et consommables</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/5";?>">Electricité</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/4";?>">Recyclage électronique</a></li>
		                    </ul>
		                </li>
		            </ul>
		        </div>
		    </nav>		
		</div>
	</section>