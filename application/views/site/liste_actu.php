<section class="newslist">
	<div class="container">
		<div class="row">
			<div class="col-md-12 page">
				<h2>StopGaspi : liste des <span>actualités</span></h2>
				<?php
                foreach ($actus as $actu){
                    
                    $mois_fr = Array("", "Janv.", "Févr.", "Mars", "Avr.", "Mai", "Juin", "Jui.", "Août","Sept.", "Oct.", "Nov.", "Déc.");
                    $dateAffichage = strtotime($actu->dateCreation);
                    $jour=date("d",$dateAffichage);
                    $mois=intval(date("m",$dateAffichage));
                    $mois = $mois_fr[$mois];
                    ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 actu-big">
        					<a href="<?= URL."/Front/actualite/".$actu->idActu; ?>">
        						<div class="actu-box">
        							<div class="news-bg" style="background:url('<?= IMG.'actu/'.$actu->imageActu?>')"></div>
        							<span class="shadow"></span>
        							<div class="infos">
        								<div class="col-lg-2 col-sm-4 col-xs-3 date">
        									<span><?= $jour; ?></span>
        									<span><?= $mois; ?></span>
        								</div>	
        								<div class="col-lg-10 col-xs-9 auteur"><i class='glyphicon glyphicon-user'></i> <?= $actu->prenomUtilisateur; ?> <?= $actu->nomUtilisateur; ?></div>
        							</div>
        							<div class="col-lg-12 col-sm-12 col-xs-12 texte">
        								<h4><?= $actu->titre; ?></h4>	
        								<p><?= substr(strip_tags($actu->description),0,220) ?><?php if (strlen($actu->description) > 220) echo "..." ?></p>	
        								<a href="#"><i class='glyphicon glyphicon-chevron-right'></i>Lire la suite</a>
        							</div>
        						</div>
        					</a>
        				</div>
                        <?php
                } ?>
                
                <div class="col-md-5 col-md-offset-5"><?= $this->pagination->create_links(); ?></div>
				
			</div>
		</div>
	</div>
</section>