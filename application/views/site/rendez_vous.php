
<style type="text/css">
	.fc-scroller.fc-time-grid-container{height:530px!important;}
</style>

<section class="defaut">
	<div class="container">
		<div class="row page">
		     
			<h1>Prendre <span>Rendez-vous</span> avec un conseiller près de chez vous.</h1>
			<p>Sélectionnez une préfecture proche de chez vous et choisissez dans le calendrier la date et l'heure qui vous convient parmis les dates disponibles. Un e-mail de confirmation vous sera envoyé après validation du formulaire.</p>
			<h4>Sélectionnez une préfecture :</h4>
	        <select id="villeAnnexe">
	            <?php   // TRIEZ DANS L'ORDRE DES PREFECTURES LES PLUS PROCHES DE L'ENTREPRISE
	            foreach ($annexes as $annexe){ ?>
	                <option value="<?=$annexe->idAnnexe ?>"><?=$annexe->nomAnnexe ?></option>
	            <?php } ?>
	        </select>
			<h4>Choisissez la date et l'heure du rendez-vous parmis les conseillers disponibles :</h4>
            <div id="calendar" class=""></div>
        </div>
	</div>
</section>


<!-- Modal Confirm Rendez vous -->
<div class="modal fade" id="cadreRdv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title" id="myModalLabel">Les informations de rendez-vous</h2>
			</div>
			
			<div class="modal-body">
				<div class="row">
				    <div class="alert alert-danger hide" role="alert" id="alertRdv">Vous avez déjà un rendez vous de prévu le : <span id="dateProchainRdv"></span></div>
	        		<div class="col-md-6">
		                <p> Votre conseiller : <label id="conseiller"></label></p>
		                <p> <b>Date de rendez vous</b> : le <label id="dateRdv"></label></p>
		                <p> <b>Heure de rendez-vous</b> : De : <label id="heureDeb"></label> à <label id="heureFin"></label></p>
	                </div>
	            	<div class="col-md-6">
	
			            <p><b>Choisissez une heure de rendez-vous :</b></p>
			            <select id="heureDispo"></select><br>
			        </div>
		        </div>
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-default" id="validRdv">Valider le rendez-vous</button>
			    <button class='btn' data-dismiss="modal">Fermer</button>
			</div>

		</div>
	</div>
</div>

<!-- Modal Confirm Rendez vous -->
<div class="modal fade" id="confirmRdv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title" id="myModalLabel">Confirmation de rendez vous</h2>
			</div>
			
			<div class="modal-body">
				<p>Demande prise en compte, nous traitons votre demande, vous allez reçevoir un email de confirmation</p>
			</div>
			
			<div class="modal-footer">
			    <button class='btn' data-dismiss="modal">Fermer</button>
			</div>

		</div>
	</div>
</div>
