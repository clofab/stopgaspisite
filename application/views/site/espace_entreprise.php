<section class="defaut">
	<div class="container">
		<div class="row page">
            <?php
             if(isset($msgError)){ // SI EMAIL DEJA UTILISER 	?>
                    <div class="alert alert-danger" role="alert"> <?= $msgError ?>  </div>
            <?php } ?>

            <h2>Espace entreprise</h2>
            <div class="col-md-6 col-xs-12">
                <form class="" action="<?=site_url()."/Front/updateEntreprise"?>" method="POST" id="formEntreprise" >
                    <div class="form-group">
                        <label class="col-md-12 control-label ">Entreprise représentée</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" value="<?= $entreprise->nomEntreprise ?>" name="nomEntreprise"  maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">Interlocuteur</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" value="<?= $entreprise->interlocuteur ?>" name="interlocuteur"  maxlength="100">
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="col-md-12 control-label">Téléphone</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" value="<?= $entreprise->telEntreprise ?>" name="telEntreprise"  maxlength="10">
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="col-md-12 control-label">Email</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" value="<?= $entreprise->emailEntreprise ?>" name="emailEntreprise"  maxlength="30">
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="col-md-12 control-label">Mot de passe</label>
                        <div class="col-md-12">
                            <input type="password" class="form-control" value="<?= $entreprise->mdpEntreprise ?>" name="mdp"  maxlength="30">
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="col-md-12 control-label">Confirmation Mot de passe</label>
                        <div class="col-md-12">
                            <input type="password" class="form-control" value="<?= $entreprise->mdpEntreprise ?>" name="mdp2"  maxlength="30">
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="col-md-12 control-label">Adresse </label>
                        <div class="col-md-12">
                            <input id="address" type="text" class="form-control" value="<?= $entreprise->adresseEntreprise ?>" name="adresseEntreprise">
                        </div>
                    </div>
                    <div class="form-group hide">
                        <label class="col-md-12 control-label"></label>
                        <div class="col-md-12">
                            <div id="geoSearch"></div>
                        </div>
                    </div>
            
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="hidden" id="needMap" value="true">
                            <input name="latitude" id="latitude" type="hidden"/>
                            <input name="longitude" id="longitude" type="hidden"/>
                            <button class="btn btn-primary" role="button" name="saveEntreprise" type="submit">Enregistrer</button>
                            <button class="btn" role="button" type="button" onclick="window.location.href='/'">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
            <?php 
                date_default_timezone_set('Europe/Paris');
                $dateNow = date( 'Y-m-d H:i:s');
            ?>
            
            <div class="col-md-6 col-xs-12">
                
                <h3>Prochains rendez-vous</h3>
    
                <table class="table table-striped table-hover tabRdv" style="width:100%">
                    <thead>
                        <tr>
                    	  	<th>Conseiller</th>
                            <th>Date - Heure</th>
                            <th>Annexe</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php 
                   
                    foreach($entreprise->rdv as $rdvConseiller){ 
                  
                        if($rdvConseiller->etatRdv == 1 or $rdvConseiller->etatRdv == 2){
    
                            $dateRdv = explode('-',substr($rdvConseiller->plageHorraire, 0, 10));
                            $heureRdv = substr($rdvConseiller->heure_deb, 0, 5);
                            ?>
    
                            <tr id="<?= $rdvConseiller->idRdv ?>">
                                <td><?= $rdvConseiller->nomUtilisateur." ".$rdvConseiller->prenomUtilisateur ?></td>
                                <td><?= $dateRdv[2]."/".$dateRdv[1]."/".$dateRdv[0]." ".$heureRdv ?></td>
                                <td><?= $rdvConseiller->nomAnnexe ?></td>
                            </tr><?php
                        }
                    } ?>
    
                    </tbody>
                </table>

                
                <!-- RENDEZ VOUS ARCHIVER-->
                <h3>Rendez-vous archivé</h3>
                <table class="table table-striped table-hover tabRdv" style="width:100%">
                    <thead>
                        <tr>
                            <th>Conseiller</th>
                            <th>Date - Heure</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                       
                         foreach($entreprise->rdv as $rdvConseiller){ 
                      
                            if($rdvConseiller->etatRdv == 0 ){
    
                            $dateRdv = explode('-',substr($rdvConseiller->plageHorraire, 0, 10));
                            $heureRdv = substr($rdvConseiller->heure_deb, 0, 5);
                            ?>
    
                            <tr id="<?= $rdvConseiller->idRdv ?>">
                                <td><?= $rdvConseiller->nomUtilisateur." ".$rdvConseiller->prenomUtilisateur ?></td>
                                <td><?= $dateRdv[2]."/".$dateRdv[1]."/".$dateRdv[0]." ".$heureRdv ?></td>
                                <td data-cr="<?=htmlspecialchars($rdvConseiller->compte_rendu) ?>"><a href="#" id="openCR">Consulter compte rendu</a></td>
                            </tr><?php
                            }
                        } ?>

                    </tbody>
                </table>
            </div>
		</div>
	</div>
</section>

<!-- MODAL COMPTE RENDU -->
<div class="modal fade modalRdv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleRdv">Compte rendu de rendez vous</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="form-group ">
                        <div class="col-md-12">
                           <textarea id="compte_rendu" class="form-control" maxlength="500" disabled></textarea>
                       </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" id="annulModalRdv">Fermer</button>
            </div>

        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFJeeQ-EdkdJ3XoWltrvOLYjRm4sBA5bQ" type="text/javascript"></script>