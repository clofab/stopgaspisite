

<section class="domaines-energetiques">
	<div class="container">
		<div class="row">
			<h1>Toutes nos <span>fiches conseil</span> classées par Domaines énergétiques</h1>
			<div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php $i = 0;
                    foreach ($typeEnergies as $typeEnergie){        // ON RECUPERE LES TYPES D'ENERGIE ?>
                        <li role="presentation" class="<?php if ($i == 0) echo "active" ?>">
                            <a data-toggle="tab" href="#cat_<?=$typeEnergie->idTypeEnergie?>" aria-controls="<?=$typeEnergie->libelleTypeEnergie?>">
                                <span class="icon">
        							<?php echo file_get_contents(SITE."/img/".$typeEnergie->imgTypeEnergie); ?>
        						</span>
                                <?=$typeEnergie->libelleTypeEnergie?>
                            </a>
                        </li>
                    <?php $i++;
                    } ?>
                </ul>
                <div class="tab-content">
                    <?php $i = 0;
                        foreach ($typeEnergies as $typeEnergie){        // ON RECUPERE LES TYPES D'ENERGIE ?>
                            
                            <div role="tabpanel" id="cat_<?=$typeEnergie->idTypeEnergie?>" class="tab-pane fade in <?php if ($i == 0) echo "active" ?>">
                                
                                <div class="row">
                                      
            			    	    <div class="ficheslist">
							            <div class="row page">
                                            <div class="col-xs-12">
                                                 <i class='glyphicon glyphicon-search searchicon'></i>
                                                 <input type="text" placeholder="Rechercher une fiche" name="filter<?=$typeEnergie->idTypeEnergie?>" class="filterfiche"/></input>
                                            </div>
            			    			<?php
            			    			    $mois_fr = Array("", "Janv.", "Févr.", "Mars", "Avr.", "Mai", "Juin", "Jui.", "Août","Sept.", "Oct.", "Nov.", "Déc.");
                                            foreach ($typeEnergies[$i]->fiche as $fiche){
                                                  
                                                $dateAffichage = strtotime($fiche->dateCreation);
                                                $jour=date("d",$dateAffichage);
                                                $mois=intval(date("m",$dateAffichage));
                                                $mois = $mois_fr[$mois];

                                                ?>
                                                
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 fiche-big">
                									<a href="<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>">
                										<div class="fiche-box">
                
                											<div class="infos row">
                												<div class="col-lg-2 col-sm-4 col-xs-3 date">
                													<span><?=$jour?></span>
                                                                    <span><?=$mois?></span> 
                												</div>
                												
                												<div class="col-lg-10 col-sm-8 col-xs-7">
                													<div class="row fiche-infos"><i class='glyphicon glyphicon-user'></i> <?=$fiche->nomUtilisateur." ".$fiche->prenomUtilisateur?></div>
                													<div class="row fiche-infos"><i class='glyphicon glyphicon-tag'></i> 
                    													<?php 
                    													// ON AFFICHE LA LISTE DES TYPES ENERGETIQUE RELIÉ
                    													foreach ($fiche->typeEnergetiques as $typeFiche) {
                    													    $separator ='|';
                    													    if ($typeFiche === end($fiche->typeEnergetiques)) $separator = "";
                    													    echo $typeFiche->libelleTypeEnergie.$separator;
                    													}?>
                													</div>
                												</div>
                                                            </div>
                                                            
                                                            <div class="col-lg-12 col-sm-12 col-xs-12 texte">
                                                                <h4 class="title-fiche"><?=$fiche->titre?></h4>
                									        	<a href="<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>"><i class='glyphicon glyphicon-chevron-right'></i>Lire la suite</a>
                											</div>

                                                        </div>
                                                    </a>
                                                </div>
                                            <?php
                                        }?>
                                    </div>
                                    
            			    	</div>
                            </div>
                              <div class="jplist-panel">
  
                                        <div 
                                             class="jplist-pagination" 
                                             data-control-type="pagination" 
                                             data-control-name="paging" 
                                             data-control-action="paging"
                                             data-items-per-page="1">
                                        </div>
                                   
                                    </div>
                        </div>
                        <?php $i++;
                    } ?>
                    </div>
                </div>  
			 </div>
		</div>
	</div>
</section>

