<footer>
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 col-md-5 col-sm-3 col-xs-12">
    				<div class="row">
    					<div class="col-lg-6 col-md-6 col-xs-12">
    						<ul>
    							<li><span><b>Domaines énergétiques</b></span></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/2";?>">Carburants</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/1";?>">Thermique</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/3";?>">Impression et consommables</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/5";?>">Electricité</a></li>
		                        <li><a href="<?= URL."/Front/domaineEnergie/4";?>">Recyclage électronique</a></li>
    						</ul>
    					</div>
    					<div class="col-lg-6 col-md-6 col-xs-12">
    						<ul>
    							<li><span><b>Liens rapides</b></span></li>
    							<li><a href="<?= URL."/Front/priseRdv" ?>">Prendre rendez-vous</li></a>
    							<li><a href="<?= URL."/Front/visioconference" ?>">Visioconférence</li></a>
    							<li><a href="<?= URL."/Front/actualites" ?>">Actualités</li></a>
    						</ul>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-6 col-md-7 col-sm-9 col-xs-12 app-dl">
    				<h2 class="title-up">
    					<span>Téléchargez</span>
    					<span>L'application smartphone</span>
    				</h2>
    				<div class="row">
    					<div class="col-lg-7">
    						<div class="row">
    							<div class="col-lg-6 store">
    								<img src="<?php echo(SITE.'/img/apple-store.png'); ?>" alt="IOS Application">
    							</div>
    							<div class="col-lg-6 store">
    								<img src="<?php echo(SITE.'/img/google-play.png'); ?>" alt="Android Application">
    							</div>
    						</div>
    						<div class="col-lg-12">
    							<p><i class="glyphicon glyphicon-heart"></i> Calculatrice de défiscalisation</p>
    							<p><i class="glyphicon glyphicon-heart"></i> Visioconférence avec un conseiller</p>
    							<p><i class="glyphicon glyphicon-heart"></i> Itinéraire vers les préfectures</p>
    						</div>
    					</div>
    					<div class="col-lg-5 smartphone">
    						<img src="<?php echo(SITE.'/img/application.png'); ?>">
    					</div>
    				</div>
    			</div>	
    		</div>	
    	</div>
    </footer>