<section class="page_photo">
	<div class="bg_flou" style="background:url('http://cdn-lejdd.ladmedia.fr/var/lejdd/storage/images/media/images/societe/la-centrale-francaise-du-bugey/12436055-1-fre-FR/La-centrale-francaise-du-Bugey.jpg')"></div>
	<div class="container bg_clear" style="background:url('http://cdn-lejdd.ladmedia.fr/var/lejdd/storage/images/media/images/societe/la-centrale-francaise-du-bugey/12436055-1-fre-FR/La-centrale-francaise-du-Bugey.jpg')">
		<h1><?= $video->titreVideo; ?></h1>
	</div>
</section>
<section class="page_infos">
	<div class="container">
		<div class="row">
			<div class="col-md-8 description">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" width="560" height="315" src="<?= $video->urlVideo;?>" frameborder="0" allowfullscreen></iframe>
				</div>
				<?php 
				$dateAffichage = strtotime($video->dateCreation);
                $jour=date("d",$dateAffichage);
                $mois=date("m",$dateAffichage);
                $annee=date("y",$dateAffichage);
                ?>
				<h4><?=$video->titreVideo;?></h4>
				<p><?= substr(strip_tags($video->descriptionVideo),0,150)."..." ?></p>
				<p class="auteur">Posté par : <?= $video->prenomUtilisateur; ?> <?= $video->nomUtilisateur; ?>, le <?= $jour;?>/<?= $mois;?>/<?= $annee;?></p>
				<p class="domaines">Domaines : 
					<?php foreach($video->typeEnergie as $typeEnergie){?>
						<a href="<?=URL;?>/index.php/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"><?= $typeEnergie->libelleTypeEnergie; ?></a>  
					<?php }?>
				</p>
			</div>
			<div class="col-md-4">
				<div class="row partage">
					<h3 class="pull-left"><i class="glyphicon glyphicon-bullhorn pull-left"></i>Je partage l'info : </h3>

					<a class="share-btn share-btn-branded share-btn-twitter pull-right"
					href="https://twitter.com/share?url=<?=URL;?>/index.php/Front/video/<?= $video->idVideo; ?>"
					title="Partager sur Twitter">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Twitter</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-facebook pull-right"
					href="https://www.facebook.com/sharer/sharer.php?u=<?=URL;?>/index.php/Front/video/<?= $video->idVideo; ?>"
					title="Partager sur Facebook">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Facebook</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-googleplus pull-right"
					href="https://plus.google.com/share?url=<?=URL;?>/index.php/Front/video/<?= $video->idVideo; ?>"
					title="Partager sur Google+">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Google+</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-linkedin pull-right"
					href="https://www.linkedin.com/shareArticle?mini=true&url=<?=URL;?>/index.php/Front/video/<?= $video->idVideo; ?>"
					title="Partager sur LinkedIn">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">LinkedIn</span>
					</a>


				</div>
				<div class="row liste-actus">
					<h3>Dernières actualités publiées :</h3>
					<?php
	                	foreach ($actus as $actu){
	                        $dateAffichage = strtotime($actu->dateCreation);
	                        $jour=date("d",$dateAffichage);
                            $mois=date("m",$dateAffichage);
                            $annee=date("y",$dateAffichage);
                            ?>
                            <a href="#" class="mini-actu">
								<h4><?=$actu->titre?></h4>
								<p><?= substr(strip_tags($actu->description),0,150)."..." ?></p>
								<p class="auteur">Ecrit le <span><?= $jour;?>/<?= $mois;?>/<?= $annee;?></span> par <span><?= $actu->prenomUtilisateur; ?> <?= $actu->nomUtilisateur; ?></span></p>
							</a>
	                        <?php
	            		} ?>

					<button class="btn center-block">Voir toutes les actualités</button>
				</div>
			</div>
		</div>
	</div>
</section>