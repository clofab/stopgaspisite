<section class="defaut">
	<div class="container">
		<div class="row">
			<div class="col-md-12 page">
				<div class="col-md-9 col-lg-10" id="homeVisio">
                	<h1>Visioconférence</h1>
		            <p>Vous pouvez contacter ici un des conseillers afin d'en savoir plus sur les nombreux avantages de Stop Gaspi.</p>
		            <button class="btn btn-default">
		            	<span class="icon-conseiller">
		            		<?php echo file_get_contents(SITE."/img/icon-conseiller.svg"); ?>
						</span>
						Démarrer une visioconférence
					</button>
                </div>
                <div class="col-md-9 col-lg-10" id="waitVisio" style="display:none;">
                	<h1>Visioconférence</h1>
	                <div id="errorVisio"></div>
					<i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>
                    <span>Veuillez patienter, un conseiller va prendre votre appel <img src="<?php echo(SITE.'/img/wait.gif'); ?>"></span>
                </div>
                <div id="cadreVisio" style="display:none;">
                	<h1>Visioconférence</h1>
                	<div class="row boxed">
                	    <div class="col-sm-8"><h3 id="nomVisio"></h3><div id="remoteVideo" style="width:100%;"></div></div>
                	    <div class="col-sm-4"><h3>Votre caméra</h3><video id="localVideo" autoplay muted style="width:100%;"></video></div>
                	    <button class="btn center-block">Raccrocher</button>
                	</div>
                	
                
                </div>
			</div>
		</div>
	</div>
</section>
    