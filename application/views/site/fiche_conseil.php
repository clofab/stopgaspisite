
<section class="domaine_energetique_intro">
	<div class="container">
		<div class="row">
			<h1>Domaine énergétique concerné : 
			<?php foreach($fiche->typeEnergie as $typeEnergie){
				$separator ='|';
		    	if ($typeEnergie === end($fiche->typeEnergie)) $separator = "";?>

				<span><a style="color:white" href="<?= URL."/Front/domaineEnergie/".$typeEnergie->idTypeEnergie?>"><?= $typeEnergie->libelleTypeEnergie; ?></a></span><?=$separator ?>
			<?php } ?>
			
			</h1>
		</div>
	</div>
</section>

<section class="fiche_conseils_infos">
	<div class="container">
		<div class="row">
			<div class="col-md-8 description no-tiny">
				<h2><span>Fiche conseil :</span><?= $fiche->titre; ?></h2>
				<p><?= $fiche->description; ?></p>
				<br>
				<a href="<?=URL;?>/Generate/pdf_fiche/<?= $fiche->idFiche; ?>" target="_blank" class="btn btn-primary col-md-offset-4" role="button">Télécharger la fiche</a>
			</div>
			<div class="col-md-4">
				<div class="row partage">
					<h3 class="pull-left"><i class="glyphicon glyphicon-bullhorn pull-left"></i>Je partage l'info : </h3>

					<a class="share-btn share-btn-branded share-btn-twitter pull-right"
					href="https://twitter.com/share?url=<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>"
					title="Partager sur Twitter">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Twitter</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-facebook pull-right"
					href="https://www.facebook.com/sharer/sharer.php?u=<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>"
					title="Partager sur Facebook">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Facebook</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-googleplus pull-right"
					href="https://plus.google.com/share?url=<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>"
					title="Partager sur Google+">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Google+</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-linkedin pull-right"
					href="https://www.linkedin.com/shareArticle?mini=true&url=<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>"
					title="Partager sur LinkedIn">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">LinkedIn</span>
					</a>


				</div>
				<div class="row rss">
					<h3 class="pull-left"><i class="glyphicon glyphicon-info-sign pull-left"></i>Je me tiens informé : </h3>

					<a class="rss-btn pull-right"
					href="<?=URL;?>/Rss/rss_fiche"
					title="RSS"  type="application/rss+xml">
						<span class="share-btn-text-sr"><img src="<?=SITE;?>/img/rss.png">Flux RSS</span>
					</a>

				</div>
				<div class="row liste-fiches">
					<h3>Autres fiches : </h3>
					<div class="weswap-current">
					<?php
	                	foreach ($fiches as $fich){
	                        $dateAffichage = strtotime($fich->dateCreation);
	                        $jour=date("d",$dateAffichage);
                            $mois=date("m",$dateAffichage);
                            $annee=date("y",$dateAffichage);
                            ?>
                            <a href="<?=URL;?>/Front/fiche/<?= $fich->idFiche; ?>" class="mini-fiche">
								<h4><?=$fich->titre?></h4>
								<p><?= substr(strip_tags($fich->description),0,150)."..." ?></p>
								<p class="auteur">Ecrit le <span><?= $jour;?>/<?= $mois;?>/<?= $annee;?></span> par <span><?= $fich->prenomUtilisateur; ?> <?= $fich->nomUtilisateur; ?></span></p>
							</a>
	                        <?php
	            		} ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>