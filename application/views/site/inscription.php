<section class="defaut">
	<div class="container">
		<div class="row page">
            <?php
             if(isset($msgError)){ // SI EMAIL DEJA UTILISER 	?>
                    <div class="alert alert-danger" role="alert"> <?= $msgError ?>  </div>
            <?php } ?>
            <h2>Enregistrez votre entreprise</h2>
            <p>L'enregistrement de votre entreprise vous donne accès à la visioconférence et vous permet de prendre rendez vous avec un conseiller en préfecture.</p>
            <form class="form-horizontal" action="<?=site_url()."/Front/validInscription"?>" method="POST" id="formEntreprise" >
              
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Entreprise représentée</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="text" class="form-control" placeholder="Nom de l'entreprise" name="nomEntreprise"  maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Interlocuteur</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="text" class="form-control" placeholder="Nom et Prénom" name="interlocuteur"  maxlength="100">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Téléphone</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="text" class="form-control" placeholder="Téléphone" name="telEntreprise"  maxlength="10">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Email</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="text" class="form-control" placeholder="Email" name="emailEntreprise"  maxlength="30">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Mot de passe</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="password" class="form-control" placeholder="Choisissez un mot de passe" name="mdp"  maxlength="30">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Confirmation Mot de passe</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="password" class="form-control" placeholder="Confirmation du mot de passe" name="mdp2"  maxlength="30">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Adresse </label>
                    <div class="col-lg-3 col-md-5">
                        <input id="address" type="text" class="form-control" placeholder="Adresse" name="adresseEntreprise">
                    </div>
                </div>
                <div class="form-group hide">
                    <label class="col-lg-2 col-md-3 control-label"></label>
                    <div class="col-lg-3 col-md-5">
                        <div id="geoSearch"></div>
                    </div>
                </div>
        
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <input type="hidden" id="needMap" value="true">
                        <input name="latitude" id="latitude" type="hidden"/>
                        <input name="longitude" id="longitude" type="hidden"/>
                        <button class="btn btn-primary" role="button" name="saveEntreprise" type="submit">Enregistrer</button>
                        <button class="btn" role="button" type="button" onclick="window.location.href='/'">Annuler</button>
                    </div>
                </div>
            </form>
		</div>
	</div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFJeeQ-EdkdJ3XoWltrvOLYjRm4sBA5bQ" type="text/javascript"></script>