<style>.calc-container input::-webkit-input-placeholder { color: white; }</style>

<section class="slider">
	<ul class="bxslider">
	   <?php
            foreach ($sliders as $slider) { ?>
                <li>
        			<img src="<?= SLIDER.'/'.$slider->imgSlider?>" />
        			<div class="slide-text-container">
        				<h2><?=$slider->titreSlider?></h2>
        				<p><?=$slider->descriptionSlider?></p>
        				<?php if((isset($slider->lienSlider))&&($slider->lienSlider!="")){?>
        				<a href="<?=$slider->lienSlider?>"><button class="btn"><?=$slider->nomLienSlider?></button></a>
        				<?php }?>
        			</div>
        		</li>
        <?php } ?>
	</ul>
	<div class="slider-controls">
		<div id="slider-next"></div>
		<div id="slider-prev"></div>
	</div>
</section>
<section class="intro">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 intro-texte">
			    <h1><?= $contenu->titreContenu ?></h1>
				<p><?= $contenu->descriptionContenu ?></p>
			</div>	
		    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 calc-container">
				<h2 class="title-up">
					<span>Calculez vos</span>
					<span>Exonérations fiscales</span>
				</h2>
				
				<div id="annee_prec">
				    
					<label for="">Montant de vos dépenses (en €) :<span>Année <?php echo date("Y", strtotime('-2 years')); ?></span></label>
					<input class="" type="text" placeholder="Carburant"></input>
					<input class="pull-right" type="text" placeholder="Thermique"></input>
					<input class="" type="text" placeholder="Consommable"></input>
					<input class="pull-right" type="text" placeholder="Éléctricité"></input>
					<input class="" type="text" placeholder="Recyclage"></input>
				</div>
					
				<div class="hide" id="annee_suiv">
					
                    <label for="">Montant de vos dépenses (en €) :<span>Année <?php echo date("Y", strtotime('-1 years')); ?></span></label>
					<input class="" type="text" placeholder="Carburant"></input>
					<input class="pull-right" type="text" placeholder="Thermique"></input>
					<input class="" type="text" placeholder="Consommable"></input>
					<input class="pull-right" type="text" placeholder="Éléctricité"></input>
					<input class="" type="text" placeholder="Recyclage"></input>
				</div>
				
				<div class="col-xs-12 block-center pull-right">
					<button class="btn changePageCalcul" id="calculetteSuiv">Suivant</button>
					<button class="btn hide changePageCalcul" id="calculettePrec">Précédent</button>
					<button class="btn" id="btnCalculer">Calculer</button>
				</div>
				
				<div id="msgCalculette"></div>
			</div>	
		</div>	
	</div>
</section>


<section class="domaines-energetiques">
	<div class="container">
		<div class="row">
			<h2><span>Domaines énergétiques</span> concernés</h2>
			<div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php $i = 0;
                    foreach ($typeEnergies as $typeEnergie){        // ON RECUPERE LES TYPES D'ENERGIE ?>
                        <li role="presentation" class="<?php if ($i == 0) echo "active" ?>">
                            <a data-toggle="tab" href="#<?=$typeEnergie->idTypeEnergie?>" aria-controls="<?=$typeEnergie->libelleTypeEnergie?>">
                                <span class="icon">
        							<?php echo file_get_contents(SITE."/img/".$typeEnergie->imgTypeEnergie); ?>
        						</span>
                                <?=$typeEnergie->libelleTypeEnergie?>
                            </a>
                        </li>
                    <?php $i++;
                    } ?>
                </ul>
                <div class="tab-content">
                    <?php $i = 0;
                        foreach ($typeEnergies as $typeEnergie){        // ON RECUPERE LES TYPES D'ENERGIE ?>
                
                            <div id="<?=$typeEnergie->idTypeEnergie?>" class="tab-pane fade in <?php if ($i == 0) echo "active" ?>">
                                <div class="row">
            			    		<div class="col-lg-6 col-md-9 col-sm-7 col-xs-12">
            			    			<p class="intro"><?=$typeEnergie->accrocheTypeEnergie?></p>
            			    			<h3>Dernières <span>fiches conseils</span></h3>
            			    			<?php
            			    			    $y = 0;
                                            foreach ($typeEnergies[$i]->fiche as $fiche){
                                                
                                                $mois_fr = Array("", "Janv.", "Févr.", "Mars", "Avr.", "Mai", "Juin", "Jui.", "Août","Sept.", "Oct.", "Nov.", "Déc.");
                                                $dateAffichage = strtotime($fiche->dateCreation);
                                                $jour=date("d",$dateAffichage);
                                                $mois=intval(date("m",$dateAffichage));
                                                $mois = $mois_fr[$mois];
                                                //var_dump($dateAffichage);
                                                if ($y == 4 ) break; ?>
                                                <div class="col-lg-12 col-xs-6 fiche">
                                                    <!-- CHANGER LIEN QUAND PAGE FICHE SERA FAITE -->
                                                    <a href="<?=URL;?>/Front/fiche/<?= $fiche->idFiche; ?>">
                                                        <div class="row">
                                                            <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 date">
                                                                <span><?=$jour?></span>
                                                                <span><?=$mois?></span>
                                                            </div>
                                                            <div class="col-lg-11 col-md-10 col-sm-12 col-xs-12 contenu">
                                                                <h4><?=$fiche->titre?></h4>
                                                                <p><?= substr(strip_tags($fiche->description),0,150)."..." ?></p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php
                                                $y++;
                                        }?>
                                    </div>
            			    		<div class="col-lg-6 col-md-3 col-sm-5 col-xs-12 videos">
            			    			<h3>Dernières <span>vidéos</span></h3>
            			    			<?php
            			    			   $y = 0;
                                           foreach ($typeEnergies[$i]->video as $video){  
                                           if ($y == 4 ) break; ?>
                                           <a href="<?=URL;?>/Front/video/<?= $video->idVideo; ?>">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe class="embed-responsive-item" width="560" height="315" src="<?=$video->urlVideo ?>" frameborder="0" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <h4><?=$video->titreVideo?></h4>
                                                        <p><?= substr(strip_tags($video->descriptionVideo),0,120)."..." ?></p>
                                                    </div>
                                                </div>
                                            </a>
                                           <?php
                                           $y++;
                                         }?>
            		    			</div>
            			    		<div class="row">
            			    			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            			    				<a href="<?= URL ?>/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie;?>"><button class="btn center-block">Plus d'infos sur ce domaine énergétique</button></a>
            			    			</div>
            			    		</div>
            			    	</div>
                            </div>
                            <?php $i++;
                        } ?>
                    </div>
			  

			 </div>
		</div>

	</div>
</section>
<section class="protection-environnement">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-8">
				<h3>Stop Gaspi lutte pour la protection de <span>L'environnement</span></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pharetra justo mi, vitae hendrerit nunc pellentesque at. Aliquam erat volutpat. Pellentesque commodo semper tellus ut vestibulum. Sed sem dolor, dapibus et ullamcorper a, placerat ac orci.</p>
				<div class="row">
					<div class="col-lg-12">
						<div id="owl-environnement" class="owl-carousel">
						    <?php

                            foreach ($pagesInfos as $pagesInfo){
                                if($pagesInfo->pageConseil == 1 And $pagesInfo->etatActu == 1){?>
                                    <div>
        								<div class="photo" style='background:url("<?= IMG.'actu/'.$pagesInfo->imageActu?>")'></div>
        								<h4><?= substr(strip_tags($pagesInfo->titre),0,60) ?><?php if (strlen($pagesInfo->titre) > 60) echo "..." ?></h4>
        								<p style="min-height:93px"><?= substr(strip_tags($pagesInfo->description),0,135)."..." ?></p>
        								<!-- CHANGER LIEN QUAND PAGE INFO SERA FAITE -->
        								<a href="<?= URL."/Front/page_info/".$pagesInfo->idActu; ?>"><button class="btn center-block" >En savoir plus</button></a>
        							</div>
                                    <?php
                                }
                            } ?>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>
<section class="actualites">
	<div class="container">
		<div class="row">
			<h2>Les dernières <span>actualités</span></h2>
			<div class="col-lg-9 col-md-9 col-xs-12">
				<div class="row">
				    <?php
                    $i =0;
                    foreach ($actus as $actu){
                        
                        $mois_fr = Array("", "Janv.", "Févr.", "Mars", "Avr.", "Mai", "Juin", "Jui.", "Août","Sept.", "Oct.", "Nov.", "Déc.");
                        $dateAffichage = strtotime($actu->dateCreation);
                        $jour=date("d",$dateAffichage);
                        $mois=intval(date("m",$dateAffichage));
                        $mois = $mois_fr[$mois];
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 actu-big">
    						<a href="<?=site_url("Front/actualite/".$actu->idActu)?>">
    							<div class="actu-box">
    								<div class="photo" style='background:url("<?= IMG.'actu/'.$actu->imageActu?>")'></div>
    								<span class="shadow"></span>
    								<div class="infos">
    									<div class="col-lg-2 col-sm-4 col-xs-3 date">
    										<span><?= $jour; ?></span>
    										<span><?= $mois; ?></span>
    									</div>	
    									<div class="col-lg-10 col-xs-9 auteur"><span>Posté par : </span><?= $actu->prenomUtilisateur; ?> <?= $actu->nomUtilisateur; ?></div>
    								</div>
    								<div class="col-lg-12 col-sm-12 col-xs-12 texte">
    									<h4><?= substr(strip_tags($actu->titre),0,60) ?><?php if (strlen($actu->titre) > 60) echo "..." ?></h4>	
    									<p><?= substr(strip_tags($actu->description),0,200) ?><?php if (strlen($actu->description) > 200) echo "..." ?></p>	
    								</div>
    							</div>
    						</a>
    					</div>
                        <?php
                        $i++;
                        if($i==3){
                            break;
                        }
                    } ?>
                        
				</div>
			</div>
			<div class="col-lg-3 col-md-3 actu-small">
			    <?php
                $i =0;
                foreach ($actus as $actu){

                    $dateAffichage = strtotime($actu->dateCreation);
                    $jour=date("d",$dateAffichage);
                    $mois=intval(date("m",$dateAffichage));
                    $mois = $mois_fr[$mois];
                    if($i>=3){
                        ?>
                        <a href="<?=site_url("Front/actualite/".$actu->idActu)?>">
        					<div class="actu-box">
        						<div class="col-lg-5 col-md-12 date">
        							<span><?= $jour; ?></span>
    								<span><?= $mois; ?></span>					
        						</div>
        						<div class="col-lg-7 col-md-12 contenu">
        							<h4><?= $actu->titre; ?></h4>
        						</div>
        					</div>
        				</a>
                        <?php
                    }
                    $i++;
                    if($i==6){
                        break;
                    }
                } ?>
                <div class="row">
					<a href="<?= site_url("Front/actualites")?>"><button class="btn center-block">Voir toutes les actualités</button></a>
				</div>
			</div>	
		</div>	
	</div>
</section>
