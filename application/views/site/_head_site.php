<?php 
	require 'lessc.inc.php';

	try {
	     lessc::ccompile('./assets/site/css/style.less', './assets/site/css/style.css');
	     lessc::ccompile('./assets/site/css/share.less', './assets/site/css/share.css');
	     lessc::ccompile('./assets/site/css/responsive.less', './assets/site/css/responsive.css');
	} catch (exception $ex) {
	     exit('lessc fatal error:
	     '.$ex->getMessage());
	}
?><!DOCTYPE html>
<html>
	<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/reset.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/bootstrap-theme.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/jquery.bxslider.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/owl.carousel.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(RESSOURCES.'/css/simplePagination.css'); ?>">
	
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/animate.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/share.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/style.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE.'/css/responsive.css'); ?>">
	<!--<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>-->
	
	<link rel="stylesheet" type="text/css" href="<?php echo(RESSOURCES.'/css/fullcalendar.css'); ?>">
	<?php if(isset($actu)){ ?>
		<title><?= url_title($actu->titre) ?></title>
		<meta name="description" description="<?= $actu->resume ?>"/><?php
	}else{ ?>
		<title><?= url_title('Stop Gaspi') ?></title>
	<?php } ?>
	

</head>
<body>
