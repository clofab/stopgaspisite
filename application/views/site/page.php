
<!--<section class="domaine_energetique_intro">-->
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			<h1>Domaine énergétique concerné : -->
<!--			<?php foreach($fiche->typeEnergie as $typeEnergie){-->
<!--				$separator ='|';-->
<!--		    	if ($typeEnergie === end($fiche->typeEnergie)) $separator = "";?>-->

<!--				<a style="color:white" href="<?= URL."/index.php/Front/domaineEnergie/".$typeEnergie->idTypeEnergie?>"><span><?= $typeEnergie->libelleTypeEnergie; ?></a></span><?=$separator ?>-->
<!--			<?php } ?>-->
<!--			</h1>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->

<!--<section class="page_infos">-->
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			<div class="col-md-8 description">-->
<!--				<h1>FICHE CONSEIL - <?= $fiche->titre; ?></h1>-->
<!--				<?= $fiche->description; ?>-->
<!--				<p class="domaines">Domaines : -->
<!--					<?php foreach($fiche->typeEnergie as $typeEnergie){?>-->
<!--						<a href="<?=URL;?>/index.php/Front/domaineEnergie/<?= $typeEnergie->idTypeEnergie; ?>"><?= $typeEnergie->libelleTypeEnergie; ?></a>  -->
<!--					<?php }?>-->
<!--				</p>-->
<!--				<br>-->
<!--				<a href="<?=URL;?>/index.php/Generate/pdf_fiche/<?= $fiche->idFiche; ?>" target="_blank" class="btn btn-primary col-md-offset-4" role="button">Télécharger la fiche</a>-->
<!--			</div>-->
<!--			<div class="col-md-4">-->
<!--				<div class="row partage">-->
<!--					<h3 class="pull-left"><i class="glyphicon glyphicon-bullhorn pull-left"></i>Je partage l'info : </h3>-->

<!--					<a class="share-btn share-btn-branded share-btn-twitter pull-right"-->
<!--					href="https://twitter.com/share?url=<?=URL;?>/index.php/Front/fiche/<?= $fiche->idFiche; ?>"-->
<!--					title="Partager sur Twitter">-->
<!--						<span class="share-btn-icon"></span>-->
<!--						<span class="share-btn-text-sr">Twitter</span>-->
<!--					</a>-->
<!--					<a class="share-btn share-btn-branded share-btn-facebook pull-right"-->
<!--					href="https://www.facebook.com/sharer/sharer.php?u=<?=URL;?>/index.php/Front/fiche/<?= $fiche->idFiche; ?>"-->
<!--					title="Partager sur Facebook">-->
<!--						<span class="share-btn-icon"></span>-->
<!--						<span class="share-btn-text-sr">Facebook</span>-->
<!--					</a>-->
<!--					<a class="share-btn share-btn-branded share-btn-googleplus pull-right"-->
<!--					href="https://plus.google.com/share?url=<?=URL;?>/index.php/Front/fiche/<?= $fiche->idFiche; ?>"-->
<!--					title="Partager sur Google+">-->
<!--						<span class="share-btn-icon"></span>-->
<!--						<span class="share-btn-text-sr">Google+</span>-->
<!--					</a>-->
<!--					<a class="share-btn share-btn-branded share-btn-linkedin pull-right"-->
<!--					href="https://www.linkedin.com/shareArticle?mini=true&url=<?=URL;?>/index.php/Front/fiche/<?= $fiche->idFiche; ?>"-->
<!--					title="Partager sur LinkedIn">-->
<!--						<span class="share-btn-icon"></span>-->
<!--						<span class="share-btn-text-sr">LinkedIn</span>-->
<!--					</a>-->


<!--				</div>-->
<!--				<div class="row rss">-->
<!--					<h3 class="pull-left"><i class="glyphicon glyphicon-info-sign pull-left"></i>Je me tiens informé : </h3>-->

<!--					<a class="rss-btn pull-right"-->
<!--					href="<?=URL;?>/Rss/index"-->
<!--					title="RSS"  type="application/rss+xml">-->
<!--						<span class="share-btn-text-sr"><img src="<?=SITE;?>/img/rss.png">Flux RSS</span>-->
<!--					</a>-->

<!--				</div>-->
<!--				<div class="row liste-actus">-->
<!--					<h3>Dernières actualités publiées :</h3>-->
<!--					<?php-->
<!--	                	foreach ($actus as $actu){-->
<!--	                        $dateAffichage = strtotime($actu->dateCreation);-->
<!--	                        $jour=date("d",$dateAffichage);-->
<!--                            $mois=date("m",$dateAffichage);-->
<!--                            $annee=date("y",$dateAffichage);-->
<!--                            ?>-->
<!--                            <a href="#" class="mini-actu">-->
<!--								<h4><?=$actu->titre?></h4>-->
<!--								<p><?= substr(strip_tags($actu->description),0,150)."..." ?></p>-->
<!--								<p class="auteur">Ecrit le <span><?= $jour;?>/<?= $mois;?>/<?= $annee;?></span> par <span><?= $actu->prenomUtilisateur; ?> <?= $actu->nomUtilisateur; ?></span></p>-->
<!--							</a>-->
<!--	                        <?php-->
<!--	            		} ?>-->

<!--					<button class="btn center-block">Voir toutes les actualités</button>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->