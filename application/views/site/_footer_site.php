

    <!-- SOURCE JS -->
    <script src="<?php echo(RESSOURCES.'/js/jquery-2.1.4.min.js'); ?>"></script>
    <script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <script src="<?php echo(RESSOURCES.'/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/moment.min.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/fullcalendar.min.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/fullcalendar-fr.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/jquery.bxslider.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/jquery.simplePagination.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/datatables.min.js'); ?>"></script>

    
     <!-- SCRIPT APPLI -->
    <script src="<?php echo(SITE.'/js/app.js'); ?>"></script>
    <script src="<?php echo(RESSOURCES.'/js/main.js'); ?>"></script>
    <script src="<?php echo(SITE.'/js/front.js'); ?>"></script>
    <script src="<?php echo(SITE.'/js/rendez_vous.js'); ?>"></script>

    
    <script type="text/javascript">
	

	/* BTN Partage */
	(function(){
	  
	  var shareButtons = document.querySelectorAll(".share-btn");

	  if (shareButtons) {
	      [].forEach.call(shareButtons, function(button) {
	      button.addEventListener("click", function(event) {
	 		var width = 650,
	        height = 450;

	        event.preventDefault();

	        window.open(this.href, 'Share Dialog', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width='+width+',height='+height+',top='+(screen.height/2-height/2)+',left='+(screen.width/2-width/2));
	      });
	    });
	  }

	})();

    </script>
    <?php if(isset($visio)){ ?>
    <script type="text/javascript">
        var username = "<?= str_replace(" ", "_", $this->session->userdata('interlocuteur'));?>";
    </script>
    <script src="//cdn.socket.io/socket.io-1.4.5.js"></script>
    <script src="<?php echo(RESSOURCES.'/js/PhoneRTCProxy.js'); ?>"></script>
    <script src="<?php echo(SITE.'/js/visio.js'); ?>"></script>
    <?php }?>
</body>

</html>