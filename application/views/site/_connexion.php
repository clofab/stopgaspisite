<section class="defaut">
	<div class="container">
		<div class="row page">
		    
		    <?php 
		    if(!empty($msgError)) { ?>
		   
		      <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <label>Identifiant de connexion incorrect</label>
            </div>
		    
		    <?php }?>
		        
            <h2>Connexion à votre espace "entreprise"</h2>
            <?php echo form_open('Front/accesEntreprise','id="connexEntreprise" class="form-horizontal"'); ?> 
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Email</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="text" class="form-control" name="emailEntreprise" id="emailEntreprise">
                    </div>
                </div>
        
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">Mot de passe</label>
                    <div class="col-lg-3 col-md-5">
                        <input type="password" class="form-control" name="mdpEntreprise" id="mdpEntreprise">
                    </div>
                </div>
        
                <div class="col-lg-offset-2 col-md-offset-3">
                    <button class="btn btn-primary" type="submit">Connexion</button>
                </div> 
                
                <div class="col-lg-offset-2 col-md-offset-3">
                    <p>Pas encore de compte ? alors <a href="/Front/inscription">inscrivez vous</a>.</p>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>
