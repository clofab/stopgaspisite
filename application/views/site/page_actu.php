<section class="page_photo">
	<div class="bg_flou" style="background:url('<?= FILES."/images/actu/".$actu->imageActu;?>')"></div>
	<div class="container bg_clear" style="background:url('<?= FILES."/images/actu/".$actu->imageActu;?>')">
		<?php
                $dateAffichage = strtotime($actu->dateCreation);
                $jour=date("d",$dateAffichage);
                $mois=date("m",$dateAffichage);
                $annee=date("y",$dateAffichage);
        ?>

		<h1><span><i class='glyphicon glyphicon-user'></i>Article écrit par <?= $actu->prenomUtilisateur; ?> <?= $actu->nomUtilisateur; ?>, le <?= $jour;?>/<?= $mois;?>/<?= $annee;?></span><?= $actu->titre; ?></h1>
	</div>
</section>
<section class="page_infos">
	<div class="container">
		<div class="row">
			<div class="col-md-8 description">
				<?= $actu->description; ?>
			</div>
			<div class="col-md-4">
				<div class="row partage">
					<h3 class="pull-left"><i class="glyphicon glyphicon-bullhorn pull-left"></i>Je partage l'info : </h3>

					<a class="share-btn share-btn-branded share-btn-twitter pull-right"
					href="https://twitter.com/share?url=<?=URL;?>/index.php/Front/actualite/<?= $actu->idActu; ?>"
					title="Partager sur Twitter">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Twitter</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-facebook pull-right"
					href="https://www.facebook.com/sharer/sharer.php?u=<?=URL;?>/index.php/Front/actualite/<?= $actu->idActu; ?>"
					title="Partager sur Facebook">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Facebook</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-googleplus pull-right"
					href="https://plus.google.com/share?url=<?=URL;?>/index.php/Front/actualite/<?= $actu->idActu; ?>"
					title="Partager sur Google+">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">Google+</span>
					</a>
					<a class="share-btn share-btn-branded share-btn-linkedin pull-right"
					href="https://www.linkedin.com/shareArticle?mini=true&url=<?=URL;?>/index.php/Front/actualite/<?= $actu->idActu; ?>"
					title="Partager sur LinkedIn">
						<span class="share-btn-icon"></span>
						<span class="share-btn-text-sr">LinkedIn</span>
					</a>


				</div>
				
				<?php if($actu->pageConseil == null){ ?>
					<div class="row rss">
						<h3 class="pull-left"><i class="glyphicon glyphicon-info-sign pull-left"></i>Je me tiens informé : </h3>
	
						<a class="rss-btn pull-right"
						href="<?=URL;?>/Rss/index"
						title="RSS"  type="application/rss+xml">
							<span class="share-btn-text-sr"><img src="<?=SITE;?>/img/rss.png">Flux RSS</span>
						</a>
	
					</div>
				<?php } ?>
				<div class="row liste-actus">
					<h3>Dernières actualités publiées :</h3>
					<?php
						$i = 0;
	                	foreach ($actus as $actualite){
	                        $dateAffichage = strtotime($actualite->dateCreation);
	                        $jour=date("d",$dateAffichage);
                            $mois=date("m",$dateAffichage);
                            $annee=date("y",$dateAffichage);
                            ?>
                            <a href="<?= URL."/index.php/Front/actualite/".$actualite->idActu; ?>" class="mini-actu">
								<h4><?=$actualite->titre?></h4>
								<p><?= substr(strip_tags($actualite->description),0,150)."..." ?></p>
								<p class="auteur">Ecrit le <span><?= $jour;?>/<?= $mois;?>/<?= $annee;?></span> par <span><?= $actualite->prenomUtilisateur; ?> <?= $actualite->nomUtilisateur; ?></span></p>
							</a>
	                        <?php
	                        $i++;
	                        if($i >= 5){
	                        	break;
	                        }
	            		}
	            	?>

					<a href="<?= URL."/index.php/Front/actualites";?>"><button class="btn center-block">Voir toutes les actualités</button></a>
				</div>
			</div>
		</div>
	</div>
</section>