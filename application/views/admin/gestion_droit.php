
<div class="col-md-9 col-lg-10 pageContaint">
    <div class="col-md-12">
        <h2>Gestion des droits</h2>
        <div class="col-md-12 boxed">
            <table class="table table-striped table-hover" id="tabDroit">
                <thead>
                <tr>
                    <th class="col-md-2"></th>
                    <th class="col-md-1">Fiche</th>
                    <th class="col-md-1">Actualité</th>
                    <th class="col-md-1">Video</th>
                    <th class="col-md-1">Webmaster</th>
                    <th class="col-md-1">Annexes</th>
                    <th class="col-md-1">Gestion du site</th>
                    <th class="col-md-1">Droit pour conseillers</th>
                    <th class="col-md-1">Gestions des Agendas</th>
                    <th class="col-md-1">Gestion des Droits</th>
                    <th class="col-md-1"></th>
                    <th class="col-md-1"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($types_utilisateur as $type_utilisateur) { ?>
                    <tr id="<?=$type_utilisateur->idTypeUtilisateur?>">
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control lblTypeUtilisateur" maxlength="55" value="<?= $type_utilisateur->lblTypeUtilisateur?>">
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkFiche" <?php if($type_utilisateur->gestionFiche == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkActu" <?php if($type_utilisateur->gestionActu == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkVideo" <?php if($type_utilisateur->gestionVideo == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkUtilisateur" <?php if($type_utilisateur->gestionUtilisateur == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkAnnexe" <?php if($type_utilisateur->gestionAnnexe == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox text-center">
                                <input type="checkbox" class="checkSite" <?php if($type_utilisateur->gestionSite == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkConseiller" <?php if($type_utilisateur->droitConseiller == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkRdv" <?php if($type_utilisateur->gestionRdv == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox">
                                <input type="checkbox" class="checkDroit" <?php if($type_utilisateur->gestionDroit == 1 ) echo "checked"?>>
                                <label></label>
                            </div>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs col-xs-12 validDroit" data-type="0">Modifier</button></td>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger btn-xs col-xs-12 deleteDroit" data-type="0">Supprimer</button></td>
                        </td>
                    </tr>
                <?php } ?>

                <tr>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control lblTypeUtilisateur" maxlength="55" placeholder="Nouveau Type">
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkFiche" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkActu" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkVideo" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkUtilisateur" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkAnnexe">
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox text-center">
                            <input type="checkbox" class="checkSite" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkConseiller">
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkRdv" >
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" class="checkDroit" >
                            <label></label>
                        </div>
                    </td>
                    <td colspan="2">
                        <button type="button" class="btn btn-primary btn-xs col-xs-12 validDroit" data-type="1">Créer</button></td>
                    </td>

                </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>
