
<div class="col-md-9 col-lg-10 pageContaint">
	<?php
	 $selEmpty ="selEmpty";

	 if($fiche->idFiche != null){ // SI ON EST EN EDITION
		 $selEmpty ="" ?>
		<div class="alert alert-info" role="alert"> Créé le <?= $fiche->dateCreation." par ".$fiche->nomUtilisateur." ".$fiche->prenomUtilisateur ?>  </div>
	<?php } ?>

	<h2>Créer une fiche conseil</h2>
	<form class="form-horizontal boxed" action="<?=site_url()."/Fiche/validSaisieFiche"?>" method="POST" id="formFiche" enctype="multipart/form-data">
		<div class="form-group">
			<label class="col-sm-2 control-label">Intitulé de la fiche</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Titre" name="titreFiche" value="<?= $fiche->titre ?>" maxlength="150">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Contenu</label>
			<div class="col-sm-8">
				<textarea class="form-control description" rows="18" name="descriptionFiche"><?= $fiche->description ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Catégories</label>
			<div class="col-sm-8">

				<?php
				foreach ($typeEnergies as $typeEnergie){           // ON RECUPERE LES TYPES D'ENERGIE
					$checked = "";
					if ($fiche->typeEnergie != null){
						foreach($fiche->typeEnergie as $type){ // ON RECUPERE LES TYPES D'ENERGIE SI UPDATE
							if ($typeEnergie->idTypeEnergie == $type->idTypeEnergie) $checked = "checked" ;
						}
					}?>
					<div class="checkbox">
					<input type="checkbox" name="typeEnergie[]" value="<?=$typeEnergie->idTypeEnergie?>" <?= $checked ?>>
					<label></label>
					<span><?=$typeEnergie->libelleTypeEnergie?></span>
					</div><?php
				} ?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="checkbox">
					<input type="checkbox" name="checkEtat"<?php if($fiche->etatFiche == 1 ) echo "checked" ?>>
					<label></label>
					<span> Publier la fiche conseil</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<input type="hidden" name="idFiche" value="<?= $fiche->idFiche ?>">
				<button class="btn btn-primary" role="button" name="saveActu" type="submit">Enregistrer</button>
				<a href="<?=site_url()."/Fiche/indexFiche"?>" class="btn btn-default" role="button" >Annuler</a>
			</div>
		</div>
	</form>

</div>