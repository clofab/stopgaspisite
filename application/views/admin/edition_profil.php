<div class="col-md-9 col-lg-7 pageContaint">
		<h2>Edition du profil</h2>
		<form  method="post" class="form-horizontal" action="<?=site_url()."/Admin/updateAdmin"?>" enctype="multipart/form-data" id="formProfilAdmin" >
			<div class="form-group">
				<div class="col-sm-12">
					<label>Email</label>
					<input type="text" class="form-control" value="<?= $this->session->userdata('email'); ?>" placeholder="Email" name="email" maxlenght="50">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<label>Mot de passe</label>
					<input type="password" class="form-control" placeholder="Mot de passe" name="mdp" maxlenght="100">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<label>Confirmation du mot de passe</label>
					<input type="password" class="form-control" placeholder="Mot de passe (confirmation)" name="mdp2" maxlenght="100">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<label>Nom</label>
					<input type="text" class="form-control" placeholder="Nom" value="<?= $this->session->userdata('nom'); ?>" name="nom" maxlenght="50">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<label>Prenom</label>
					<input type="text" class="form-control" placeholder="Prenom" value="<?= $this->session->userdata('prenom'); ?>" name="prenom" maxlenght="50">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<label>Ajouter une photo</label>
					<input type="file" name="file" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12 col-md-12 ">
					<button type="submit" class="col-sm-3 col-sm-offset-1 btn btn-danger pull-right" id="updateProfil">Modifier</button>
				</div>
			</div>
		</form>
	</div>