
<div class="col-md-9 col-lg-10">
	<h2>Fiches conseil</h2>
	

	<div class="col-md-12 boxed">
			<a href="<?=site_url("Fiche/saisieFiche")?>" class="btn btn-primary" role="button">Nouvelle Fiche</a>
			<input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchbox">
			<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#archivFiche">
				Consulter les archives
			</button>
	</div>
    
    <div class="col-md-12 boxed">
        <table class="table table-striped table-hover" id="tabFiche">
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkAll"></th>
                    <th>Titre</th>
                    <th>Domaine énergétique</th>
                    <th>Date de publication</th>
                    <th>Publier</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($fiches as $fiche) { 
                    $date = explode('-', $fiche->dateCreation);
                    $fiche->dateCreation = $date[2].'/'.$date[1].'/'.$date[0]?>

                    <tr id="<?= $fiche->idFiche;?>">
                        <td><input class="checkFiche" type="checkbox"></td>
                        <td><?= $fiche->titre ?></td>
                        <td>
                            <ul>
                                <?php foreach ($fiche->typeEnergie as $typeEnergie){ ?>
                                    <li> <?=$typeEnergie->libelleTypeEnergie;?> </li>
                                <?php }?>
                            </ul>
                        </td>
                        <td><?= $fiche->dateCreation?></td>

                        <td style="width:5%" class="publishFiche">
                            <div class="checkbox">
                                <input type="checkbox" class="etatPublish" <?php if ($fiche->etatFiche == 1) echo "checked" ?>>
                                <label></label>
                            </div>
                        </td>
                        <td style="width:5%">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                    Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="pull-right dropdown-menu">
                                    <li><a href="<?=site_url("Fiche/saisieFiche/".$fiche->idFiche);?>">Editer</a></li>
                                    <li><a href="#" class="deleteFiche" >Archiver</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>

	<div class="btn-group dropup" role="group">
		<button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Sélection
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="#" class="publishAllFiche" data-etat="1">Publier</a></li>
			<li><a href="#" class="publishAllFiche" data-etat="2">Dépublier</a></li>
			<li><a href="#" class="deleteAllFiche">Archiver</a></li>
		</ul>
	</div>



	<!-- Modal Archive -->
	<div class="modal fade" id="archivFiche" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Archives Fiches</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-10">
							<label><input type="search" class="form-control" placeholder="Rechercher" id="searchArchiveFiche"></label>
						</div>
					</div>
					<table class="table table-striped table-hover" id="tabArchiveFiche" style="width: 100%;">
						<thead>
						<tr>
							<th>Date de publication</th>
							<th>Titre</th>
							<th>Domaine énergetique</th>
							<th></th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
	
</div>
