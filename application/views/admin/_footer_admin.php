<!-- SOURCE ADMIN -->
<script src="<?php echo(RESSOURCES.'/js/jquery-2.1.4.min.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/bootstrap.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/datatables.min.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/bootstrap-datetimepicker.fr-FR.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/drag.js'); ?>"></script>
<script src='//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src="//cdn.socket.io/socket.io-1.4.5.js"></script>
<script src="<?php echo(RESSOURCES.'/js/PhoneRTCProxy.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.js"></script>


<script>
    username = "<?= $this->session->userdata('prenom')?>_<?= $this->session->userdata('nom')?>";
</script>
<script src="<?php echo(RESSOURCES.'/js/main.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_actu.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_fiche.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_utilisateur.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_droit.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_annexe.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_video.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_visio.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_planning.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_gestionsite.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_slider.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/admin_interface.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/dashboard.js'); ?>"></script>


</body>
</html>

