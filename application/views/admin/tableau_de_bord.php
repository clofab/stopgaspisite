<div class="col-md-9 col-lg-10 pageContaint">
    <div class="col-md-12">
        <h2>Tableau de bord</h2>
        <div class="col-md-12 dashboard boxed">
            
            <?php // SI GESTION DES ACTUALITES
                if((isset($chart_actu_mois_global))||(isset($chart_actu_etat_global))){
                    ?>
                    <h2>Actualités</h2>
                    <div class="row"><?php
                    if(isset($chart_actu_etat_global)){
                        ?>
                        <div class="col-md-4">
                            <h3>Etat des actualités</h3>
                            <canvas id="chartActuEtat"></canvas>
                        </div>
                        
                    <?php
                    }
                    if(isset($chart_actu_mois_global)){
                        ?>
                        <div class="col-md-8">
                            <h3>Publication des actualités</h3>
                            <canvas id="chartActuMois"></canvas>
                        </div>
                        <?php
                    }
                    ?></div><?php
                }
                if((isset($chart_fiche_mois_global))||(isset($chart_fiche_etat_global))){
                    ?>
                    <h2>Fiches conseils</h2>
                    <div class="row"><?php
                    if(isset($chart_fiche_etat_global)){
                        ?>
                        <div class="col-md-4">
                            <h3>Etat des fiches conseils</h3>
                            <canvas id="chartFicheEtat"></canvas>
                        </div>
                        
                        <?php
                    }
                    if(isset($chart_fiche_mois_global)){
                        ?>
                        <div class="col-md-8">
                            <h3>Publication des fiches conseils</h3>
                            <canvas id="chartFicheMois"></canvas>
                        </div>
                        
                        <?php
                    }
                    ?></div><?php
                }
                if((isset($chart_fiche_mois_global))||(isset($chart_fiche_etat_global))){
                    ?>
                    <h2>Vidéos</h2>
                    <div class="row"><?php
                        if(isset($chart_video_etat_global)){
                        ?>
                        <div class="col-md-4">
                            <h3>Etat des vidéos</h3>
                            <canvas id="chartVideoEtat"></canvas>
                        </div>
                        
                        <?php
                    }
                    if(isset($chart_video_mois_global)){
                        ?>
                        <div class="col-md-8">
                            <h3>Publication des vidéos</h3>
                            <canvas id="chartVideoMois"></canvas>
                        </div>
                        
                        <?php
                    }
                ?></div><?php
                }
                // SI GESTION DES UTILISATEURS OK, ALORS ON AFFICHE LES UTILISATEURS AVEC LEUR DROITS
                if((isset($chart_uti))||(isset($chart_rdv_global))){
                    ?>
                    <h2>Utilisateurs</h2>
                    <div class="row"><?php
                        if(isset($chart_uti)){
                            ?>
                            <div class="col-md-4">
                                <h3>Effectif des Utilisateurs par type</h3>
                                <canvas id="chartTypeUti"></canvas>
                            </div>
                        <?php
                        }
                        if(isset($chart_rdv_global)){
                            ?>
                            <div class="col-md-4">
                                <h3>Détail des rendez-vous</h3>
                                <canvas id="chartRdvG"></canvas>
                            </div>
                        <?php
                    }
                    ?></div><?php
                }
            ?>
            
            
        </div>
    </div>
</div>

<!-- SOURCE ADMIN -->
<script src="<?php echo(RESSOURCES.'/js/jquery-2.1.4.min.js'); ?>"></script>
<script src="<?php echo(RESSOURCES.'/js/bootstrap.js'); ?>"></script>

<script src="//cdn.socket.io/socket.io-1.4.5.js"></script>
<script src="<?php echo(RESSOURCES.'/js/PhoneRTCProxy.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.js"></script>




<!-- MY APP -->
<script>
    username = "<?= $this->session->userdata('prenom')?>_<?= $this->session->userdata('nom')?>";
    <?php  // STATS RDV
        if(isset($chart_rdv_global)){
            ?>
            var dataRdvG = {
                labels: [
                    "Finis / Annulés / Réfusés",
                    "A faire",
                    "En attente de compte-rendu"
                ],
                datasets: [
                    {
                        data: [<?=(array_key_exists('fini',$chart_rdv_global)?$chart_rdv_global['fini']:0) ?>, <?=(array_key_exists('attente_rdv',$chart_rdv_global)?$chart_rdv_global['attente_rdv']:0) ?>, <?=(array_key_exists('attente_cr',$chart_rdv_global)?$chart_rdv_global['attente_cr']:0) ?>],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }]
            };<?php
        }
        //STATS ETAT ACTUS
        if(isset($chart_actu_etat_global)){
            ?>
            var dataTypeActus = {
                labels: [
                    "Archivés",
                    "Publiés"
                ],
                datasets: [
                    {
                        data: [<?=(array_key_exists('0',$chart_actu_etat_global)?$chart_actu_etat_global['0']:0) ?>, <?=(array_key_exists('1',$chart_actu_etat_global)?$chart_actu_etat_global['1']:0) ?>],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }]
            };<?php
        }
        //STATS PUBLICATION ACTUS
        if(isset($chart_actu_mois_global)){
            ?>
            var dataPubliActus = {
                labels: <?=json_encode(array_keys($chart_actu_mois_global))?>,
                datasets: [
                    {
                        label: "Actualités publiées ce mois",
                        yAxisID: 0,
                        data: <?=json_encode(array_values($chart_actu_mois_global))?>
                    }]
            };<?php
        }
        
        //STATS ETAT FICHES CONSEIL
        if(isset($chart_fiche_etat_global)){
            ?>
            var dataTypeFiches = {
                labels: [
                    "Archivés",
                    "Publiés"
                ],
                datasets: [
                    {
                        data: [<?=(array_key_exists('0',$chart_fiche_etat_global)?$chart_fiche_etat_global['0']:0) ?>, <?=(array_key_exists('1',$chart_fiche_etat_global)?$chart_fiche_etat_global['1']:0) ?>],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }]
            };<?php
        }
        
        //STATS PUBLICATION FICHES CONSEILS
        if(isset($chart_fiche_mois_global)){
            ?>
            var dataPubliFiches = {
                labels: <?=json_encode(array_keys($chart_fiche_mois_global))?>,
                datasets: [
                    {
                        label: "Fiches conseils publiées ce mois",
                        yAxisID: 0,
                        data: <?=json_encode(array_values($chart_fiche_mois_global))?>
                    }]
            };<?php
        }
        //STATS ETAT VIDEOS
        if(isset($chart_video_etat_global)){
            ?>
            var dataTypeVideos = {
                labels: [
                    "Archivés",
                    "Publiés"
                ],
                datasets: [
                    {
                        data: [<?=(array_key_exists('0',$chart_video_etat_global)?$chart_video_etat_global['0']:0) ?>, <?=(array_key_exists('1',$chart_video_etat_global)?$chart_video_etat_global['1']:0) ?>],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }]
            };<?php
        }
        //STATS PUBLICATION VIDEOS
        if(isset($chart_video_mois_global)){
            ?>
            var dataPubliVideos = {
                labels: <?=json_encode(array_keys($chart_video_mois_global))?>,
                datasets: [
                    {
                        label: "Videos publiées ce mois",
                        yAxisID: 0,
                        data: <?=json_encode(array_values($chart_video_mois_global))?>
                    }]
            };<?php
        }
        // STATS TYPES UTILISATEURS
        
         if(isset($chart_uti)){
            ?>
            var dataTypeUti = {
                labels: [
                    <?php foreach($chart_uti as $utilisateur){ ?>
                         '<?=$utilisateur->lblTypeUtilisateur?>',
                    <?php } ?>
                ],
                datasets: [
                    {
                        data:[
                            <?php foreach($chart_uti as $utilisateur){ ?>
                                 '<?=$utilisateur->nbType?>',
                            <?php } ?>
                        ],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }]
            };<?php
        }
    ?>
</script>
<script src="<?php echo(RESSOURCES.'/js/main.js'); ?>"></script>
<script src="<?php echo(ADMIN.'/js/dashboard.js'); ?>"></script>


</body>
</html>
