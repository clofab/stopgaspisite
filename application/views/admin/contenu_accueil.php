
<?php $contenu = $contenu[0];?>
<div class="col-md-9 col-lg-10 pageContaint">

    <h2>Paragraphe de présentation (en page d'accueil)</h2>
    <div class="boxed col-md-12">
    
        <form class="form-horizontal" action="<?=site_url()."/GestionSite/validAccueil"?>" method="POST" id="formAccueil">
            <div class="form-group">
                <label class="col-sm-2 control-label">Titre</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Titre" name="titrePres" value="<?=$contenu->titreContenu?>" maxlength="150">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Contenu</label>
                <div class="col-sm-8">
                    <textarea class="form-control description" rows="18" name="descriptionPres" ><?=$contenu->descriptionContenu?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <button class="btn btn-primary" role="button" id="saveAccueil" type="submit">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>
</div>