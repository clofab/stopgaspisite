
<div class="col-md-9 col-lg-10 pageContaint">
    <?php if($video->idVideo != null){ // SI ON EST EN EDITION
        $date = explode('-', $video->dateCreation);
        $video->dateCreation = $date[2].'/'.$date[1].'/'.$date[0]?>
        <div class="alert alert-info" role="alert"> Créé le <?= $video->dateCreation." par ".$video->nomUtilisateur." ".$video->prenomUtilisateur ?>  </div>
    <?php } ?>

	<h2>Publier une vidéo</h2>
	<form class="form-horizontal boxed" id="formVideo" action="<?=site_url()."/Videos/validSaisieVideo"?>" method="POST">
		<div class="form-group">
			<label class="col-sm-2 control-label">Titre de la vidéo</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Titre" name="titreVideo" value="<?=$video->titreVideo ?>" maxlength="150">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">URL vidéo </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Url de la video" name="urlVideo" value="<?=$video->urlVideo ?>">
			</div>
		</div>

		<div class="form-group" id="areaIframe">

            <?php if($video->urlVideo != null){?>
                <iframe class="col-md-6 col-md-offset-3 embed-responsive-item " height="315px" src="<?=$video->urlVideo ?>" frameborder="0" allowfullscreen></iframe>
            <?php }?>

		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Description</label>
			<div class="col-sm-8">
				<textarea class="form-control" rows="8"  name="descriptionVideo"><?=$video->descriptionVideo ?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Catégories</label>
			<div class="col-sm-8">

                 <?php
				 foreach ($typeEnergies as $typeEnergie){           // ON RECUPERE LES TYPES D'ENERGIE
                     $checked = "";

                     if ($video->typeEnergie != null){
                         foreach($video->typeEnergie as $idType){ // ON RECUPERE LES TYPES D'ENERGIE SI UPDATE
                             if ($typeEnergie->idTypeEnergie == $idType->idTypeEnergie) $checked = "checked" ;
                         }
                     }?>
                     <div class="checkbox">
                     <input type="checkbox" name="typeEnergie[]" value="<?=$typeEnergie->idTypeEnergie?>" <?= $checked ?>>
                     <label></label>
                     <span><?=$typeEnergie->libelleTypeEnergie?></span>
                     </div><?php
                 } ?>

			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Visibilité</label>
			<div class="col-sm-8">
				<div class="checkbox">
					<input type="checkbox" name="etatVideo" <?php if($video->etatVideo == 1 ) echo "checked" ?>>
					<label></label>
					<span>Publier</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
                <input name="idVideo" id="idVideo" type="hidden" value="<?= $video->idVideo ?>"/>
                <button class="btn btn-primary" role="button" name="validVideo" type="submit">Enregistrer</button>
				<a href="<?=site_url()."/Videos/indexVideo"?>" class="btn btn-default" role="button" >Annuler</a>
			</div>
		</div>
	</form>

</div>

