<?php $conseiller = $conseiller[0]?>

<div class="col-md-9 col-lg-10">

    <div class="col-md-12">
        <h2>Agenda</h2>

        <div class="col-md-12 boxed">
            <p>Renseignez dans le planing les horaires durant lesquelles vous souhaitez recevoir des rendez-vous.</br>En cas d'absence, renseignez vos indisponibilités via le bouton "Indisponibilités" :</p>
            <button class="btn btn-primary openPlanning">Planning</button>
            <button class="btn btn-primary openIndispo">Indisponibilités</button>
            <input type="hidden" id="data-espace-cons">
        </div>

    </div>

    <div class="col-md-12">
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a data-toggle="tab" href="#prochain" aria-controls="prochain"><h3>Prochains rendez-vous</h3></a></li>   
            <li role="presentation"><a data-toggle="tab" href="#attente" aria-controls="attente"><h3>Rendez-vous passés</h3></a></li>
            <li role="presentation"><a data-toggle="tab" href="#archiver" aria-controls="archiver"><h3>Rendez-vous archivés</h3> </a></li>
        </ul>
        
        <div class="col-md-12 boxed tab-content">
            
            <?php 
                date_default_timezone_set('Europe/Paris');
                $dateNow = date( 'Y-m-d H:i:s');
            ?>
            
            <!-- PROCHAIN RENDEZ VOUS -->
            <div id="prochain" class="tab-pane fade in active">
                <table class="table table-striped table-hover tabRdv">
                    <thead>
                        <tr>
                            <th>Nom du contact</th>
                            <th>Entreprise</th>
                            <th>Téléphone</th>
                            <th>Mail</th>
                            <th>RDV</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                       
                        foreach($rdvConseillers as $rdvConseiller){ 
                      
                            if($rdvConseiller->etatRdv == 1 or ($rdvConseiller->etatRdv == 2 and $rdvConseiller->plageHorraire > $dateNow)){
    
                                $dateRdv = explode('-',substr($rdvConseiller->plageHorraire, 0, 10));
                                $heureRdv = substr($rdvConseiller->heure_deb, 0, 5);
                                
                                switch ($rdvConseiller->etatRdv) {
                                    case 1:
                                        $btn = '<button type="button" class="col-md-3 btn btn-success btn-xs confirmRdv">Confirmer RDV</button> <button type="button" class="col-md-3 col-md-offset-1 btn btn-danger btn-xs refusRdv">Refuser RDV</button>';
                                        break;
                                    case 2:
                                        $btn = '<button type="button" class="col-md-7 btn btn-danger btn-xs annulRdv">Annuler RDV</button>';
                                        break;
                                }
                                ?>
        
                                <tr id="<?= $rdvConseiller->idRdv ?>">
                                    <td><?= $rdvConseiller->interlocuteur   ?></td>
                                    <td><?= $rdvConseiller->nomEntreprise   ?></td>
                                    <td><?= $rdvConseiller->telEntreprise   ?></td>
                                    <td><?= $rdvConseiller->emailEntreprise ?></td>
                                    <td><?= $dateRdv[2]."/".$dateRdv[1]."/".$dateRdv[0]." ".$heureRdv ?></td>
                                    <td><?= $btn ?></td>
                                </tr><?php
                            }
                        } ?>
    
                    </tbody>
                </table>
            </div>
            
            <!-- RENDEZ VOUS EN ATTENTE-->
            <div id="attente" class="tab-pane fade in">
                <table class="table table-striped table-hover tabRdv" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nom du contact</th>
                            <th>Entreprise</th>
                            <th>Téléphone</th>
                            <th>Mail</th>
                            <th>RDV</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                       
                        foreach($rdvConseillers as $rdvConseiller){ 
                      
                            if($rdvConseiller->etatRdv == 2 and $rdvConseiller->plageHorraire < $dateNow){
    
                                $dateRdv = explode('-',substr($rdvConseiller->plageHorraire, 0, 10));
                                $heureRdv = substr($rdvConseiller->heure_deb, 0, 5);
                                
                                ?>
        
                                <tr id="<?= $rdvConseiller->idRdv ?>">
                                    <td><?= $rdvConseiller->interlocuteur   ?></td>
                                    <td><?= $rdvConseiller->nomEntreprise   ?></td>
                                    <td><?= $rdvConseiller->telEntreprise   ?></td>
                                    <td><?= $rdvConseiller->emailEntreprise ?></td>
                                    <td><?= $dateRdv[2]."/".$dateRdv[1]."/".$dateRdv[0]." ".$heureRdv ?></td>
                                    <td><button type="button" class="btn btn-success btn-xs saisirCompteRendu">Remplir compte rendu</button></td>
                                </tr><?php
                            }
                        } ?>
    
                    </tbody>
                </table>
                
            </div>        
            
            <!-- RENDEZ VOUS ARCHIVER-->
            <div id="archiver" class="tab-pane fade in">
                <table class=" table table-striped table-hover tabRdv" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nom du contact</th>
                            <th>Entreprise</th>
                            <th>Téléphone</th>
                            <th>Mail</th>
                            <th>RDV</th>
                            <th>Compte rendu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                       
                        foreach($rdvConseillers as $rdvConseiller){ 
                      
                            if($rdvConseiller->etatRdv == 0 ){
    
                                $dateRdv = explode('-',substr($rdvConseiller->plageHorraire, 0, 10));
                                $heureRdv = substr($rdvConseiller->heure_deb, 0, 5);
                                ?>
        
                                <tr id="<?= $rdvConseiller->idRdv ?>">
                                    <td><?= $rdvConseiller->interlocuteur   ?></td>
                                    <td><?= $rdvConseiller->nomEntreprise   ?></td>
                                    <td><?= $rdvConseiller->telEntreprise   ?></td>
                                    <td><?= $rdvConseiller->emailEntreprise ?></td>
                                    <td><?= $dateRdv[2]."/".$dateRdv[1]."/".$dateRdv[0]." ".$heureRdv ?></td>
                                    <td data-cr="<?=htmlspecialchars($rdvConseiller->compte_rendu) ?>">
                                        <button type="button" class="col-md-12 btn btn-success btn-xs consultCR">Voir compte rendu </button>
                                    </td>

                                </tr><?php
                            }
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- MODAL GESTION PLANNING -->
<div class="modal fade planning" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Renseignez vos horaires de travail (debut / après-midi)</h4>
            </div>
            <div class="modal-body" >
                <div class="row" id="rowLundi" >
                    <div class="col-md-3"><p>Lundi</p></div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="debut" data-period="matin">
                        <input type="text" value="" placeholder="08:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="matin">
                        <input type="text" value="" placeholder="12:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 col-md-offset-1 form-group input-group-sm timePicker" data-time="debut" data-period="midi">
                        <input type="text" value="" placeholder="13:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="midi">
                        <input type="text" value="" placeholder="18:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                </div>

                <div class="row" id="rowMardi">
                    <div class="col-md-3"><p>Mardi</p></div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="debut" data-period="matin">
                        <input type="text" value="" placeholder="08:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="matin">
                        <input type="text" value="" placeholder="12:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 col-md-offset-1 form-group input-group-sm timePicker" data-time="debut" data-period="midi">
                        <input type="text" value="" placeholder="13:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="midi">
                        <input type="text" value="" placeholder="18:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                </div>

                <div class="row" id="rowMercredi">

                    <div class="col-md-3"><p>Mercredi</p></div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="debut" data-period="matin">
                        <input type="text" value="" placeholder="08:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="matin">
                        <input type="text" value="" placeholder="12:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 col-md-offset-1 form-group input-group-sm timePicker" data-time="debut" data-period="midi">
                        <input type="text" value="" placeholder="13:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="midi">
                        <input type="text" value="" placeholder="18:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                </div>

                <div class="row" id="rowJeudi">
                    <div class="col-md-3"><p>Jeudi</p></div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="debut" data-period="matin">
                        <input type="text" value="" placeholder="08:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="matin">
                        <input type="text" value="" placeholder="12:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 col-md-offset-1 form-group input-group-sm timePicker" data-time="debut" data-period="midi">
                        <input type="text" value="" placeholder="13:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="midi">
                        <input type="text" value="" placeholder="18:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                </div>

                <div class="row" id="rowVendredi">
                    <div class="col-md-3"><p>Vendredi</p></div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="debut" data-period="matin">
                        <input type="text" value="" placeholder="08:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="matin">
                        <input type="text" value="" placeholder="12:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 col-md-offset-1 form-group input-group-sm timePicker" data-time="debut" data-period="midi">
                        <input type="text" value="" placeholder="13:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                    <div class="col-md-2 form-group input-group-sm timePicker" data-time="fin" data-period="midi">
                        <input type="text" value="" placeholder="18:00" class="add-on form-control" ><i class="form-control-feedback glyphicon glyphicon-time"></i>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-primary" id="validPlanning">Enregistrer</button>
            </div>

        </div>
    </div>
</div>

<!-- MODAL INDISPONIBILITE -->
<div class="modal fade indisponibilite" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Vos indisponibilités</h2>
            </div>
            <div class="modal-body">
                <div class="row">

                    <h3 class="col-md-12">Ajoutez une période d'indisponibilité :</h3>
                    <h5 class="col-md-6" id="indispoAnnexe" data-annexe=""></h5>
                    <h5 class="col-md-6" id="indispoConseiller" data-conseiller=""></h5>


                    <div class="col-md-12 form-group">
                        <label class="control-label">Intitulé</label>
                        <input id="intitule" class="form-control" type="text" maxlength="150"/>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="input-append datePicker">
                            <label class="control-label">Date de début</label>
                            <input id="jourDebut" class="add-on form-control" type="date"/>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="input-append datePicker">
                            <label class="control-label">Date de fin</label>
                            <input id="jourFin" class="add-on form-control" type="date"/>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        <div class="input-append timePicker">
                            <label class="control-label">Heure de début</label>
                            <input id="heureDebut" class="add-on form-control" data-format="hh:mm:ss" placeholder="Heure de début" type="text"/>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="input-append timePicker">
                            <label class="control-label">Heure de fin</label>
                            <input id="heureFin" class="add-on form-control" data-format="hh:mm:ss" placeholder="Heure de fin" type="text"/>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <button type="button" class="btn btn-primary" id="validIndispo">Enregistrer</button>
                    </div>

                    <div id="indispoListe" class="col-md-12 hide">
                        <h3>Récapitulatif de vos absences</h3>
                        <table class="table table-striped table-hover col-md-12" id="tabIndispo" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Intitule</th>
                                <th>Date debut</th>
                                <th>Date Fin</th>
                                <th>Heure debut</th>
                                <th>Heure Fin</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>

            </div>

        </div>
    </div>
</div>



<!-- MODAL ANNULATION / REFUS RDV -->
<div class="modal fade modalRdv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleRdv">Étes vous sûr de vouloir supprimer le rendez vous ?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="form-group ">
                        <h5 class="col-md-12" id="titleCompteRendu">Compte rendu  :</h5>
                        <div class="col-md-12">
                           <textarea id="compte_rendu" class="form-control" maxlength="500"></textarea>
                       </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="validCompteRendu">Valider</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="annulModalRdv">Annuler</button>
            </div>

        </div>
    </div>
</div>

