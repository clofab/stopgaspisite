
<div class="col-md-9 col-lg-10 pageContaint">

    <?php if($actu->idActu != null){ // SI ON EST EN EDITION
        $date = explode('-', $actu->dateCreation);
        $actu->dateCreation = $date[2].'/'.$date[1].'/'.$date[0]?>
        <div class="alert alert-info" role="alert"> Créé le <?= $actu->dateCreation." par ".$actu->nomUtilisateur." ".$actu->prenomUtilisateur ?>  </div>
    <?php } ?>

    <h2>Créer une page</h2>
    <form class="form-horizontal boxed" action="<?=site_url()."/GestionSite/validSaisiePage"?>" method="POST" id="formActu" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label">Titre de la page</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Titre" name="titreActu" value="<?= $actu->titre ?>" maxlength="150">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Résumé (SEO)</label>
            <div class="col-sm-8">
                <textarea class="form-control" rows="3" name="resumeActu" maxlength="150"><?= $actu->resume ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Photo / illustration</label>
            <div class="col-sm-6">
                <input type="file" class="form-control" name="fileActu" >
            </div>
            <?php if($actu->idActu != null){ // SI ON EST EN EDITION 	?>
                <div class="col-sm-4">
                    <img class="imgEdit" height="75" width="75" src="<?= IMG.'actu/'.$actu->imageActu ?>" alt="">
                </div>
            <?php } ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Contenu</label>
            <div class="col-sm-8">
                <textarea class="form-control description" rows="18" name="descriptionActu" id="descriptionActu"><?= $actu->description ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="checkbox">
                    <input type="checkbox" name="checkEtat" <?php if($actu->etatActu == 1 ) echo "checked" ?>>
                    <label></label>
                    <span>Publier la page</span>
                </div>
            </div>
        </div>
        <!-- SI SUR PAGE ACCUEIL-->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="checkbox">
                    <input type="checkbox" name="checkAccueil" <?php if($actu->pageConseil == 1 ) echo "checked" ?>>
                    <label></label>
                    <span>Afficher sur page d'accueil (Section "environnement")</span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="hidden" name="idActu" value="<?= $actu->idActu ?>">
                <input type="hidden" name="lastImage" value="<?= $actu->imageActu ?>">
                <input type="hidden" name="etatImage" value="0">
                <input type="hidden" name="typeActu" value="1">
                <button class="btn btn-primary" role="button" name="saveActu" type="submit">Enregistrer</button>
            	<a href="<?=site_url()."/GestionSite/indexPageInfo"?>" class="btn btn-default" role="button" >Annuler</a>
               
            </div>
        </div>
    </form>

</div>


