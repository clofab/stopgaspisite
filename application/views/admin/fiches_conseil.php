<h2>Créer une fiche conseil</h2>
<form class="form-horizontal" action="<?=site_url()."/Fiche/validSaisieFiche"?>" method="POST" id="formFiche" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-sm-2 control-label">Intitulé de la fiche</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" placeholder="Titre" name="titreFiche" value="<?= $fiche->titre ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contenu</label>
		<div class="col-sm-8">
			<textarea class="form-control description" rows="18" name="descriptionFiche"><?= $fiche->description ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Catégories</label>
		<div class="col-sm-8">

			<?php
			foreach ($typeEnergies as $typeEnergie){           // ON RECUPERE LES TYPES D'ENERGIE
				$checked = "";
				if ($fiche->typeEnergie != null){
					foreach($fiche->typeEnergie as $type){ // ON RECUPERE LES TYPES D'ENERGIE SI UPDATE
						if ($typeEnergie->idTypeEnergie == $type->idTypeEnergie) $checked = "checked" ;
					}
				}?>
				<div class="checkbox">
				<input type="checkbox" name="typeEnergie[]" value="<?=$typeEnergie->idTypeEnergie?>" <?= $checked ?>>
				<label></label>
				<span><?=$typeEnergie->libelleTypeEnergie?></span>
				</div><?php
			} ?>

		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="checkbox">
				<input type="checkbox" name="checkEtat"<?php if($fiche->etatFiche == 1 ) echo "checked" ?>>
				<label></label>
				<span> Publier la fiche conseil</span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-8">
			<input type="hidden" name="idFiche" value="<?= $fiche->idFiche ?>">
			<button class="btn btn-primary" role="button" name="saveActu" type="submit">Enregistrer</button>
			<button class="btn" role="button" type="button">Annuler</button>
		</div>
	</div>
</form>

</div>