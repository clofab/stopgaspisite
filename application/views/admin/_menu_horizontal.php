<nav class="navbar navbar-default nav-horizontal">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img alt="Brand" src="<?php echo(IMG.'admin/logo_demo.png'); ?>">
			</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Menu de navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="photo_mini"><img src="<?= IMG.$this->session->userdata('imgAvatar'); ?>" width="40" height="40"></span>
						<span class="user">Bonjour <b><?= $this->session->userdata('prenom')?></b> <span class="caret"></span></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?=site_url()."Admin/profilAdmin"?>">Editer mon profil</a></li>
						<li><a href="<?=site_url()?>" target="_blank">Accéder au site</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?=site_url()."Admin/deconnexion"?>">Se déconnecter</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>