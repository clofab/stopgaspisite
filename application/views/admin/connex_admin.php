<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion Back Office</title>
    <link rel="stylesheet" type="text/css" href="<?php echo(RESSOURCES.'/css/login-page.css'); ?>">
</head>
<body>

	<div class="bg-planet"></div>
	<div class="container">
		<div class="form">
		    <form class="form-horizontal" action="<?=site_url()."/Admin/accesAdmin"?>" method="POST" id="formConnex"  name="formConnex">
    			<input type="text" placeholder="Email" name="email">	
    			<input type="password" placeholder="Password" name="password">
    			<input type="submit" name="submitAdmin" value="Connexion" class="connect-btn">
    			<label><input type="checkbox" name="remember"><span>Se souvenir de moi</span></label>
    		 </form>
		</div>
		<div class="logo">
            <?php
            if(isset($msgError)){ // SI ON EST EN EDITION 	?>
                <div class="alert"> <?= $msgError ?>  </div>
            <?php } ?>
			<img src="<?php echo(RESSOURCES.'/img/logo.jpg'); ?>" alt="">
		</div>
	</div>
	
</body>
</html>