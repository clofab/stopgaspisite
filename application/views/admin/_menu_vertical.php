	<nav class="col-md-3 col-lg-2 nav-vertical">
		
		<ul class="nav">
            <li class="menu-title">Navigation</li>
            <?php if($this->session->userdata("droitConseiller") != true) { ?>
				<li><a href="<?=site_url("Admin/dashboard")?>"><i class="fa fa-home"></i>Tableau de bord</a></li>
			<?php } ?>
			<?php if($this->session->userdata("gestionActu") == true) {	?>
				<li><a href="<?=site_url("Actualite/indexActu")?>"><i class="fa fa-newspaper-o"></i>Actualités</a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionFiche") == true) {	?>
				<li><a href="<?=site_url("Fiche/indexFiche")?>"><i class="fa fa-file-text-o"></i>Fiches conseil</a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionSite") == true) {	?>
				<li><a href="#" id="btn-site" data-toggle="collapse" data-target="#menuSite" aria-expanded="false"><i class="fa fa-desktop"></i>Contenu du site<i class="fa fa-chevron-right fa-pull-right"></i></a>
					<ul class="nav collapse" id="menuSite" role="menu" aria-labelledby="btn-site">

						<li><a href="#" id="btn-accueil" data-toggle="collapse" data-target="#menuAccueil" aria-expanded="false">Page Accueil<i class="fa fa-chevron-right fa-pull-right"></i></a>
							<ul class="nav collapse" id="menuAccueil" role="menu" aria-labelledby="btn-accueil">
								<li><a href="<?=site_url("Slider/indexSlider")?>">Slider</a></li>
								<li><a href="<?=site_url("GestionSite/contenuAccueil")?>">Contenu</a></li>
							</ul>
						</li>

                        <li><a href="#" id="btn-domaine" data-toggle="collapse" data-target="#menuDomaine" aria-expanded="false">Domaines énergétiques<i class="fa fa-chevron-right fa-pull-right"></i></a>
							<ul class="nav collapse" id="menuDomaine" role="menu" aria-labelledby="btn-domaine">

								<?php
								foreach ($typeEnergies as $typeEnergie){           // ON RECUPERE LES TYPES D'ENERGIE
									?>
									<li><a href="<?=site_url("GestionSite/indexTypeEnergie/".$typeEnergie->idTypeEnergie)?>"><?=$typeEnergie->libelleTypeEnergie?></a></li><?php
								} ?>
							</ul>
						</li>

						<li><a href="<?=site_url("GestionSite/indexPageInfo/1")?>">Pages Informations</a></li>
					</ul>
				</li>

			<?php } ?>

			<?php if($this->session->userdata("gestionVideo") == true) {	?>
				<li><a href="<?=site_url("Videos/indexVideo")?>"><i class="fa fa-youtube-play"></i>Gestion des vidéos</a></li>
			<?php } ?>

			<?php if($this->session->userdata("droitConseiller") == true) {	?>
				<li><a href="<?=site_url("Admin/visioconferenceAdmin")?>" id="visioLink">
					<i class="fa fa-comments-o"></i>Visioconférence  <i class="fa fa-phone fa-pull-right" style="display:none;color:green;"></i></a></li>
			<?php } ?>

			<?php if($this->session->userdata("droitConseiller") == true) {	
				//	SI CONSEILLER ALORS ON AFFICHE LE NOMBRE DE RENDEZ VOUS NON CONFIRMER SOUS FORME DE NOTIFICATIONS	?>
			
				<li><a href="<?=site_url("Conseillers/indexConseiller")?>"><i class="fa fa-calendar-minus-o"></i>Mon agenda <?php if(isset($notifRdv) && $notifRdv != 0 ){ ?> <span class="label label-danger" id="notifRdv"><?= $notifRdv ?> </span> <?php } ?></a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionRdv") == true) {	?>
			<li><a href="<?=site_url("Planning/indexPlanning")?>"><i class="fa fa-calendar-check-o"></i>Gestion des Agendas</a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionAnnexe") == true) {	?>
			<li><a href="<?=site_url("Annexes/indexAnnexe")?>"><i class="fa fa-university"></i>Gestion des annexes</a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionUtilisateur") == true) {	?>
			<li><a href="<?=site_url("Utilisateurs/indexUtilisateur")?>"><i class="fa fa-users"></i>Gestion des utilisateurs</a></li>
			<?php } ?>

			<?php if($this->session->userdata("gestionDroit") == true) {	?>
			<li><a href="<?=site_url("Droit/gestionDroit")?>"><i class="fa fa-lock"></i>Gestion des Droits</a></li>
			<?php } ?>
		</ul>
	</nav>