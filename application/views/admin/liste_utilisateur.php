
<div class="col-md-9 col-lg-10 pageContaint">
	<div class="col-md-3">
		<h2>Ajouter un utilisateur du backOffice</h2>
        <div class="col-md-12 boxed">
            <form class="form-horizontal" enctype="multipart/form-data" id="formUserAdmin" >
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="Email" id="email" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Mot de passe</label>
                        <input type="password" class="form-control" placeholder="Mot de passe" id="mdp" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Confirmation du mot de passe</label>
                        <input type="password" class="form-control" placeholder="Mot de passe (confirmation)" id="mdp2" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Nom</label>
                        <input type="text" class="form-control" placeholder="Nom" id="nom" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Prenom</label>
                        <input type="text" class="form-control" placeholder="Prenom" id="prenom" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Ajouter une photo</label>
                        <input type="file" id="imgUtilisateur" class="form-control" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 col-md-12 ">
                        <label>Groupe utilisateur</label>
                        <select class="form-control selEmpty" id="typeUtilisateur">
                            <?php  foreach ( $types_utilisateur as $type_utilisateur) {?>
                                <option value="<?=$type_utilisateur->idTypeUtilisateur?>" data-conseiller="<?=$type_utilisateur->droitConseiller?>"><?= $type_utilisateur->lblTypeUtilisateur?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>

                <div class="form-group hide" id="cadreAnnexe">
                    <div class="col-sm-12 col-md-12 ">
                        <label>Annexes</label>
                        <select class="form-control selEmpty" id="annexe">
                            <?php  foreach ( $annexes as $annexe) {?>
                                <option value="<?=$annexe->idAnnexe?>" ><?= $annexe->nomAnnexe." / ".$annexe->adresseAnnexe?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 col-md-12 ">
                        <button type="button" class=" col-md-12 btn btn-primary pull-right addUtilisateur" id="addUtilisateur">Enregistrer</button>
                        <button type="button" class=" col-md-12 col-sm-offset-1 btn btn-default pull-right hide" id="annulEditUtilisateur">Annuler</button>
                        <button type="button" class=" col-md-12 col-sm-offset-1 btn btn-danger pull-right addUtilisateur hide" id="validEditUtilisateur">Modifier</button>
                    </div>
                </div>
            </form>
        </div>
	</div>
	<div class="col-md-9">
		<h2>Liste des utilisateurs du BackOffice</h2>
        <div class="col-md-12 boxed">
            <div class="row">
                <div class="col-md-12">
                    <input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchbox">
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#listeEntreprise">
                        Consulter les entreprises
                    </button>
                    <button type="button" class="btn btn-primary pull-right margin-right-5" data-toggle="modal" data-target="#archiveUtilisateur">
                        Utilisateurs archivés
                    </button>
                </div>
            </div>
        

            <table class="table table-striped table-hover" id="tabUtilisateur">
                <thead>
                    <tr>
                        <th>Photo</th>
                        <th>Nom d'utilisateur</th>
                        <th>Groupe</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($utilisateurs as $utilisateur) { ?>
                    <tr id="<?=$utilisateur->idUtilisateur?>">
                        <td><img width="35" height="35" src="<?= IMG.'utilisateur/'.$utilisateur->imgUtilisateur ?>" alt="<?= $utilisateur->imgUtilisateur?>"></td>
                        <td>
                            <?= $utilisateur->nomUtilisateur." ".$utilisateur->prenomUtilisateur?>
                        </td>
                        <td>
                            <?= $utilisateur->lblTypeUtilisateur?>
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                    Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="pull-right dropdown-menu">
                                    <li><a href="#" class="editUtilisateur">Editer</a></li>
                                    <li><a href="#" class="deleteUtilisateur">Archiver</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
	</div>
</div>

<!-- Modal Archive -->
<div class="modal fade" id="archiveUtilisateur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Archives utilisateur</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-10">
						<label><input type="search" class="form-control" placeholder="Rechercher" id="searchArchiveUtilisateur"></label>
					</div>
				</div>
				<table class="table table-striped table-hover" id="tabArchiveUtilisateur" style="width: 100%;">
					<thead>
						<tr>
							<th></th>
							<th>Nom d'utilisateur</th>
							<th>Groupe</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>


<!-- Modal Entreprise -->
<div class="modal fade" id="listeEntreprise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Listes Entreprises</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-10">
						<label><input type="search" class="form-control" placeholder="Rechercher" id="searchEntreprise"></label>
					</div>
				</div>
				<table class="table table-striped table-hover" id="tabEntreprise" style="width: 100%;">
					<thead>
						<tr>
							<th>Entreprise</th>
							<th>Interlocuteur</th>
							<th>Téléphone</th>
							<th>Email</th>
							<th>Adresse</th>
							<th>etat</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>

