
<div class="col-md-9 col-lg-10 pageContaint">
    <!-- FORMULAIRE AJOUT DE SLIDE -->
    <h2>Ajouter un slide</h2>

    <div class="col-md-4">
        <div class="col-md-12 boxed">
            <form class="form-horizontal" enctype="multipart/form-data" id="formSlide" >

                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Titre</label>
                        <input type="text" class="form-control" placeholder="Titre" id="titreSlider" maxlength="150">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Description</label>
                        <textarea id="descriptionSlider" placeholder="Description" class="form-control"  maxlength="250"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Nom du lien</label>
                        <input type="text" class="form-control" placeholder="Nom du lien" id="nomLienSlider" maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Lien de l'article</label>
                        <input type="text" class="form-control" placeholder="Lien (URL)" id="lienSlider" maxlength="500">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Image</label>
                        <input type="file" id="imgSlider" class=" form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-primary col-md-4 pull-right " id="addSlider">Ajouter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-8">
        <div class="col-md-12 boxed">
            <h4 class="col-md-12 <?php if (empty($sliders)) { echo "hide"; };?>" id="msgSlide">Déplacer les slides pour changer l'ordre d'apparition</h4>
            <ol class="all-slides col-md-12">

                <?php
                foreach ($sliders as $slider) { ?>
                    <li id="<?=$slider->idSlider?>" class="slide col-md-12 mt10" draggable="true">
                        <div class="col-md-12 row">
                            <div class="col-md-3 row">
                                <img src="<?= IMG.'slider/'.$slider->imgSlider?>" class="col-md-12" width="100%">
                                <button type="button" class="btn btn-danger delSlider col-md-12">Supprimer</button>
                            </div>
                            <div class="col-md-9">
                                <h4><?=$slider->titreSlider?></h4>
                                <p><?=$slider->descriptionSlider?></p>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ol>
        </div>
    </div>

</div>




<!-- Modal Confirmation -->
<div class="modal fade" id="confirmSuppr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Attention</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <label>Êtes vous sûres de vouloir supprimer ce slide ?</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default col-md-3" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-danger col-md-3 confirmDelSlide">Supprimer</button>
            </div>

        </div>
    </div>
</div>
