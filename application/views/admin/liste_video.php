
<div class="col-md-9 col-lg-10">
	<h2>Gestion des vidéos</h2>

	<div class="col-md-12 boxed">
        <a href="<?=site_url("Videos/saisieVideo")?>" class="btn btn-primary" role="button">Nouvelle vidéo</a>
        <input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchbox">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#archiveVideo">
            Consulter les archives
        </button>
	</div>
    <div class="col-md-12 boxed">
        <table class="table table-striped table-hover" id="tabVideo">
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkAll"></th>
                    <th>Miniature</th>
                    <th style='width:40%'>Titre</th>
                    <th>Type Energie</th>
                    <th>Date de publication</th>
                    <th>Publier</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($videos as $video){
                    $date = explode('-', $video->dateCreation);
                    $video->dateCreation = $date[2].'/'.$date[1].'/'.$date[0]?>

                    <tr id="<?= $video->idVideo ?>">
                        <td>
                            <input class="checkVideo" type="checkbox"/>
                        </td>
                        <td>
                            <?php
                            $video->urlVideo = explode('/', $video->urlVideo);
                            $video->urlVideo = array_pop($video->urlVideo);
                            ?>

                            <img src="http://img.youtube.com/vi/<?= $video->urlVideo?>/0.jpg" alt="" width="70" height="auto"></td>

                        <td>
                            <h4><?= substr($video->titreVideo,0,50) ?><?php if (strlen($video->titreVideo) > 50) echo "..." ?></h4>
                            <p><?= substr($video->descriptionVideo,0,165) ?><?php if (strlen($video->descriptionVideo) > 165) echo "..." ?></p>
                        </td>
                        <td>
                            <ul>
                                <?php foreach ($video->typeEnergie as $typeEnergie){ ?>
                                  <li> <?=$typeEnergie->libelleTypeEnergie;?> </li>
                                <?php }?>
                            </ul>
                        </td>
                        <td><?= $video->dateCreation ?></td>
                        <td class="publishVideo">
                            <div class="checkbox" >
                                <input class="etatPublish" type="checkbox" <?php if ($video->etatVideo == 1) echo "checked";?>/>
                                <label></label>
                            </div>
                        </td>


                        <td>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                    Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="pull-right dropdown-menu">
                                    <li><a href="<?=site_url("Videos/saisieVideo/".$video->idVideo);?>">Editer</a></li>
                                    <li><a href="#" class="deleteVideo">Archiver</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
	<div class="btn-group dropup" role="group">
		<button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Sélection
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
            <li><a href="#" class="publishAllVideo" data-etat="1">Publier</a></li>
            <li><a href="#" class="publishAllVideo" data-etat="2">Dépublier</a></li>
            <li><a href="#" class="deleteAllVideo">Archiver</a></li>
		</ul>
	</div>
</div>



<!-- Modal Archive -->
<div class="modal fade" id="archiveVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Archives Fiches</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <label><input type="search" class="form-control" placeholder="Rechercher" id="searchArchiveVideo"></label>
                    </div>
                </div>
                <table class="table table-striped table-hover" id="tabArchiveVideo" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>Miniature</th>
                        <th>Date de publication</th>
                        <th width="35%">Titre</th>
                        <th>Domaine énergetique</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

