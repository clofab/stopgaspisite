
<div class="col-md-9 col-lg-10 pageContaint">

    <h2>Gestion des annexes départementales (préfectures)</h2>
    <div class="col-md-12 boxed">
        <a href="<?=site_url("Annexes/saisieAnnexe")?>" class="btn btn-primary" role="button">Nouvelle annexe</a>
        <input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchbox">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#archiveAnnexe">
            Consulter les archives
        </button>
    </div>
    <div class="col-md-12 boxed">
        <table class="table table-striped table-hover" id="tabAnnexe">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Tel</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($annexes as $annexe){ ?>
                <tr id="<?=$annexe->idAnnexe ?>">
                    <td>
                        <?=$annexe->nomAnnexe ?>
                    </td>
                    <td>
                        <?=$annexe->adresseAnnexe ?>
                    </td>
                    <td>
                        <?=$annexe->telAnnexe ?>
                    </td>
                    <td>
                        <?=$annexe->emailAnnexe ?>
                    </td>
                    <td>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul class="pull-right dropdown-menu">
                                <li><a href="<?=site_url("Annexes/saisieAnnexe/".$annexe->idAnnexe);?>" >Editer</a></li>
                                <li><a href="#" class="deleteAnnexe">Archiver</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Archive -->
<div class="modal fade" id="archiveAnnexe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="myModalLabel">Archives annexe</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-10">
						<label><input type="search" class="form-control" placeholder="Rechercher" id="searchArchiveAnnexe"></label>
					</div>
				</div>
				<table class="table table-striped table-hover" id="tabArchiveAnnexe" style="width: 100%;">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Adresse</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>
