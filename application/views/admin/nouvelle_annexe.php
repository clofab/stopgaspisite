<div class="col-md-9 col-lg-10 pageContaint">
	<h2>Créer une annexe départementale</h2>
    <div class="col-md-12 boxed">
        <form class="form-horizontal" id="formAnnexe" action="<?=site_url()."/Annexes/validSaisieAnnexe"?>" method="POST">
            <div class="form-group">
                <label class="col-sm-2 control-label">Nom de l'établissement</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Nom" id="nomAnnexe" name="nomAnnexe" value="<?= $annexe->nomAnnexe ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" placeholder="Email" id="emailAnnexe" name="emailAnnexe" value="<?= $annexe->emailAnnexe ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Téléphone</label>
                <div class="col-sm-8">
                    <input type="tel" class="form-control" placeholder="Téléphone" id="telAnnexe" name="telAnnexe" value="<?= $annexe->telAnnexe ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Adresse</label>
                <div class="col-sm-6">
                    <input id="address" type="text" class="form-control" placeholder="Adresse" name="adresseAnnexe" value="<?= $annexe->adresseAnnexe ?>">
                </div>

            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-8">
                    <div id="geoSearch"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <input type="hidden" id="needMap" value="true">
                    <input name="latitude" id="latitude" type="hidden" value="<?= $annexe->latitudeAnnexe ?>"/>
                    <input name="longitude" id="longitude" type="hidden" value="<?= $annexe->longitudeAnnexe ?>"/>
                    <input name="idAnnexe" id="idAnnexe" type="hidden" value="<?= $annexe->idAnnexe ?>"/>
                
                    <button class="btn btn-primary" role="button" name="validVideo" type="submit" id="validAnnexe">Enregistrer</button>
                	<a href="<?=site_url()."/Annexes/indexAnnexe"?>" class="btn btn-default" role="button" >Annuler</a>
                </div>
            </div>

        </form>
    </div>
</div>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFJeeQ-EdkdJ3XoWltrvOLYjRm4sBA5bQ" type="text/javascript"></script>