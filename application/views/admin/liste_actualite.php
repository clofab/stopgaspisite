
<div class="col-md-9 col-lg-10">
	<h2>Actualités</h2>

    <div class="col-md-12 boxed">
        <a href="<?=site_url("Actualite/saisieActu")?>" class="btn btn-primary" role="button">Nouvelle actualité</a>
        <input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchbox">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#archivActu">
            Consulter les archives
        </button>
    </div>

	<div class="col-md-12 boxed">
        <table class="table table-striped table-hover" id="tabActu">
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkAll"></th>
                    <th>Image</th>
                    <th>Titre</th>
                    <th>Date de publication</th>
                    <th>Publier</th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($actus as $actu) {
                    $date = explode('-', $actu->dateCreation);
                    $actu->dateCreation = $date[2].'/'.$date[1].'/'.$date[0]?>

                    <tr id="<?= $actu->idActu;?>">
                        <td style="width:5%"><input class="checkActu" type="checkbox"></td>
                        <td style="width:10%"><img width="32" height="auto" src="<?= IMG.'actu/'.$actu->imageActu ?>" alt="<?= $actu->titre?>"></td>
                        <td style="width:30%"><?= $actu->titre ?></td>
                        <td style="width:10%"><?= $actu->dateCreation?></td>
                        <td style="width:5%" class="publishActu">
                            <div class="checkbox">
                                <input type="checkbox" class="etatPublish" <?php if ($actu->etatActu == 1) echo "checked" ?>>
                                <label></label>
                            </div>
                        </td>
                        <td style="width:5%">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                    Action
                                    <span class="caret"></span>
                                </button>
                                <ul class="pull-right dropdown-menu">
                                    <li><a href="<?=site_url("Actualite/saisieActu/".$actu->idActu);?>">Editer</a></li>
                                    <li><a href="#" class="deleteActu" >Archiver</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

	<div class="btn-group dropup" role="group">
		<button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Sélection
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="#" class="publishAllActu" data-etat="1">Publier</a></li>
			<li><a href="#" class="publishAllActu" data-etat="2">Dépublier</a></li>
			<li><a href="#" class="deleteAllActu">Archiver</a></li>
			<input type="hidden" value="0" id="typeActu">
		</ul>
	</div>

</div>

<!-- Modal Archive -->
<div class="modal fade" id="archivActu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Archives actualités</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-10">
						<input type="search" class="form-control input-auto-width" placeholder="Rechercher" id="searchArchiveActu">
					</div>
				</div>
				<table class="table table-striped table-hover" id="tabArchiveActu" style="width: 100%;">
					<thead>
					<tr>
						<th></th>
						<th>Titre</th>
						<th>Date de publication</th>
						<th></th>

					</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>

