<?php $typeEnergie = $typeEnergie[0] ;?>



<div class="col-md-9 col-lg-10">
    <h2>Domaine énergétique : <?=$typeEnergie->libelleTypeEnergie ?></h2>
    <div class="boxed col-md-12">
        <form class="form-horizontal" action="<?=site_url()."/GestionSite/validTypeEnergie"?>" method="POST" id="formEnergie" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Intitulé</label>
                <div class="col-sm-8">
                    <input type="text" name="titreEnergie" class="form-control" placeholder="Titre" value="<?=$typeEnergie->libelleTypeEnergie ?>" maxlength="150">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Accroche <span>(Page d'accueil)</span></label>
                <div class="col-sm-8">
                    <input type="text" name="accrocheEnergie" class="form-control" placeholder="Description" value="<?=$typeEnergie->accrocheTypeEnergie ?>" maxlength="500">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description courte <span>(moteurs de recherche)</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control " name="descriptionEnergie" class="description" rows="4" maxlength="1000"><?=$typeEnergie->descriptionTypeEnergie ?></textarea>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">Contenu</label>
                <div class="col-sm-8">
                    <textarea class="form-control description" name="contenuEnergie" rows="18"><?=$typeEnergie->contenuTypeEnergie ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <button class="btn btn-primary" role="button">Enregistrer</button>
                    <input type="hidden" value="<?=$typeEnergie->idTypeEnergie ?>" name="idTypeEnergie">
                    <input type="hidden" value="<?=$typeEnergie->imgTypeEnergie ?>" name="lastImage">
                    <input type="hidden" name="etatImage" value="0">
                </div>
            </div>
        </form>
    </div>
</div>

