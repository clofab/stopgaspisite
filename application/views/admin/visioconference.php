<div class="col-md-9 col-lg-10" id="listeVisio">
	<h2>Liste des appels disponibles</h2>
	<div id="appelsVisio"></div>
</div>

<div class="col-md-9 col-lg-10" id="cadreVisio" style="display:none;">
	<h2>Visioconférence</h2>
	<div id="errorVisio"></div>
	
	<div class="row boxed">
	    <div class="col-sm-8"><h3 id="nomVisio"></h3><div id="remoteVideo" style="width:100%;"></div></div>
	    <div class="col-sm-4">
	    	<h3>Votre caméra</h3>
	    	<video id="localVideo" autoplay muted style="width:100%;"></video>
	    	<div class="row text-center">
	    		<button id="hangupButton" class="btn btn-danger" style="margin:15px auto;z-index:999;">Raccrocher</button>
	    	</div>
	    </div>
	</div>
	

</div>

