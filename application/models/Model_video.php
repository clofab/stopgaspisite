<?php

class model_video extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // SELECTION TOUTES LES VIDEOS
    public function selectVideos($archives = false)
    {

        $this->db->from('video ');

        if($archives == false){
            $this->db->where('etatVideo != ',0);
        }else{
            $this->db->where('etatVideo',0);
        }

        $this->db->order_by("idVideo", "desc");
        $results = $this->db->get()->result();

        foreach($results as $result){
            // récupération des types energies
            $this->db->select('te.idTypeEnergie,te.libelleTypeEnergie');
            $this->db->from('video v');
            $this->db->join('video_type_energie vte', 'v.idVideo = vte.idVideo');
            $this->db->join('type_energie te', 'vte.idTypeEnergie = te.idTypeEnergie');
            $this->db->where('v.idVideo',$result->idVideo);
            $typeEnergie = $this->db->get()->result();

            $result->typeEnergie = $typeEnergie;
        }

        return $results;
    }

     // SELECTIONNE UNE VIDEO
    public function selectVideo($idVideo)
    {
        $this->db->from('video v');
        $this->db->join('utilisateur u', 'v.idUtilisateur = u.idUtilisateur');
        $this->db->where('idVideo',$idVideo);
        $result = $this->db->get()->result();

        // récupération des types energies
        $this->db->select('te.idTypeEnergie,te.libelleTypeEnergie');
        $this->db->from('video v');
        $this->db->join('video_type_energie vte', 'v.idVideo = vte.idVideo');
        $this->db->join('type_energie te', 'vte.idTypeEnergie = te.idTypeEnergie');
        $this->db->where('v.idVideo',$idVideo);
        $typeEnergie = $this->db->get()->result();

        $result[0]->typeEnergie = $typeEnergie;

        return $result;
    }
}
