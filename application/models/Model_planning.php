<?php
class model_planning extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectConseiller($idUtilisateur = null, $idAnnexe = null){

        $this->db->from('utilisateur u');
        $this->db->join('annexe a', 'a.idAnnexe = u.idAnnexe');
        $this->db->where('u.etatUtilisateur',1);
        $this->db->where('u.idAnnexe != ',null);

        if($idUtilisateur != null){
            $this->db->where('idUtilisateur',$idUtilisateur);
        }

        if($idAnnexe != null or $idAnnexe > 0){
            $this->db->where('u.idAnnexe',$idAnnexe);
        }

        return $this->db->get()->result();
    }

    // RECUPERE LES INDISPO
    public function selectIndispo($idAnnexe,$idConseiller){

        $this->db->from('indisponibilite i');

        if($idAnnexe != 0 ){
            $this->db->join('annexe a', 'a.idAnnexe = i.idAnnexe');
        }

        if($idConseiller != 0){
            $this->db->join('utilisateur u', 'u.idUtilisateur = i.idUtilisateur');
        }

        $this->db->where('i.idAnnexe',$idAnnexe);
        $this->db->where('i.etatIndispo',1);

        return $this->db->get()->result();
    }
    
    
    public function selectRdvConseiller($idUtilisateur = null, $idRdv = null, $idAnnexe = null, $date_deb = null, $date_fin = null){
        
        $this->db->from('rendez_vous rdv');
        $this->db->join('entreprise e', 'rdv.idEntreprise = e.idEntreprise');
        $this->db->join('utilisateur u', 'rdv.idUtilisateur = u.idUtilisateur');
        
        // SI SELON LE CONSEILLER
        if($idUtilisateur != null){
            $this->db->where('rdv.idUtilisateur',$idUtilisateur);
        }       
        
        // SI SELON L'ID RDV
        if($idRdv != null){
           $this->db->where('idRdv',$idRdv);
        }
        
        // SI SELON SELECT ANNEXE [GESTION DES AGENDAS]
        if($idAnnexe != null or $idAnnexe > 0){
           $this->db->where('idAnnexe',$idAnnexe);
        }
        
        // SI VERIF INDISPO
        if($date_deb != null){
            $this->db->where('plageHorraire >=', $date_deb);
            $this->db->where('plageHorraire <=', $date_fin);
            $this->db->where('etatRdv <>', 0);
        }
        
        $this->db->order_by('etatRdv');
        $this->db->order_by('plageHorraire');
        $this->db->order_by('heure_deb');
        
        return $this->db->get()->result();
        
    }
    
    public function notifRdv($idUtilisateur){
          
        $this->db->from('rendez_vous');
        $this->db->where('idUtilisateur',$idUtilisateur);
        $this->db->where('etatRdv', 1);

        return $this->db->get()->result();
    }
    

}
    