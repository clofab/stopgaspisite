<?php
class model_fiche extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    //  SELECTIONNE LES FICHES NON ARCHIVER
    public function selectFiches($archives = false){

    	$this->db->from('fiche_conseil fc');
    	$this->db->join('utilisateur u', 'fc.idUtilisateur = u.idUtilisateur');

        if($archives == false){
            $this->db->where('etatFiche != ',0);
        }else{
            $this->db->where('etatFiche',0);
        }

        $this->db->order_by("idFiche", "desc");
        $this->db->order_by("dateCreation", "desc");

        $results = $this->db->get()->result();

        foreach($results as $result){
            // récupération des types energies
            $this->db->select('te.idTypeEnergie,te.libelleTypeEnergie');
            $this->db->from('fiche_conseil fc');
            $this->db->join('fiche_type_energie fte', 'fc.idFiche = fte.idFiche');
            $this->db->join('type_energie te', 'fte.idTypeEnergie = te.idTypeEnergie');
            $this->db->where('fc.idFiche',$result->idFiche);
            $typeEnergie = $this->db->get()->result();

            $result->typeEnergie = $typeEnergie;
        }

        return $results;
    }

    //  SELECTIONNE UNE FICHE SELON $idFiche
    public function selectFiche($idFiche){

    	$this->db->from('fiche_conseil fc');
        $this->db->join('utilisateur u', 'fc.idUtilisateur = u.idUtilisateur');
        $this->db->where('fc.idFiche', $idFiche);
        $result = $this->db->get()->result();

        // récupération des types energies
        $this->db->select('te.idTypeEnergie,te.libelleTypeEnergie');
        $this->db->from('fiche_conseil fc');
        $this->db->join('fiche_type_energie fte', 'fc.idFiche = fte.idFiche');
        $this->db->join('type_energie te', 'fte.idTypeEnergie = te.idTypeEnergie');
        $this->db->where('fc.idFiche',$idFiche);
        $typeEnergie = $this->db->get()->result();

        $result[0]->typeEnergie = $typeEnergie;
        return $result;
    }
    
    
    public function getFicheRss($nbLimit){
    	
    	$this->db->from('fiche_conseil fc');
    	$this->db->join('utilisateur u', 'fc.idUtilisateur = u.idUtilisateur');
        $this->db->where('fc.etatFiche', 1);
        $this->db->limit($nbLimit);

        return $this->db->get()->result();
    }


}

?>