<?php

class model_front extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // VERIFICATION LOGIN ENTREPRISE
    public function connexEntreprise($entreprise)
    {
        $this->db->from('entreprise');
        $this->db->where('emailEntreprise', $entreprise->emailEntreprise);
        $result = $this->db->get()->result();

        foreach ($result as $key => $value) {
            
            if ($value->emailEntreprise == $entreprise->emailEntreprise AND $this->encrypt->decode($value->mdpEntreprise) == $entreprise->mdpEntreprise AND $value->etatEntreprise == 1) {

                $tabAcces = array(
                    'Entreprise' => true,
                    'idEntreprise' => $value->idEntreprise,
                    'nomEntreprise' => $value->nomEntreprise,
                    'emailEntreprise' => $value->emailEntreprise,
                    'interlocuteur' => $value->interlocuteur,
                    'longitudeEntreprise' => $value->longitudeEntreprise,
                    'latitudeEntreprise' => $value->latitudeEntreprise,
                );
                $this->session->set_userdata($tabAcces);

                return $value;
            }
        }
    }
    
	public function verifEmail($email){

		$this->db->from('entreprise');
		$this->db->where('emailEntreprise', $email);
		$result = $this->db->get()->result();
		return $result;
	}

    // ON RECUPERE L'ANNEXE LA PLUS PROCHE DE L'ENTREPRISE
    public function selectAnnexeProche($longitude,$latitude){

        $this->db->select('idAnnexe, nomAnnexe, ( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( latitudeAnnexe ) ) * cos( radians( longitudeAnnexe ) - radians(-'.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitudeAnnexe ) ) ) ) AS distance');
        $this->db->from('annexe');
        
        $this->db->having("distance < 1000");
        $this->db->where('etatAnnexe',1);
        $this->db->order_by("distance");

        return $this->db->get()->result();
    }

    public function selectPlage($idAnnexe){

        $this->db->from('utilisateur u');
        $this->db->join('planning p', 'p.idUtilisateur = u.idUtilisateur');
        $this->db->where('u.idAnnexe', $idAnnexe);
        $this->db->where('u.etatUtilisateur',1);
        return $this->db->get()->result();
    }

    public function selectIndispo($idAnnexe){
        
        $this->db->from('indisponibilite i');
        $this->db->join('utilisateur u', 'u.idUtilisateur = i.idUtilisateur','left');
 
        $where = '(i.idAnnexe="0" or i.idAnnexe ='.$idAnnexe.' )';
        $this->db->where($where);
        $this->db->where('i.etatIndispo',1);
        return $this->db->get()->result();

    }
    
    public function selectRdvAnnexe($idAnnexe){
        
        $this->db->select('*, COUNT(plageHorraire) as nbPlage');
        $this->db->from('rendez_vous rdv');
        $this->db->join('utilisateur u', 'u.idUtilisateur = rdv.idUtilisateur');
        $this->db->join('annexe a', 'a.idAnnexe = u.idAnnexe');

        $this->db->where('a.idAnnexe',$idAnnexe);
        $this->db->where('rdv.etatRdv',1);
        $this->db->group_by('plageHorraire'); 
        $this->db->group_by('rdv.idUtilisateur'); 

        return $this->db->get()->result();

    }


    // RECUPERE TOUS LES TYPE ENERGIE AVEC LEUR CONTENU(Fiche & Video)
    public function selectAllEnergie(){

        $this->db->from('type_energie te');
        $typeEnergies = $this->db->get()->result();
        $i = 0;
        
        foreach ($typeEnergies as $typeEnergie){

            // ON RECUPERE LES VIDEOS ASSOCIÉE AUX TYPE ENERGETIQUE
            $this->db->from('video_type_energie vte');
            $this->db->join('video v', 'vte.idVideo= v.idVideo');
            $this->db->where('v.etatVideo',1);
            $this->db->where('vte.idTypeEnergie',$typeEnergie->idTypeEnergie);
            $typeEnergies[$i]->video = $this->db->get()->result();

            // ON RECUPERE LES FICHES ASSOCIÉE AUX TYPE ENERGETIQUE
            $this->db->from('fiche_type_energie fte');
            $this->db->join('fiche_conseil f', 'fte.idFiche= f.idFiche');
            $this->db->join('utilisateur u', 'f.idUtilisateur= u.idUtilisateur');
            $this->db->where('f.etatFiche',1);
            $this->db->where('fte.idTypeEnergie',$typeEnergie->idTypeEnergie);
            $this->db->order_by("dateCreation", "desc");
            $typeEnergies[$i]->fiche = $this->db->get()->result();
            
            $y = 0;
            // SI UNE FICHE POSSEDE PLUSIEURS DOMAINE ENERGETIQUE, ALORS ON LES RECUPERE
            foreach ($typeEnergies[$i]->fiche as $fiche){
                
                $this->db->select('fte.idTypeEnergie,libelleTypeEnergie');
                $this->db->from('fiche_type_energie fte');
                $this->db->join('type_energie te', 'te.idTypeEnergie= fte.idTypeEnergie');
                $this->db->where('idFiche',$fiche->idFiche);
                $typeEnergies[$i]->fiche[$y]->typeEnergetiques = $this->db->get()->result(); 
                $y++;
            }
                

            $i++;
        }

        return $typeEnergies;
    }
    
    public function selectEnergie($idTypeEnergie){
        
        $this->db->from('type_energie te');
        $this->db->where('te.idTypeEnergie',$idTypeEnergie);
        $typeEnergie = $this->db->get()->result()[0];
        
        // ON RECUPERE LES VIDEOS ASSOCIÉE AUX TYPE ENERGETIQUE
        $this->db->from('video_type_energie vte');
        $this->db->join('video v', 'vte.idVideo= v.idVideo');
        $this->db->join('utilisateur u', 'v.idUtilisateur = u.idUtilisateur');
        $this->db->where('v.etatVideo',1);
        $this->db->where('vte.idTypeEnergie',$typeEnergie->idTypeEnergie);
        $this->db->order_by("dateCreation", "desc");
        $typeEnergie->videos = $this->db->get()->result();

        // ON RECUPERE LES FICHES ASSOCIÉE AUX TYPE ENERGETIQUE
        $this->db->from('fiche_type_energie fte');
        $this->db->join('fiche_conseil f', 'fte.idFiche= f.idFiche');
        $this->db->join('utilisateur u', 'f.idUtilisateur = u.idUtilisateur');
        $this->db->where('f.etatFiche',1);
        $this->db->where('fte.idTypeEnergie',$typeEnergie->idTypeEnergie);
        $this->db->order_by("dateCreation", "desc");
        $typeEnergie->fiches = $this->db->get()->result();

        return $typeEnergie;
        
    }
    
    public function selectRdv($idEntreprise){
        
        $this->db->from('rendez_vous');
        $this->db->where('idEntreprise',$idEntreprise);
        $this->db->where('etatRdv <>',0);
        
        return $this->db->get()->result();
    }

    // SELECTIONNE LE CONTENU DE LA PAGE D'ACCUEIL
    public function selectContenu(){
        
        $this->db->from('contenu');
        $this->db->where('idContenu',1);
        
        return $this->db->get()->result()[0];
    }
    
    
    //  COMPTE LE NOMBRE TOTAL DE LIGNE
    public function nbActu() {
        
        $this->db->where('pageConseil', null);
        $this->db->where('etatActu', 1);
        $this->db->from('actualites');
        return $this->db->count_all_results();
    }


    // GESTION PAGINATIONS ACTUALITES
    public function pagination_actu($limit, $start) {
        
        $this->db->limit($limit, $start);
        
        $this->db->from('actualites a');
        $this->db->join('utilisateur u', 'a.idUtilisateur = u.idUtilisateur');
        $this->db->where('a.pageConseil', null);
        $this->db->where('a.etatActu', 1);
        $this->db->order_by("a.dateCreation","desc");

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    
    public function selectIndispoConseiller($idConseiller,$dateRdv,$dateFinRdv){
        
        $dateRdv = explode(" ",$dateRdv);
        $dateDeb = $dateRdv[0];    
        $heureDeb = $dateRdv[1];    
        
        $dateFinRdv = explode(" ",$dateFinRdv);
        $dateFin = $dateFinRdv[0];
        $heureFin = $dateFinRdv[1];
        
        $this->db->from('indisponibilite');
        
        $this->db->where('idUtilisateur',$idConseiller);
        $this->db->where('dateDebutIndispo',$dateDeb);
        $this->db->where('heureDebutIndispo >=',$heureDeb);
        $this->db->where('heureDebutIndispo <=',$heureFin);
        
        $this->db->where('etatIndispo',1);

        return $this->db->get()->result();
    }
  
    // RECUPERE INFOS SUR ENTREPRISE 
    public function selectEntreprise($idEntreprise) {
        
        $this->db->from('entreprise');
        $this->db->where('idEntreprise',$idEntreprise);
        $data = $this->db->get()->result();
        
        $this->db->from('rendez_vous rdv');
        $this->db->where('rdv.idEntreprise',$idEntreprise);
        $this->db->join('utilisateur u', 'rdv.idUtilisateur = u.idUtilisateur');
        $this->db->join('annexe a', 'a.idAnnexe = u.idAnnexe');
        $data[0]->rdv = $this->db->get()->result();
        
        return $data;
        
    }

}