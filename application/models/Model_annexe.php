<?php

class model_annexe extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // RECUPERE LES UTILISATEUR RELIE A L'ANNEXE QUE L'ON SOUHAITE SUPPRIMÉ
    public function verifAnnexeUti($idAnnexe){

        $this->db->from('utilisateur');
        $this->db->where('etatUtilisateur !=', 0);
        $this->db->where('idAnnexe', $idAnnexe);

        return $this->db->get()->result();
    }
}
