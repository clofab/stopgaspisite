<?php
class model_actualite extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //  SELECTIONNE L'ACTUALITE SELON $idActu
    public function selectActu($idActu){

    	$this->db->from('actualites a');
    	$this->db->join('utilisateur u', 'a.idUtilisateur = u.idUtilisateur');
        $this->db->where('a.idActu', $idActu);

        return $this->db->get()->result();
    }

    public function selectActus($archive = false,$pageConseil = null, $publier = false){

        $this->db->from('actualites a');
        $this->db->join('utilisateur u', 'a.idUtilisateur = u.idUtilisateur');

        //  SI ACTU OU PAGE INFO
        if($pageConseil == null){
            $this->db->where('a.pageConseil', null);
        }else{
            $this->db->where('a.pageConseil >=', 0);
        }

        //  SI ARCHIVE OU NON
        if ($archive == false){
            $this->db->where('a.etatActu !=', 0);
        }else{
            $this->db->where('a.etatActu', 0);
        }
        
        //  SI PUBLIER OU NON
        if ($publier == true){
            $this->db->where('a.etatActu', 1);
        }
        
        $this->db->order_by('a.idActu','desc');
        return $this->db->get()->result();
    }

    public function getActuRss($nbLimit){
    	
    	$this->db->from('actualites a');
    	$this->db->join('utilisateur u', 'a.idUtilisateur = u.idUtilisateur');
        $this->db->where('a.etatActu', 1);
        $this->db->where('a.pageConseil', NULL);
        $this->db->limit($nbLimit);

        return $this->db->get()->result();
    }


//    public function deleteActu($idActu){
//
//        // UPDATE ETAT = 2
//        $this->db->where('idActu', $idActu);
//        $this->db->update('actualites', array('etat'=>'2'));
//
//        // INSERT ARCHIVE ACTU
//        $tabInsert = array('idActu' => $idActu,'idUserAdmin' => '15','dateSuppression' => date("Y-m-d H:i:s"));
//        $this->db->insert('archive_actu', $tabInsert);
//    }

}

?>