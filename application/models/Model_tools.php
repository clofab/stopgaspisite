<?php
class model_tools extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Select toutes les données de $table
    public function selectAll($table)
    {
        $this->db->from($table);
        $result = $this->db->get()->result();

        return $result;
    }

    // SELECT AVEC ORDER BY
    public function selectOrder($table,$colOrder,$ordre){
        $this->db->from($table);
        $this->db->order_by($colOrder,$ordre);
        $result = $this->db->get()->result();

        return $result;
    }

    // Select données de $table selon le WHERE
    public function selectWhere($table,$where,$condition)
    {
        $this->db->from($table);
        $this->db->where($where, $condition);

        return $this->db->get()->result();
    }

    // Delete selon l'id
    public function delete($id,$nameId,$table)
    {
        $this->db->delete($table, array($nameId => $id));
    }

    // Insert selon array et table
    public function insert($tabInsert,$table)
    {
        $this->db->insert($table, $tabInsert);
        return $this->db->insert_id();
    }
 
    // Update les données dans $tabUpdate selon $idWhere
    public function update($tabUpdate,$nameWhere,$idWhere,$table){

        $this->db->where($nameWhere, $idWhere);
        $this->db->update($table, $tabUpdate); 
    }

}

?>


