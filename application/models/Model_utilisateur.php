<?php
class model_utilisateur extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    // VERIFICATION LOGIN
	public function connexAdmin($utilisateur,$idUtilisateur = false)
	{
		$this->db->from('utilisateur u');
		$this->db->join('type_utilisateur tu', 'tu.idTypeUtilisateur = u.idTypeUtilisateur');
		$this->db->where('u.emailUtilisateur', $utilisateur->email);
		$result = $this->db->get()->result();
		$i =0;
		$dataUtilisateur;
		foreach ($result as $key => $value) {

			if ($value->emailUtilisateur == $utilisateur->email AND $this->encrypt->decode($value->mdpUtilisateur) == $utilisateur->password AND $value->etatUtilisateur == 1) {
				return $result[$i];
			}
			$i++;
		}
		
		return null;
	}

	// Select toutes les données d'un utilisateur
	public function selectUtilisateur($id = null,$archive = false ,$idTypeUtilisateur = null )
	{
		$this->db->from('utilisateur u');
		$this->db->join('type_utilisateur tu', 'tu.idTypeUtilisateur = u.idTypeUtilisateur');

		// SI ARCHIVER OU NON
		if($archive == false){
			$this->db->where('u.etatUtilisateur != ',0);
		}else{
			$this->db->where('u.etatUtilisateur',0);
		}

		// SI SELON UN ID
		if($id != null){
			$this->db->where('u.idUtilisateur = ',$id);
		}

		// SI SELON LE TYPE UTILISATEUR
		if($idTypeUtilisateur != null){
			$this->db->where('u.idTypeUtilisateur = ',$idTypeUtilisateur);
		}
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function verifEmail($email,$idUtilisateur){

		$this->db->from('utilisateur');
		$this->db->where('emailUtilisateur', $email);
		$this->db->where('idUtilisateur != ', $idUtilisateur);
		$result = $this->db->get()->result();
		return $result;
	}

	public function updateProfil($data){

		$this->db->where('idUtilisateur',$this->session->userdata('idUtilisateur'));
		$this->db->update('utilisateur', $data);
		$this->db->from('utilisateur u');
		$this->db->join('type_utilisateur tu', 'tu.idTypeUtilisateur = u.idTypeUtilisateur');
		$this->db->where('u.idUtilisateur', $this->session->userdata('idUtilisateur'));
		$result = $this->db->get()->result();

		foreach ($result as $key => $value) {

			$tabAcces = array(
				'Admin' => true,
				'idUtilisateur' => $value->idUtilisateur,
				'imgAvatar' =>'utilisateur/'.$value->imgUtilisateur,
				'nom' => $value->nomUtilisateur,
				'prenom' => $value->prenomUtilisateur,
				'email' => $value->emailUtilisateur,
			);
			$this->session->set_userdata($tabAcces);
		}
	}



}

?>