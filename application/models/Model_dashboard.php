<?php
class model_dashboard extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    // Select toutes les données d'un utilisateur
	public function selectUtilisateur()
	{
	   
	    $this->db->select('lblTypeUtilisateur,count(*) as nbType');
		$this->db->from('utilisateur u');
		$this->db->join('type_utilisateur tu', 'tu.idTypeUtilisateur = u.idTypeUtilisateur');
    	$this->db->where('u.etatUtilisateur',1);
    	$this->db->group_by('lblTypeUtilisateur');

		$result = $this->db->get()->result();
		return $result;
	}
    
    //Donne le nombre de rendez-vous par état
    public function countNbRendezVous(){
        date_default_timezone_set('Europe/Paris');
        $dateNow = date( 'Y-m-d H:i:s');
        
        //$this->db->select('etatRdv, count(etatRdv) as nbRdv');
        $this->db->from('rendez_vous rdv');
        //$this->db->group_by('etatRdv');

		$result = $this->db->get()->result();
		
        $data['fini'] = 0;
        $data['attente_rdv'] = 0;
        $data['attente_cr'] = 0;
     
		foreach ($result as $value) {
		    if($value->etatRdv == 1 or ($value->etatRdv == 2 and strtotime($value->plageHorraire) > strtotime($dateNow))) $data['attente_rdv']++;
            if($value->etatRdv == 2 and strtotime($value->plageHorraire) < strtotime($dateNow)) $data['attente_cr']++;
            if($value->etatRdv == 0 ) $data['fini']++;
            
		}
		return $data;
    }
    
    //Donne le nombre d'actualités par état
    public function countNbActusEtat(){
        $this->db->select('etatActu, count(etatActu) as nbActu');
        $this->db->from('actualites actus');
        $this->db->group_by('etatActu');

		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$value->etatActu] = $value->nbActu;
		}
		return $data;
    }
    
    //Donne le nombre d'actualités par mois
    public function countNbActusMois(){
        $this->db->select('MONTH(dateCreation) as mois,YEAR(dateCreation) as annee, count(idActu) as nbActu');
        $this->db->from('actualites actus');
        $this->db->group_by('MONTH(dateCreation),YEAR(dateCreation)');
        $mois = Array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août","Septembre", "Octobre", "Novembre", "Décembre");
                                               
		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$mois[$value->mois]." ".$value->annee] = $value->nbActu;
		}
		return $data;
    }
    
    //Donne le nombre de fiches conseils par état
    public function countNbFichesEtat(){
        $this->db->select('etatFiche, count(etatFiche) as nbFiche');
        $this->db->from('fiche_conseil fiche');
        $this->db->group_by('etatFiche');

		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$value->etatFiche] = $value->nbFiche;
		}
		return $data;
    }
    
    //Donne le nombre de fiches conseils par mois
    public function countNbFichesMois(){
        $this->db->select('MONTH(dateCreation) as mois,YEAR(dateCreation) as annee, count(idFiche) as nbFiche');
        $this->db->from('fiche_conseil fiche');
        $this->db->group_by('MONTH(dateCreation),YEAR(dateCreation)');
        $mois = Array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août","Septembre", "Octobre", "Novembre", "Décembre");
                                               
		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$mois[$value->mois]." ".$value->annee] = $value->nbFiche;
		}
		return $data;
    }
    
    //Donne le nombre de videos par état
    public function countNbVideosEtat(){
        $this->db->select('etatVideo, count(etatVideo) as nbVideo');
        $this->db->from('video v');
        $this->db->group_by('etatVideo');

		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$value->etatVideo] = $value->nbVideo;
		}
		return $data;
    }
    
    //Donne le nombre de videos par mois
    public function countNbVideosMois(){
        $this->db->select('MONTH(dateCreation) as mois,YEAR(dateCreation) as annee, count(idVideo) as nbVideo');
        $this->db->from('video v');
        $this->db->group_by('MONTH(dateCreation),YEAR(dateCreation)');
        $mois = Array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août","Septembre", "Octobre", "Novembre", "Décembre");
                                               
		$result = $this->db->get()->result();
		$data = array();
		foreach ($result as $value) {
		    $data[$mois[$value->mois]." ".$value->annee] = $value->nbVideo;
		}
		return $data;
    }
    
}