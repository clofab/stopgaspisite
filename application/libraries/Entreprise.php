<?php


class Entreprise
{
    public $nomEntreprise;
    public $interlocuteur;
    public $telEntreprise;
    public $emailEntreprise;
    public $mdpEntreprise;
    public $adresseEntreprise;
    public $longitudeEntreprise;
    public $latitudeEntreprise;
    public $etatEntreprise;
}