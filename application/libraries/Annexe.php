<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Annexe 
{
    public $idAnnexe;
    public $nomAnnexe;
    public $adresseAnnexe;
    public $latitudeAnnexe;
    public $longitudeAnnexe;
    public $telAnnexe;
    public $emailAnnexe;

}