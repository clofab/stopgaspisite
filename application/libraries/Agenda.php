<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Agenda
{
    public $idAgenda;
    public $lundi;
    public $mardi;
    public $mercredi;
    public $jeudi;
    public $vendredi;
    public $intitule;
    public $dateDebut;
    public $dateFin;
    public $heureDebut;
    public $heureFin;
    public $heureMax;
    public $idAnnexe;
    public $idConseiller;

}