<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Utilisateur
{

    public $idUtilisateur;
    public $nom;
    public $prenom;
    public $email;
    public $password;
    public $imageUtilisateur;
    public $CI;
    public $idAnnexe;

    public function __construct()
    {
        $this->CI =& get_instance(); // CHARGEMENT DES METHODES DU FRAMEWORK
    }

    //   UPLOAD D'IMAGE
    public function uploadImg($dossierImg,$filePost){

        $config['upload_path']     = './assets/files/images/'.$dossierImg.'/';
        $config['allowed_types']   = 'gif|jpg|png|jpeg';

        // ACCES AU RESSOURCES CODEIGNITER

        $this->CI->load->library('upload', $config);
        $this->CI->upload->do_upload($filePost);

        $upload_data = $this->CI->upload->data();
        $file_name = $upload_data['file_name'];
        return $file_name;
    }

    // SUPPRESSION D'UNE IMG
    public function deleteImg($nameImg){
        $pathImg = 'assets/files/images/utilisateur/'.$nameImg;

        if (file_exists($pathImg)){
            unlink($pathImg);
        }
    }

//    public function getDroit($utilisateur){ // ATTRIBUTION DES DROITS APRES CONNEXION
//
//        if($utilisateur->idTypeUtilisateur == 1){ // SI WEBMASTER
//
//            $tabAcces = array(
//                'Admin' => true,
//                'webMaster' => true,
//                'idWebmaster' => $utilisateur->idUtilisateur
//            );
//
//        }elseif($utilisateur->idTypeUtilisateur == 2){ // SI CONSEILLER
//
//            $tabAcces = array(
//                'Admin' => true,
//                'Conseiller' => true,
//                'idConseiller' => $utilisateur->idUtilisateur
//            );
//        }
//        $tabAcces['idUtilisateur'] = $utilisateur->idUtilisateur;
//        $this->CI->session->set_userdata($tabAcces);
//    }
}
