<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Video
{
    public $idVideo;
    public $titreVideo;
    public $urlVideo;
    public $descriptionVideo;
    public $typeEnergie;
    public $idTypeEnergie;
    public $libelleTypeEnergie;
    public $dateCreation;
    public $etatVideo;
    public $idUtilisateur;
}