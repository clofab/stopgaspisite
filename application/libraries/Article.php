<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article
{
    public $titre;

    public $description;
    public $datePublication;
    public $nom;
    public $prenom;
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance(); // CHARGEMENT DES METHODES DU FRAMEWORK
    }

    //   UPLOAD D'IMAGE
    public function uploadImg($dossierImg,$filePost){

        $config['upload_path']     = './assets/files/images/'.$dossierImg.'/';
        $config['allowed_types']   = 'gif|jpg|png|jpeg';

        // ACCES AU RESSOURCES CODEIGNITER

        $this->CI->load->library('upload', $config);
        $this->CI->upload->do_upload($filePost);

        $upload_data = $this->CI->upload->data();
        $file_name = $upload_data['file_name'];
        return $file_name;
    }

    // SUPPRESSION D'UNE IMG [ACTU]
    public function deleteImg($dossierImg,$nameImg){
        $pathImg = 'assets/files/images/'.$dossierImg.'/'.$nameImg;

        if (file_exists($pathImg)){
            unlink($pathImg);
        }
    }

}