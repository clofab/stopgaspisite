<?php

class Front extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_front');
        $this->load->model('model_fiche');
        $this->load->model('model_actualite');
        $this->load->model('model_annexe');
        $this->load->model('model_video');
        $this->load->library('Entreprise');
    }

    public function inscription(){
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/inscription');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }

    public function validInscription(){

        $email = $this->input->post('emailEntreprise');
        $nbEmail = $this->model_front->verifEmail($email);   // verifie si mail deja existant

        if (!empty($nbEmail)){ // Si email existe deja, erreur
            $data['msgError'] = "Attention email déja utilisé";
            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/inscription');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }else{

            $tabInsert= array(
                "nomEntreprise" => $this->input->post('nomEntreprise'),
                "interlocuteur" => $this->input->post('interlocuteur'),
                "telEntreprise" => $this->input->post('telEntreprise'),
                "emailEntreprise" => $this->input->post('emailEntreprise'),
                "mdpEntreprise" =>  $this->encrypt->encode($this->input->post('mdp')),
                "adresseEntreprise" => $this->input->post('adresseEntreprise'),
                "longitudeEntreprise" => $this->input->post('longitude'),
                "latitudeEntreprise" => $this->input->post('latitude'),
                "etatEntreprise" => 1, // A METTRE à 0 UNE FOIS EMAIL OK
            );
    
            $this->model_tools->insert($tabInsert,'entreprise');   // INSERTION EN BDD

//        $this->email->from('contact@stop-gaspi.fr', 'Contact Stop Gaspi');
//        $this->email->to($this->input->post('emailEntreprise'));
//
//        $this->email->subject('Confirmation d'inscription : StopGaspi);
//        $this->email->message('Lien de confirmation :');
//        $this->email->send();

          redirect('/Accueil/index');
        }
    }

    public function connexion($error=''){
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/_connexion');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }

    public function deconnexion(){
        $this->session->unset_userdata('Entreprise');
        $this->session->unset_userdata('idEntreprise');
        $this->session->unset_userdata('nomEntreprise');
        $this->session->unset_userdata('interlocuteur');
        $this->session->unset_userdata('longitudeEntreprise');
        $this->session->unset_userdata('latitudeEntreprise');
        redirect('/Accueil/index');
    }
    
    public function accesEntreprise()
    {
        $entreprise = new Entreprise();
        
        $entreprise->emailEntreprise = $this->input->post('emailEntreprise');
        $entreprise->mdpEntreprise = $this->input->post('mdpEntreprise');

        $entreprise = $this->model_front->connexEntreprise($entreprise);
        if(empty($entreprise)){
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
            $data['msgError'] = true;
            
            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/_connexion');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }else{
            redirect('/Accueil/index');
        }
    }

    public function visioconference(){
         if($this->session->userdata('Entreprise') == true){
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 

            $data['visio'] = true;
            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/visio');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }else{
           redirect('/Front/connexion');
        }
    }
    /**************************************************************************
     *                             PRISE DE RDV                               *
     *************************************************************************/

    // PAGE DE PRISE DE RDV
    public function priseRdv(){

        if($this->session->userdata('Entreprise') == true){

            $longitude = $this->session->userdata('longitudeEntreprise');
            $latitude = $this->session->userdata('latitudeEntreprise');
            
            // SI LONGITUTE ET LATITUDE RENSEIGNEZ ALORS ON RECUPERES L'ANNEXE LA PLUS PROCHE
            if(!empty($longitude) and !empty($latitude)){
                $data['annexes'] = $this->model_front->selectAnnexeProche($longitude,$latitude);
            }else{
                $data['annexes'] = $this->model_tools->selectWhere('annexe','etatAnnexe',1);
            }
           
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 

            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/rendez_vous');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }else{
           redirect('/Front/connexion');
        }
    }

    // RECUPERE LES HORRAIRES POUR UNE ANNEXE
    public function getHorraire(){

        $idAnnexe = $this->input->post('idAnnexe');
       
        $data['plage']   =  $this->model_front->selectPlage($idAnnexe);
        $data['indispo'] =  $this->model_front->selectIndispo($idAnnexe);
        $data['rdv']     =  $this->model_front->selectRdvAnnexe($idAnnexe);
        
        echo json_encode($data);
    }
    
    
    //  VERIFICATION SI ENTREPRISE POSSEDE DEJA UN RDV
    public function verifRdv(){
        
        $idEntreprise = $this->session->userdata('idEntreprise');
        $data = $this->model_front->selectRdv($idEntreprise);
        
        echo json_encode($data);
    }
    
    
    //  ENREGISTREMENT D'UN RDV EN BASE
    public function saveRdv(){
        
        $dateRdv = $this->input->post('dateRdv');
 
        $tabRdv = array(
            'plageHorraire' => $dateRdv,
            'heure_deb' => $this->input->post('heureDeb'),
            'etatRdv' => 1,
            'idUtilisateur' => $this->input->post('idConseiller'),
            'idEntreprise' => $this->session->userdata('idEntreprise'),
        );
        
        $this->model_tools->insert($tabRdv,'rendez_vous');   // INSERTION EN BASE
        
        //   $this->email->from('contact@stop-gaspi.fr', 'Contact Stop Gaspi');
        //   $this->email->to($this->session->userdata('emailEntreprise'));
        //
        //   $this->email->subject('Confirmation de demande de rendez vous : StopGaspi');
        //   $this->email->message('Bonjour, nous vous confirmons que votre demande de rendez vous à bien été prise en compte, un conseiller vous contactera dans les plus bref delais');
        //   $this->email->send();
        
        echo true;
    }
    
    // VERIFIE DISPONIBILITE D'UNE PLAGE HORRAIRE
    public function verifDispo(){
        
        $dateRdv = $this->input->post('dateRdv');
        $dateFinRdv = $this->input->post('dateFinRdv');
        $idConseiller = $this->input->post('idConseiller');
        
        $data['rdv'] = $this->model_tools->selectWhere('rendez_vous','plageHorraire',$dateRdv);
        $data['indispo'] = $this->model_front->selectIndispoConseiller($idConseiller,$dateRdv,$dateFinRdv);
        
        echo json_encode($data);
    }

    // PAGE POUR UN DOMAINE ENERGETIQUE
    public function domaineEnergie($idTypeEnergie){
        $data['typeEnergie'] = $this->model_front->selectEnergie($idTypeEnergie);
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/domaine_energetique');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    //PAGE POUR UNE FICHE
    public function fiche($idFiche){
       
        $data['fiche'] = $this->model_fiche->selectFiche($idFiche)[0];
        $data['fiches'] = $this->model_fiche->selectFiches();
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/fiche_conseil');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    // PAGE POUR UNE VIDEO
    public function video($idVideo){
        $data['video'] = $this->model_video->selectVideo($idVideo)[0];
        $data['actus'] = $this->model_actualite->selectActus(false,null,true);
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
                
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/video');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    // PAGE DES ACTUALITES
    public function actualites(){

        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        // CONFIGURATION PAGINATION
        $this->load->library('pagination');
        
        $config = array();
        $config['base_url'] = URL.'/Front/actualites';
        $config["total_rows"] = $this->model_front->nbActu();
        $config["per_page"] = 6; // 6 ARTICLES PAR PAGE
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["actus"] = $this->model_front->pagination_actu($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        
        $this->pagination->initialize($config);
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/liste_actu');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    
    // PAGE POUR UNE ACTUALITE
    public function actualite($idActu){
        
        $data['actu'] = $this->model_actualite->selectActu($idActu)[0];
        $data['actus'] = $this->model_actualite->selectActus(false,null,true);
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site',$data);
        $this->load->view('site/_header_section');
        $this->load->view('site/page_actu');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    public function fiches(){
        
        $data['typeEnergies'] = $this->model_front->selectAllEnergie();
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
        
        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/liste_fiche');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }
    
    
    // ACCEDER A L'ESPACE ENTREPRISE
    public function espace_entreprise(){
        
        if($this->session->userdata('Entreprise') == true){
    
            $idEntreprise = $this->session->userdata('idEntreprise');
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
            $data['entreprise'] = $this->model_front->selectEntreprise($idEntreprise)[0];
            $data['entreprise']->mdpEntreprise = $this->encrypt->decode($data['entreprise']->mdpEntreprise);
            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/espace_entreprise');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }else{
           redirect('/Front/connexion');
        }
    }
    
    public function updateEntreprise(){
        
        $email = $this->input->post('emailEntreprise');
        $nbEmail = $this->model_front->verifEmail($email);   // verifie si mail deja existant
        $acces = false;
        
        // SI MAIL EXISTE MAIS QUE C'EST LE SIEN, ALORS OK
        if(!empty($nbEmail)){
            if($nbEmail[0]->emailEntreprise == $this->session->userdata('emailEntreprise')) {
               $acces = true;
            }
        }

        if (!empty($nbEmail) and $acces == false){ // Si email existe deja, erreur
            $idEntreprise = $this->session->userdata('idEntreprise');
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
            $data['entreprise'] = $this->model_front->selectEntreprise($idEntreprise)[0];
            $data['entreprise']->mdpEntreprise = $this->encrypt->decode($data['entreprise']->mdpEntreprise);
            $data['msgError'] = "Attention email déja utilisé";
            
            $this->load->view('site/_head_site');
            $this->load->view('site/_header_section',$data);
            $this->load->view('site/espace_entreprise');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');

        }else{

            $tabUpdate= array(
                "nomEntreprise" => $this->input->post('nomEntreprise'),
                "interlocuteur" => $this->input->post('interlocuteur'),
                "telEntreprise" => $this->input->post('telEntreprise'),
                "emailEntreprise" => $this->input->post('emailEntreprise'),
                "mdpEntreprise" =>  $this->encrypt->encode($this->input->post('mdp')),
                "adresseEntreprise" => $this->input->post('adresseEntreprise'),
                "longitudeEntreprise" => $this->input->post('longitude'),
                "latitudeEntreprise" => $this->input->post('latitude'),
            );
            
            
            $idEntreprise = $this->session->userdata('idEntreprise');
            $this->model_tools->update($tabUpdate,'idEntreprise',$idEntreprise,'entreprise');
        

            redirect('/Front/espace_entreprise','refresh');
        }

    }
            // PAGE POUR UNE PAGE INFOS
        public function page_info($idActu){
            
            $data['actu'] = $this->model_actualite->selectActu($idActu)[0];
            $data['actus'] = $this->model_actualite->selectActus(false,null,true);
            $data['pagesInfos'] = $this->model_actualite->selectActus(false,1,true); 
            
            $this->load->view('site/_head_site',$data);
            $this->load->view('site/_header_section');
            $this->load->view('site/page_actu');
            $this->load->view('site/_footer_section');
            $this->load->view('site/_footer_site');
        }
}
