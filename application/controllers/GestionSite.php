<?php

class Gestionsite extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_actualite');
        $this->load->library('Actu');
    }

    //  PAGE D'EDITION PRESENTATION DE LA PAGE ACCUEIL
    public function contenuAccueil(){

        if($this->session->userdata("gestionSite") == true) {

            $data['contenu'] = $this->model_tools->selectWhere('contenu','idContenu',1); // RECUPERE LE CONTENU POUR ID 1 => ACCUEIL
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');


            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/contenu_accueil');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    //  VALIDATION FORMULAIRE TEXTE PRESENTATION ACCUEIL
    public function validAccueil(){

        if($this->session->userdata("gestionSite") == true) {

            //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer
            $tabPres = array(
                'titreContenu' => $this->input->post('titrePres'),
                'descriptionContenu' => $this->input->post('descriptionPres'),
            );

            $this->model_tools->update($tabPres,'idContenu',1,'contenu'); // UPDATE DANS BDD
            redirect('/GestionSite/contenuAccueil', 'refresh');

        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    public function indexPageInfo(){

        if($this->session->userdata("gestionSite") == true) {

            $data['actus'] = $this->model_actualite->selectActus(false,1);
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_page');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    public function getArchivePage(){
        $data = $this->model_actualite->selectActus(true,1);
        echo json_encode($data);
    }


    // SAISIE D'UNE PAGE INFO
    public function saisiePage($idActu = null){

        $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

        if($idActu != null){    // EDITON
            $actu = $this->model_actualite->selectActu($idActu);
            $data['actu'] = $actu[0];
        }else{                  // AJOUT
            $data['actu'] = new Actu();
        }

        if($this->session->userdata('Admin') == true){
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/nouvelle_page');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // VALIDATION DE SAISIE D'UNE PAGE INFO
    public function validSaisiePage(){

        $actu = new Actu();

        $actu->idActu = $this->input->post('idActu');
        $etat = $this->input->post('checkEtat');
        $pageAccueil = $this->input->post('checkAccueil');
        $actu->etatActu = ( $etat != null ? 1 : 2);
        $actu->pageConseil = ( $pageAccueil != null ? 1 : 0);

        if( $actu->idActu != null){ // SI EDITION

            $lastImg = $this->input->post('lastImage');

            // SI PAS DE NOUVELLE IMAGE
            if($this->input->post('etatImage') == 1){
                $actu->imageActu = $lastImg;
            }else{
                $actu->imageActu = $actu->uploadImg("actu","fileActu");           //  UPLOAD IMAGE
                $actu->deleteImg('actu',$lastImg);                                // DELETE ANCIENNE IMAGE
            }

            //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer
            $tabActu = array(
                'titre' => $this->input->post('titreActu'),
                'resume' => $this->input->post('resumeActu'),
                'imageActu' => $actu->imageActu,
                'description' => $this->input->post('descriptionActu'),
                'etatActu' =>  $actu->etatActu,
                'pageConseil' => $actu->pageConseil, // INDIQUE QUE CECI EST UNE PAGE INFO, -> Si 1 = affichage en page d'info | sinon 0 | si null alors actu
            );

            $this->model_tools->update($tabActu,'idActu',$actu->idActu,'actualites'); // UPDATE DANS BDD
        }else{             // SINON CREATION

            $actu->imageActu = $actu->uploadImg("actu","fileActu");           //  UPLOAD IMAGE
            $tabActu = array(
                'idActu' => $this->input->post('idActu'),
                'titre' => $this->input->post('titreActu'),
                'resume' => $this->input->post('resumeActu'),
                'imageActu  ' => $actu->imageActu,
                'description' => $this->input->post('descriptionActu'),
                'dateCreation' => date("Y-m-d H:i:s"),
                'etatActu' =>  $actu->etatActu,
                'idUtilisateur' => $this->session->userdata('Admin'), // ID SESSION A METRE
                'pageConseil' => $actu->pageConseil, // INDIQUE QUE CECI EST UNE PAGE INFO, et NON UNE ACTU
            );
            $this->model_tools->insert($tabActu,'actualites');   // INSERTION EN BASE
        }

        redirect('/GestionSite/indexPageInfo/', 'refresh');
    }


    // PAGE D'EDITION TYPE ENERGIE
    public function indexTypeEnergie(){

        if($this->session->userdata("gestionSite") == true) {

            $idTypeEnergie =  intval($this->uri->segment(3));
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie'); // MENU VERTICALE
            $data['typeEnergie'] = $this->model_tools->selectWhere('type_energie','idTypeEnergie',$idTypeEnergie); // POUR FORMULAIRE

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/gestion_domaine');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // PAGE D'EDITION TYPE ENERGIE
    public function validTypeEnergie(){

        if($this->session->userdata("gestionSite") == true) {

            $idTypeEnergie =  $this->input->post('idTypeEnergie');

            //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer
            $tabEnergie = array(
                'libelleTypeEnergie' => $this->input->post('titreEnergie'),
                'accrocheTypeEnergie' => $this->input->post('accrocheEnergie'),
                'descriptionTypeEnergie' => $this->input->post('descriptionEnergie'),
                'contenuTypeEnergie' => $this->input->post('contenuEnergie'),
            );

            $this->model_tools->update($tabEnergie,'idTypeEnergie',$idTypeEnergie,'type_energie'); // UPDATE DANS BDD
            redirect('/GestionSite/indexTypeEnergie/'.$idTypeEnergie, 'refresh');
        }else{
            $this->load->view('admin/connex_admin');
        }



    }




}
    