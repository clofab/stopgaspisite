<?php

class Annexes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_annexe');
        $this->load->library('Annexe');
    }

    public function indexAnnexe()
    {
        $data['annexes'] = $this->model_tools->selectWhere('annexe','etatAnnexe !=',0);
        $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

        if($this->session->userdata('gestionAnnexe') == true){
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_annexe');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // SUPPRESSION D'UNE ANNEXE
    public function deleteAnnexe(){

        $idAnnexe = $this->input->post('id');
        $annexe = $this->model_annexe->verifAnnexeUti($idAnnexe);

        if(empty($annexe)){
            $tabEtat['etatAnnexe'] = 0;
            $this->model_tools->update($tabEtat,'idAnnexe',$idAnnexe,'annexe');
            echo true;
        }else{
            echo json_encode("annexe");
        }
    }

    // PAGE EDITION D'UNE ANNEXE
    public function saisieAnnexe($idAnnexe = null){

        $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

        if($idAnnexe != null){    // EDITON
            $annexe = $this->model_tools->selectWhere('annexe','idAnnexe',$idAnnexe);
            $data['annexe'] = $annexe[0];
        }else{                  // AJOUT
            $data['annexe'] = new Annexe();
        }

        if($this->session->userdata('gestionAnnexe') == true){
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/nouvelle_annexe');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // VALIDATION DE SAISIE D'UNE ANNEXE
    public function validSaisieAnnexe(){

        $annexe = new Annexe();
        $annexe->idAnnexe = $this->input->post('idAnnexe');

        //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer/modifier
        $tabAnnexe = array(
            'nomAnnexe' => $this->input->post('nomAnnexe'),
            'emailAnnexe' => $this->input->post('emailAnnexe'),
            'telAnnexe' =>  $this->input->post('telAnnexe'),
            'adresseAnnexe' => $this->input->post('adresseAnnexe'),
            'latitudeAnnexe' => $this->input->post('latitude'),
            'longitudeAnnexe' => $this->input->post('longitude'),
            'etatAnnexe' => 1
        );

        if( $annexe->idAnnexe != null){ // SI EDITION
            $this->model_tools->update($tabAnnexe,'idAnnexe',$annexe->idAnnexe,'annexe'); // UPDATE DANS BDD
        }else{             // SINON CREATION
            //$tabAnnexe['idAnnexe'] = $this->input->post('idAnnexe');
            $this->model_tools->insert($tabAnnexe,'annexe');   // INSERTION EN BASE
        }

        redirect('/Annexes/indexAnnexe/', 'refresh');

    }

    //RECUPERATION DES ARCHIVES
    public function getArchiveAnnexe(){
        $data = $this->model_tools->selectWhere('annexe','etatAnnexe',0);
        echo json_encode($data);
    }

    // REACTIVATION D'ARCHIVE DESACTIVER
    public function publishAnnexe(){
        $idAnnexe = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatAnnexe' => $etat);
        $this->model_tools->update($tabEtat,"idAnnexe",$idAnnexe,"Annexe");
        echo true;
    }

    // RÉCUPÈRE LES DONNÉES SUR UNE ANNEXE
    public function getAnnexe(){
        $idAnnexe = $this->input->post('id');
        $annexe = $this->model_tools->selectWhere('annexe','idAnnexe',$idAnnexe);
        echo json_encode($annexe);

    }

    // RÉCUPÈRE LES DONNÉES SUR TOUTES LES ANNEXES
    public function getAllAnnexes(){
        $data = $this->model_tools->selectWhere('annexe','etatAnnexe !=',0);
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        header('Content-Type: application/json');
        echo json_encode( $data);

    }



}