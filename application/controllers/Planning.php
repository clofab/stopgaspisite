<?php

class Planning extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_planning');
        $this->load->library('Agenda');
    }

    // ACCUEIL GESTION DES UTILISATEURS
    public function indexPlanning()
    {
        if ($this->session->userdata('gestionRdv') == true) {
            $data['annexes'] = $this->model_tools->selectWhere('annexe','etatAnnexe !=',0);
            $data['conseillers'] = $this->model_planning->selectConseiller();
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            
            $data['rdvConseillers'] = $this->model_planning->selectRdvConseiller();

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/plannings_gestion');
            $this->load->view('admin/_footer_admin');
        } else {
            $this->load->view('admin/connex_admin');
        }
    }


    /**************************************************************************
     *                             SELECT - DROPBOX                           *
     *************************************************************************/

    //  RECUPERE LES CONSEILLERS SELON L'ID ANNEXE
    public function changeAnnexe($idAnnexe = null){
        
        $idAnnexe = intval($this->input->post('idAnnexe'));
        $data['annexe'] = $this->model_planning->selectConseiller(null,$idAnnexe);
        $data['rdv'] = $this->model_planning->selectRdvConseiller(null,null,$idAnnexe);
        echo json_encode($data);
    }

    /**************************************************************************
     *                              PLANNING                                  *
     *************************************************************************/

    //  RECUPERE LES HORRAIRES DU CONSEILLER SELON L'IDCONSEILLER
    public function getHorraires(){
        $idConseiller = $this->input->post('idConseiller');

        // Si idConseiller vide alors espace_conseiller donc recupération de l 'id session du conseiller
        if(empty($idConseiller)){
            $idConseiller = intval($this->session->userdata("idUtilisateur"));
        }

        $data = $this->model_tools->selectWhere('planning','idUtilisateur',$idConseiller);
        echo json_encode($data);
    }

    // EDITION PLANNING
    public function savePlanning(){

        $agenda = new Agenda();
        $agenda->idConseiller = $this->input->post('idConseiller');
        $agenda->lundi = json_decode($this->input->post('lundi'));
        $agenda->mardi = json_decode($this->input->post('mardi'));
        $agenda->mercredi = json_decode($this->input->post('mercredi'));
        $agenda->jeudi= json_decode($this->input->post('jeudi'));
        $agenda->vendredi = json_decode($this->input->post('vendredi'));

        //SI CONSEILLER NULL --> ALORS SESSION DU CONSEILLER
        if($agenda->idConseiller == null){
            $agenda->idConseiller = intval($this->session->userdata("idUtilisateur"));
        }

        $this->model_tools->delete($agenda->idConseiller,"idUtilisateur","planning"); // DELETE DE L'ANCIEN PLANNING

        $tabInsert= array(
            'lundi_matin_deb' => $agenda->lundi[0],
            'lundi_matin_fin' => $agenda->lundi[1],
            'lundi_midi_deb' => $agenda->lundi[2],
            'lundi_midi_fin' => $agenda->lundi[3],
            'mardi_matin_deb' => $agenda->mardi[0],
            'mardi_matin_fin' => $agenda->mardi[1],
            'mardi_midi_deb' => $agenda->mardi[2],
            'mardi_midi_fin' => $agenda->mardi[3],
            'mercredi_matin_deb' => $agenda->mercredi[0],
            'mercredi_matin_fin' => $agenda->mercredi[1],
            'mercredi_midi_deb' => $agenda->mercredi[2],
            'mercredi_midi_fin' => $agenda->mercredi[3],
            'jeudi_matin_deb' => $agenda->jeudi[0],
            'jeudi_matin_fin' => $agenda->jeudi[1],
            'jeudi_midi_deb' => $agenda->jeudi[2],
            'jeudi_midi_fin' => $agenda->jeudi[3],
            'vendredi_matin_deb' => $agenda->vendredi[0],
            'vendredi_matin_fin' => $agenda->vendredi[1],
            'vendredi_midi_deb' => $agenda->vendredi[2],
            'vendredi_midi_fin' => $agenda->vendredi[3],
            'idUtilisateur' => $agenda->idConseiller
        );

        $this->model_tools->insert($tabInsert,'planning');   // INSERTION EN BDD

    }

    /**************************************************************************
     *                              INDISPONIBILITE                           *
     *************************************************************************/

    // RECUPERE LES INDISPONIBILITE SELON ANNEXE ET CONSEILLER
    public function getIndispo(){

        $idConseiller = $this->input->post('idConseiller');

        if($idConseiller == "false"){
            $idConseiller = intval($this->session->userdata("idUtilisateur"));
            $idAnnexe = intval($this->session->userdata("idAnnexe"));

        }else{
            $idAnnexe = intval($this->input->post('idAnnexe'));
            $idConseiller = intval($idConseiller);
        }

        $data = $this->model_planning->selectIndispo($idAnnexe,$idConseiller);

        echo json_encode($data);
    }

    // SAISIE NOUVELLE INDISPONIBILITE
    public function saveIndispo(){

        $agenda = new Agenda();

        $agenda->intitule = $this->input->post('intitule');
        $agenda->dateDebut = $this->input->post('dateDebut');
        $agenda->dateFin = $this->input->post('dateFin');
        $agenda->heureDebut = $this->input->post('heureDebut');
        $agenda->heureFin = $this->input->post('heureFin');
        $agenda->idAnnexe = $this->input->post('idAnnexe');
        $agenda->idConseiller = $this->input->post('idConseiller');

        //SI CONSEILLER NULL --> ALORS SESSION DU CONSEILLER
        if($agenda->idConseiller == null){
            $agenda->idConseiller = intval($this->session->userdata("idUtilisateur"));
            $agenda->idAnnexe = intval($this->session->userdata("idAnnexe"));
        }
        
        
        // VERIF SI RENDEZ VOUS DEJA EXISTANT
        $rdvExist = $this->model_planning->selectRdvConseiller($agenda->idConseiller,null,$agenda->idAnnexe, $agenda->dateDebut." ".$agenda->heureDebut, $agenda->dateFin." ".$agenda->heureFin);
    
        if(empty($rdvExist)){

            $tabInsert= array(
                'intituleIndispo' => $agenda->intitule,
                'dateDebutIndispo' => $agenda->dateDebut,
                'dateFinIndispo' => $agenda->dateFin,
                'heureDebutIndispo' => $agenda->heureDebut,
                'heureFinIndispo' => $agenda->heureFin,
                'idAnnexe' => $agenda->idAnnexe,
                'idUtilisateur' => $agenda->idConseiller,
                'etatIndispo' => 1,
            );

            $lastId = $this->model_tools->insert($tabInsert,'indisponibilite');   // INSERTION EN BDD
            
            echo $lastId;
        }else{
            echo 0;
        }
        
    }

    public function deleteIndispo(){

        $idIndispo = $this->input->post('id');
        $tabEtat['etatIndispo'] = 0;
        $this->model_tools->update($tabEtat,'idIndispo',$idIndispo,'indisponibilite');
        echo true;
    }
    
    public function changeEtatRdv(){
        
        $idRdv = $this->input->post('idRdv');
        $etatRdv = intval($this->input->post('etatRdv'));
        $rdv = $this->model_planning->selectRdvConseiller(null,$idRdv);
        $txtEmail = $this->input->post('txtAnnul');
        $tabUpdate['compte_rendu'] = $this->input->post('compte_rendu');

        switch ($etatRdv) {
            
            // SI 1 ALORS RDV CONFIRMER
            case 1:
                $tabUpdate['etatRdv'] = 2;
                $txtEmail = "Confirmation de votre rendez vous le : à avec à l'adresse suivante ";
                
                break;
                
            // SI 2 ALORS RDV REFUSER ET SUPPRIMER DE LA BASE 
            case 2:
              
                $tabUpdate['etatRdv'] = 0;
                $txtEmail = "Bonjour, votre rendez vous ne sera pas prise en compte pour la raison suivante : ".$this->input->post('compte_rendu');
                break;

        }
        
          $this->model_tools->update($tabUpdate,'idRdv',$idRdv,'rendez_vous');
        
        //  ENVOIE MAIL DE CONFIRMATION / REFUS / ANNULATION 
        //  $this->email->from('contact@stop-gaspi.fr', 'Contact Stop Gaspi');
        //  $this->email->to($rdv->emailEntreprise');
        //
        //  $this->email->subject('Réponse a votre demande de rendez vous');
        //  $this->email->message($txtEmail);
        //  $this->email->send();
        
        echo true;
        
    }
        



}