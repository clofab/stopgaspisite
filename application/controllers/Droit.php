<?php

class Droit extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_utilisateur');

    }


    // PAGE ACCUEIL GESTION DE DROIT
    public function gestionDroit(){

        if($this->session->userdata('gestionDroit') == true){
            $data['types_utilisateur'] = $this->model_tools->selectAll('type_utilisateur');
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/gestion_droit');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }


    // VALIDATION -- EDITION DROI
    public function setDroit(){

        $idType = $this->input->post('id');
        $lblTypeUtilisateur = $this->input->post('lblTypeUtilisateur');

        $types_utilisateur = $this->model_tools->selectWhere('type_utilisateur','lblTypeUtilisateur',$lblTypeUtilisateur); // verifie si libelle deja existant

        foreach($types_utilisateur as $type_utilisateur){
            // Si lbl deja utilisé ET $id différent Si modif
            if($type_utilisateur->lblTypeUtilisateur == $lblTypeUtilisateur && $type_utilisateur->idTypeUtilisateur != ($idType || null)){
                echo 0;
                return false;
            }
        }

        $tabDroit = array(
            'lblTypeUtilisateur'=> $lblTypeUtilisateur,
            'gestionFiche'=> filter_var($this->input->post('fiche'), FILTER_VALIDATE_BOOLEAN),
            'gestionActu'=> filter_var($this->input->post('actu'), FILTER_VALIDATE_BOOLEAN),
            'gestionVideo'=> filter_var($this->input->post('video'), FILTER_VALIDATE_BOOLEAN),
            'gestionSite'=> filter_var($this->input->post('site'), FILTER_VALIDATE_BOOLEAN),
            'droitConseiller'=> filter_var($this->input->post('commercial'), FILTER_VALIDATE_BOOLEAN),
            'gestionRdv'=> filter_var($this->input->post('rdv'), FILTER_VALIDATE_BOOLEAN),
            'gestionUtilisateur'=> filter_var($this->input->post('utilisateur'), FILTER_VALIDATE_BOOLEAN),
            'gestionDroit'=> filter_var($this->input->post('droit'), FILTER_VALIDATE_BOOLEAN),
            'gestionAnnexe'=> filter_var($this->input->post('annexe'), FILTER_VALIDATE_BOOLEAN),
        );

        if ($idType > 0){   // update
            $this->model_tools->update($tabDroit,'idTypeUtilisateur',$idType,'type_utilisateur');
            echo $idType;
        }else{ // insert

            $lastId = $this->model_tools->insert($tabDroit,'type_utilisateur');
            echo $lastId;
        }
    }

    public function deleteDroit(){
        $idType = $this->input->post('id');
        $utilisateur = $this->model_utilisateur->selectUtilisateur(null,false,$idType);

        if(empty($utilisateur)){

            $this->model_tools->delete($idType,'idTypeUtilisateur','type_utilisateur');
            echo true;
        }else{
            echo json_encode(0);
        }

    }





}
