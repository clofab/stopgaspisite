<?php

class Generate extends CI_Controller
{

    
    public function pdf_fiche($idFiche)
    {   
        $this->load->model('model_fiche');
        
        $data['fiche'] = $this->model_fiche->selectFiche($idFiche)[0];

        //$fileName = $data['fiche']->titre;
        //load the view, pass the variable and do not show it but "save" the output into $html variable
        $html=$this->load->view('site/fiche_pdf', $data, true); 
        
        //this the the PDF filename that user will get to download
        $pdfFilePath = "fiche_conseil.pdf";
        
        //load mPDF library
        $this->load->library('m_pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->m_pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD)
        $pdf->Output($pdfFilePath, "D");
    }
    

}