<?php

class Actualite extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_actualite');
        $this->load->library('Actu');
    }

    // PAGE GESTION DES ACTUALITES
    public function indexActu()                   
    {
        if($this->session->userdata('gestionActu') == true){
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            $data['actus'] = $this->model_actualite->selectActus();
           
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_actualite');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    public function getActu(){
        $idActu = $this->input->post('idActu');
        $data = $this->model_actualite->selectActu($idActu);
        echo json_encode($data);
    }

    public function getArchiveActu(){
        $typeActu = intval($this->input->post('typeActu'));
        if($typeActu == 0) $typeActu = null;
        $data = $this->model_actualite->selectActus(true,$typeActu);
        echo json_encode($data);
    }

    // ARCHIVE UNE ACTUALITE
    public function deleteActu(){

        $idActu = $this->input->post('id');
        $tabEtat['etatActu'] = 0;

        // SI UN SEUL ID , ALORS ON LE MET DANS UN TABLEAU
        if(!is_array($idActu)) $idActu = array('0' => $idActu);

        foreach ($idActu as $id) {
            $this->model_tools->update($tabEtat,'idActu',$id,'actualites');
        }

        echo true;
    }


    // PUBLICATION/DEPUBLICATION D'UNE ACTU
    public function publishActu(){

        $idActu = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatActu' => $etat);

        // SI UN SEUL ID , ALORS ON LE MET DANS UN TABLEAU
        if(!is_array($idActu)) $idActu = array('0' => $idActu);

        foreach ($idActu as $id) {
            $this->model_tools->update($tabEtat,'idActu',$id,'actualites');
        }

        echo true;
    }

    // SAISIE D'UNE ACTUALITE
    public function saisieActu($idActu = null){
        $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
        if($idActu != null){    // EDITON
            $actu = $this->model_actualite->selectActu($idActu);
            $data['actu'] = $actu[0];
        }else{                  // AJOUT
            $data['actu'] = new Actu();
        }

        if($this->session->userdata('Admin') == true){
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/nouvelle_actualite');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // VALIDATION DE SAISIE D'UNE ACTUALITE
    public function validSaisieActu(){

        $actu = new Actu();

        $actu->idActu = $this->input->post('idActu');
        $etat = $this->input->post('checkEtat');
        $actu->etatActu = ( $etat != null ? 1 : 2);

        if( $actu->idActu != null){ // SI EDITION

            $lastImg = $this->input->post('lastImage');

            // SI PAS DE NOUVELLE IMAGE
            if($this->input->post('etatImage') == 1){
                $actu->imageActu = $lastImg;
            }else{
                $actu->imageActu = $actu->uploadImg("actu","fileActu");           //  UPLOAD IMAGE
                $actu->deleteImg('actu',$lastImg);                                // DELETE ANCIENNE IMAGE
            }

            //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer
            $tabActu = array(
                'titre' => $this->input->post('titreActu'),
                'resume' => $this->input->post('resumeActu'),
                'imageActu' => $actu->imageActu,
                'description' => $this->input->post('descriptionActu'),
                'etatActu' =>  $actu->etatActu,
            );

            $this->model_tools->update($tabActu,'idActu',$actu->idActu,'actualites'); // UPDATE DANS BDD
        }else{             // SINON CREATION

            $actu->imageActu = $actu->uploadImg("actu","fileActu");           //  UPLOAD IMAGE
            $tabActu = array(
                'idActu' => $this->input->post('idActu'),
                'titre' => $this->input->post('titreActu'),
                'resume' => $this->input->post('resumeActu'),
                'imageActu  ' => $actu->imageActu,
                'description' => $this->input->post('descriptionActu'),
                'dateCreation' => date("Y-m-d H:i:s"),
                'etatActu' =>  $actu->etatActu,
                'idUtilisateur' => $this->session->userdata('idUtilisateur'), // ID SESSION A METRE
            );
            $this->model_tools->insert($tabActu,'actualites');   // INSERTION EN BASE
        }
        $url = base_url(uri_string());
        $type = substr($url, strrpos($url, '/') + 1);


        redirect('/Actualite/indexActu/', 'refresh');



    }
}
