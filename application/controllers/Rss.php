<?php

class Rss extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_actualite');
        $this->load->model('model_fiche');
        $this->load->helper('xml');
        $this->load->helper('text');

    }

    // RSS POUR ACTUALITE
    public function index()
    {
        $data['feed_name'] = 'https://stopgaspi-clofab.c9users.io/'; // your website
        $data['encoding'] = 'utf-8'; // the encoding
        $data['feed_url'] = 'https://stopgaspi-clofab.c9users.io/feed'; // the url to your feed
        $data['page_description'] = 'Abonnement actualité Stop Gaspi'; // some description
        $data['page_language'] = 'en-fr'; // the language
        $data['creator_email'] = 'mail@me.com'; // your email
        $data['posts'] = $this->model_actualite->getActuRss(10);
        header("Content-Type: application/rss+xml"); // important!
        $this->load->view('site/rss_actu', $data);
    }
    
    
    // RSS POUR FICHES CONSEIL
    public function rss_fiche()
    {
        $data['feed_name'] = 'https://stopgaspi-clofab.c9users.io/'; // your website
        $data['encoding'] = 'utf-8'; // the encoding
        $data['feed_url'] = 'https://stopgaspi-clofab.c9users.io/feed'; // the url to your feed
        $data['page_description'] = 'Abonnement fiche conseil Stop Gaspi'; // some description
        $data['page_language'] = 'en-fr'; // the language
        $data['creator_email'] = 'mail@me.com'; // your email
        $data['posts'] = $this->model_fiche->getFicheRss(10);
        header("Content-Type: application/rss+xml"); // important!
        $this->load->view('site/rss_fiche', $data);
    }
    
    

}
    