<?php

class Utilisateurs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_utilisateur');
        $this->load->model('model_front');
        $this->load->library('Entreprise');
    }

    // ACCUEIL GESTION DES UTILISATEURS
    public function indexUtilisateur()
    {
        if($this->session->userdata('gestionUtilisateur') == true){

            $data['utilisateurs'] = $this->model_utilisateur->selectUtilisateur();
            $data['types_utilisateur'] = $this->model_tools->selectAll('type_utilisateur');
            $data['annexes'] = $this->model_tools->selectWhere('annexe','etatAnnexe !=',0);
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_utilisateur');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // REACTIVATION ARCHIVE
    public function publishUtilisateur(){
        $idUtilisateur = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatUtilisateur' => $etat);
        $this->model_tools->update($tabEtat,"idUtilisateur",$idUtilisateur,"utilisateur");
        echo true;
    }

    // RÉCUPÈRE LES DONNÉES SUR UN UTILISATEUR
    public function getUtilisateur(){
        $idUtilisateur = $this->input->post('id');

        $utilisateur = $this->model_utilisateur->selectUtilisateur($idUtilisateur,false);
        $utilisateur[0]->mdpUtilisateur = $this->encrypt->decode($utilisateur[0]->mdpUtilisateur); // Decodage mot de passe
        echo json_encode($utilisateur);
    }

    //RECUPERATION DES ARCHIVES
    public function getArchiveUtilisateur(){
        $data =$this->model_utilisateur->selectUtilisateur(null,true);
        echo json_encode($data);
    }

    // PUBLICATION/DEPUBLICATION D'UNE ACTU
    public function deleteUtilisateur(){
        $idUtilisateur = $this->input->post('id');
        $etat = 0;
        $tabEtat = array('etatUtilisateur' => $etat);
        $this->model_tools->update($tabEtat,"idUtilisateur",$idUtilisateur,"utilisateur");
        echo true;
    }

    // AJOUT D'UN UTILISATEUR
    public function addUtilisateur()
    {
        $utilisateur = new Utilisateur();
        $utilisateur->email = $this->input->post('email') ;
        $utilisateur->password = $this->input->post('mdp');
        $utilisateur->idUtilisateur =  $this->input->post('idUtilisateur');
        $utilisateur->idAnnexe =  $this->input->post('annexe');
        $lastImg = $this->input->post('lastImg');

        $nbEmail = $this->model_utilisateur->verifEmail($utilisateur->email,$utilisateur->idUtilisateur);   // verifie si mail deja existant

        if (!empty($nbEmail)){ // Si email existe deja, erreur
            echo 0;
        }else{

            // TABLEAU POUR MISE A JOUR OU INSERT
            $tabUti = array(
                'emailUtilisateur' =>   $utilisateur->email,
                'nomUtilisateur' =>  $this->input->post('nom'),
                'prenomUtilisateur' =>  $this->input->post('prenom'),
                'mdpUtilisateur' => $this->encrypt->encode($utilisateur->password),
                'etatUtilisateur'=> 1,
                'idTypeUtilisateur'=>  $this->input->post('typeUtilisateur'),
                'idAnnexe'=>  $utilisateur->idAnnexe
            );

            if($utilisateur->idUtilisateur == null){    // SI INSERT

                if($this->input->post('file') == "false") {

                    $utilisateur->imageUtilisateur = avatarDefault; // alors l'image est egale a celle par default [constante]
                }else{  //Sinon on upload l'image
                    $utilisateur->imageUtilisateur = $utilisateur->uploadImg("utilisateur", "file");   //  UPLOAD IMAGE
                }
                $tabUti['imgUtilisateur'] = $utilisateur->imageUtilisateur;
                $lastId = $this->model_tools->insert($tabUti,'utilisateur');

                echo json_decode($lastId);

            }else{  // SI UPDATE

                if($this->input->post('file') == true){          // SI PAS DE NOUVELLE IMAGE
                    $utilisateur->imageUtilisateur = $lastImg;   // upload image
                }else{

                    $utilisateur->imageUtilisateur = $utilisateur->uploadImg("utilisateur","file");
                    if($lastImg != avatarDefault){
                        $utilisateur->deleteImg($lastImg);                                // DELETE ANCIENNE IMAGE
                    }

                    $tabUti['imgUtilisateur'] = $utilisateur->imageUtilisateur;
                }
                $this->model_tools->update($tabUti,"idUtilisateur",$utilisateur->idUtilisateur,"utilisateur");

                echo json_decode($utilisateur->idUtilisateur);
            }
        }
    }
    
    public function addUtilisateurAppli()
    {
        $data = json_decode(file_get_contents('php://input'), TRUE);
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
        header("Access-Control-Expose-Headers: Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Methods");
        $email = $data['email'];
        $nbEmail = $this->model_front->verifEmail($email);   // verifie si mail deja existant

        if (!empty($nbEmail)){ // Si email existe deja, erreur
            echo 0;
        }else{

            $tabInsert= array(
                "nomEntreprise" => $data['entreprise'],
                "interlocuteur" => $data['prenom']." ".$data["nom"],
                "telEntreprise" => $data['telephone'],
                "emailEntreprise" => $data['email'],
                "mdpEntreprise" =>  $this->encrypt->encode($data['mdp']),
                "adresseEntreprise" => $data['adresse'],
                "longitudeEntreprise" => $data['longitude'],
                "latitudeEntreprise" => $data['latitude'],
                "etatEntreprise" => 1, // A METTRE à 0 UNE FOIS EMAIL OK
            );
    
            $this->model_tools->insert($tabInsert,'entreprise');   // INSERTION EN BDD

//        $this->email->from('thomas.dourlens@gmail.com', 'Contact Stop Gaspi');
//        $this->email->to('Decottigniesf@yahoo.com');
//
//        $this->email->subject('Confirmation d'inscription : StopGaspi);
//        $this->email->message('Lien de confirmation :');
//        $this->email->send();

          echo 1;
        }
    }
    // RECUPERATION DE LA LISTE DES ENTREPRISES
    public function getEntreprise(){
        echo json_encode($this->model_tools->selectAll('entreprise'));
    }
    
    
    // BANNISSEMENT -- RE ACTIVATION ENTREPRISES
    public function updateEtatEntreprise(){
        $idEntreprise = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatEntreprise' => $etat);
        $this->model_tools->update($tabEtat,"idEntreprise",$idEntreprise,"entreprise");
        echo true;
    }

    //Connexion depuis l'application
     public function connexionUtilisateurAppli()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
        header("Access-Control-Expose-Headers: Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Methods");
        
        $entreprise = new Entreprise();
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $entreprise->emailEntreprise = $params["email"];
        $entreprise->mdpEntreprise = $params["password"];
        $entreprise = $this->model_front->connexEntreprise($entreprise);
         // VERIFICATION DANS BDD
        // SI BONNES INFORMATION FORMULAIRE
        if(isset($entreprise)){
            echo json_encode($entreprise);           
        }else{
             echo json_encode(array('idEntreprise'=>0));   
       }
    }
}