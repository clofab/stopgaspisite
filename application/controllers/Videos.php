<?php

class Videos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Video');
        $this->load->model('model_video');
    }

    // PAGE ACCUEIL GESTION DE VIDEOS
    public function indexVideo(){

        if($this->session->userdata('gestionVideo') == true){
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            $data['videos'] = $this->model_video->selectVideos();

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_video');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }

    }

    // SUPPRESSION VIDEO
    public function deleteVideo(){
        $idVideo = $this->input->post('id');
        $tabEtat['etatVideo'] = 0;

        if(!is_array($idVideo)) $idVideo = array('0' => $idVideo);

        foreach ($idVideo as $id) {
            $this->model_tools->update($tabEtat,'idVideo',$id,'video');
        }
        echo true;
    }


    // PUBLICATION/DEPUBLICATION VIDEO
    public function publishVideo(){
        $idVideo = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatVideo' => $etat);

        if(!is_array($idVideo)) $idVideo = array('0' => $idVideo);

        foreach ($idVideo as $id) {
            $this->model_tools->update($tabEtat,'idVideo',$id,'video');
        }

        echo true;
    }
    // PAGE D'EDITION D'UNE VIDEO
    public function saisieVideo($idVideo = null){

        if($idVideo != null){    // EDITON
            $video = $this->model_video->selectVideo($idVideo);
            $data['video'] = $video[0];
        }else{                  // AJOUT
            $data['video'] = new Video();
        }

        if($this->session->userdata('gestionVideo') == true){
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/nouvelle_video');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    // VALIDATION DE SAISIE D'UNE VIDEO
    public function validSaisieVideo(){

        $video = new Video;
        $video->idVideo = $this->input->post('idVideo');
        $video->idTypeEnergie = $this->input->post('typeEnergie');

        $video->etatVideo = $this->input->post('etatVideo');
        $video->etatVideo = ($video->etatVideo != null ? 1 : 2);
        $video->urlVideo = $this->input->post('urlVideo');
        $video->urlVideo = str_replace("www.youtube.com/watch?v=","www.youtube.com/embed/",$video->urlVideo);

        //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer/modifier
        $tabVideo = array(
            'titreVideo' => $this->input->post('titreVideo'),
            'urlVideo' => $video->urlVideo,
            'descriptionVideo' => $this->input->post('descriptionVideo'),
            'etatVideo' => $video->etatVideo,
        );

        if($video->idVideo != null){ // SI EDITION

            $this->model_tools->update($tabVideo,'idVideo',$video->idVideo,'video');  // UPDATE DANS BDD
            $this->model_tools->delete($video->idVideo,"idVideo","video_type_energie");//DELETE DES ANCIENS TYPE D'ENERGIE

        }else{             // SINON CREATION

            $tabVideo['dateCreation'] = date("Y-m-d H:i:s");
            $tabVideo['idUtilisateur'] = $this->session->userdata('idUtilisateur');

            $video->idVideo = $this->model_tools->insert($tabVideo,'video');   // INSERTION EN BASE
        }
        // recupération et insertion des checks [typeEnergie]
        foreach ($video->idTypeEnergie as $typeEnergie) {

            $tabCategorie = array(
                'idVideo' =>  $video->idVideo,
                'idTypeEnergie' =>  $typeEnergie,
            );
            $this->model_tools->insert($tabCategorie,'video_type_energie');   // INSERTION EN BASE
        }

        redirect('/Videos/indexVideo/', 'refresh');
    }

    //RECUPERATION DES ARCHIVES
    public function getArchiveVideo(){
        $data =$this->model_video->selectVideos(true);
        echo json_encode($data);
    }

    public function getVideo(){
        $idVideo = $this->input->post('idVideo');
        $data = $this->model_video->selectVideo($idVideo);
        echo json_encode($data);
    }

}

