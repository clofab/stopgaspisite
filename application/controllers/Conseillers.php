<?php

class Conseillers extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_planning');
    }

    public function indexConseiller()
    {
        if ($this->session->userdata("droitConseiller") == true) {

            $idUtilisateur = intval($this->session->userdata("idUtilisateur"));

            $data['conseiller'] = $this->model_planning->selectConseiller($idUtilisateur);
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            $data['rdvConseillers'] = $this->model_planning->selectRdvConseiller($idUtilisateur);
            
            // RECUPERE LE NOMBRE DE RDV NON CONFIRMER
            $notifRdv = $this->model_planning->notifRdv($idUtilisateur);
            $data['notifRdv'] = count($notifRdv, COUNT_RECURSIVE); 
            
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/conseiller_espace');
            $this->load->view('admin/_footer_admin');
        } else {
            $this->load->view('admin/connex_admin');
        }

    }

}