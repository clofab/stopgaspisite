<?php

class Fiche extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_fiche');
        $this->load->library('FicheConseil');

    }
    
    // PAGE GESTION DES FICHES
    public function indexFiche()
    {
        if($this->session->userdata('gestionFiche') == true){
            $data['fiches'] =  $this->model_fiche->selectFiches();
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/liste_fiche');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    public function getArchiveFiche(){
        $data =$this->model_fiche->selectFiches(true);
        echo json_encode($data);
    }

    public function getFiche(){
        $idFiche = $this->input->post('idFiche');
        $data = $this->model_fiche->selectFiche($idFiche);
        echo json_encode($data);
    }


    // SUPPRESSION D'UNE FICHE
    public function deleteFiche(){
        $idFiche = $this->input->post('id');
        $tabEtat['etatFiche'] = 0;

        if(!is_array($idFiche)) $idFiche = array('0' => $idFiche);

        foreach ($idFiche as $id) {
            $this->model_tools->update($tabEtat,"idFiche",$id,"fiche_conseil");
        }
        echo true;
    }


    // PUBLICATION/DEPUBLICATION D'UNE ACTU
    public function publishFiche(){

        $idFiche = $this->input->post('id');
        $etat = $this->input->post('etat');
        $tabEtat = array('etatFiche' => $etat);

        if(!is_array($idFiche)) $idFiche = array('0' => $idFiche);

        foreach ($idFiche as $id) {
            $this->model_tools->update($tabEtat,"idFiche",$id,"fiche_conseil");
        }

        echo true;
    }


    // VALIDATION DE SAISIE D'UNE ACTUALITE
    public function saisieFiche($idFiche = null){

        $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
        if($idFiche != null){    // EDITON
            $fiche = $this->model_fiche->selectFiche($idFiche);
            $data['fiche'] = $fiche[0];
        }else{                  // AJOUT
            $data['fiche'] = new FicheConseil();
        }

        if($this->session->userdata('Admin') == true){
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/nouvelle_fiche',$data);
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }


    // VALIDATION DE SAISIE D'UNE ACTUALITE
    public function validSaisieFiche(){

        $fiche = new FicheConseil();

        $fiche->idFiche = $this->input->post('idFiche');
        $fiche->etatFiche = $this->input->post('checkEtat');
        $fiche->etatFiche = (  $fiche->etatFiche != null ? 1 : 2);
        $fiche->idTypeEnergie = $this->input->post('typeEnergie');

        //  Tableau avec en index le nom du champs en BDD puis la valeur à inserer
        $tabFiche = array(
            'titre' => $this->input->post('titreFiche'),
            'description' => $this->input->post('descriptionFiche'),
            'etatFiche' =>  $fiche->etatFiche,
        );

        if( $fiche->idFiche != null){ // SI EDITION

            $this->model_tools->update($tabFiche,'idFiche',$fiche->idFiche,'fiche_conseil'); // UPDATE DANS BDD
            $this->model_tools->delete($fiche->idFiche,"idFiche","fiche_type_energie");//DELETE DES ANCIENS TYPE D'ENERGIE

        }else{             // SINON CREATION

            $tabFiche['dateCreation'] = date("Y-m-d H:i:s");
            $tabFiche['idUtilisateur'] = $this->session->userdata('idUtilisateur');// ID SESSION A METRE

            $fiche->idFiche = $this->model_tools->insert($tabFiche,'fiche_conseil');   // INSERTION EN BASE
        }

        // recupération et insertion des checks [typeEnergie]
        foreach ($fiche->idTypeEnergie as $typeEnergie) {

            $tabCategorie = array(
                'idFiche' =>  $fiche->idFiche,
                'idTypeEnergie' =>  $typeEnergie,
            );
            $this->model_tools->insert($tabCategorie,'fiche_type_energie');   // INSERTION EN BASE
        }

        redirect('/Fiche/indexFiche/', 'refresh');

    }


}
