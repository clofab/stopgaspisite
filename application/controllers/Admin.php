<?php

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_utilisateur');
        $this->load->helper('cookie');       
        $this->load->model('model_planning');
    }
    
    
    private function addDroit($utilisateur){

		$tabAcces = array(
			'Admin' => true,
			'idUtilisateur' => $utilisateur->idUtilisateur,
			'imgAvatar' => 'utilisateur/'.$utilisateur->imgUtilisateur,
			'nom' => $utilisateur->nomUtilisateur,
			'prenom' => $utilisateur->prenomUtilisateur,
			'idTypeUtilisateur' => $utilisateur->idTypeUtilisateur,
			'gestionActu' => $utilisateur->gestionActu,
			'gestionFiche' => $utilisateur->gestionFiche,
			'gestionVideo' => $utilisateur->gestionVideo,
			'gestionSite' => $utilisateur->gestionSite,
			'droitConseiller' => $utilisateur->droitConseiller,
			'gestionRdv' => $utilisateur->gestionRdv,
			'gestionUtilisateur' => $utilisateur->gestionUtilisateur,
			'gestionDroit' => $utilisateur->gestionDroit,
			'gestionAnnexe' => $utilisateur->gestionAnnexe,
			'idAnnexe' => $utilisateur->idAnnexe,
		);
        
		
		$this->session->set_userdata($tabAcces);
    }

    public function index()
    {
        
        $idUtilisateur =  $this->input->cookie('authBack',true);
        
        // SI IL Y A UN COOKIE, ALORS ON VERIFIE UTILSATEUR
        if(isset($idUtilisateur)){
            
            $idUtilisateur =  $this->encrypt->decode($idUtilisateur);
            $utilisateur= $this->model_utilisateur->selectUtilisateur($idUtilisateur);
            $this->addDroit($utilisateur['0']);
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/tableau_de_bord');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
        
    }
    
    public function deconnexion(){
         // delete_cookie('authBack'); 
        $this->session->sess_destroy();
        $this->load->view('admin/connex_admin');
    }

    public function accesAdmin()
    {
        $table = null;
        $utilisateur = new Utilisateur();
        $utilisateur->email = $this->input->post('email');
        $utilisateur->password = $this->input->post('password');
        $remember = $this->input->post('remember');
        
        $utilisateur = $this->model_utilisateur->connexAdmin($utilisateur); // VERIFICATION DANS BDD

        // SI BONNES INFORMATION FORMULAIRE
        if(isset($utilisateur)){
                
            //  CREATION DU COOKIE SI CASE COCHER 
            if(isset($remember)){
                $cookie= array(
                  'name'   => 'authBack',
                  'value'  => $this->encrypt->encode($utilisateur->idUtilisateur),
                  'expire' => '86500',
                  'path' => '/',
                  'httponly' => true,
                  
                );
                $this->input->set_cookie($cookie);
            }
            
            $this->addDroit($utilisateur);
            
            
            // SI CONSEILLER 
            if ($this->session->userdata("droitConseiller") == true) {
                redirect('Conseillers/indexConseiller');
            }else{
                redirect('Admin/dashboard');
            }

        }else{
            $data['msgError'] = "Email ou identifiant incorrect";
            $this->load->view('admin/connex_admin', $data);
       }
    }

    public function profilAdmin()
    {
        if($this->session != null){
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/edition_profil');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }

    public function visioconferenceAdmin()
    {

        if($this->session->userdata('Admin') == true){
            
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            
            // RECUPERE LE NOMBRE DE RDV NON CONFIRMER
            $idUtilisateur = intval($this->session->userdata("idUtilisateur"));
            $notifRdv = $this->model_planning->notifRdv($idUtilisateur);
            $data['notifRdv'] = count($notifRdv, COUNT_RECURSIVE); 
            
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/visioconference');
            $this->load->view('admin/_footer_admin');
        }else{
            $this->load->view('admin/connex_admin');
        }
    }
    public function updateAdmin(){
        $data = array();
        if(($this->input->post('email') != "") && ($this->input->post('email') != $this->session->userdata('email'))){
            $data['emailUtilisateur']=$this->input->post('email');
        }
        if(($this->input->post('mdp') != "") && ($this->input->post('mdp')==$this->input->post('mdp2'))){
            $data['mdpUtilisateur']=$this->encrypt->encode($this->input->post('mdp'));
        }
        if(($this->input->post('nom') != "") && ($this->input->post('nom') != $this->session->userdata('nom'))){
            $data['nomUtilisateur']=$this->input->post('nom');
        }
        if(($this->input->post('prenom') != "") && ($this->input->post('prenom') != $this->session->userdata('prenom'))){
            $data['prenomUtilisateur']=$this->input->post('prenom');
        }

        $utilisateur = new Utilisateur();
        $utilisateur->imageUtilisateur = $utilisateur->uploadImg("utilisateur","file");
        if(($utilisateur->imageUtilisateur != "") && ('utilisateur/'.$utilisateur->imageUtilisateur != $this->session->userdata('imgAvatar'))){
            $data['imgUtilisateur']=$utilisateur->imageUtilisateur;
            $utilisateur->deleteImg($this->session->userdata('imgAvatar'));
        }
        if(count($data)>0){
            $this->model_utilisateur->updateProfil($data);
        }

        redirect('/Admin/profilAdmin', 'refresh');
    }


    public function dashboard(){
        
        if($this->session->userdata('Admin') == true){
            
            $this->load->model('model_dashboard');
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');
            
            
            //REPARTITIONS DES TYPES UTILISATEURS
            if($this->session->userdata('gestionUtilisateur') == true) {
               $data['chart_uti'] = $this->model_dashboard->selectUtilisateur();
            }
            
            
            //Répartition des rendez-vous par état
            if(($this->session->userdata('gestionRdv') == true)&&($this->session->userdata('droitConseiller')==false)){
                $data['chart_rdv_global'] = $this->model_dashboard->countNbRendezVous();
            }
            //Statistiques sur les actualités
            if(($this->session->userdata('gestionActu') == true)&&($this->session->userdata('gestionDroit')==true)){
                $data['chart_actu_etat_global'] = $this->model_dashboard->countNbActusEtat();
                $data['chart_actu_mois_global'] = $this->model_dashboard->countNbActusMois();
                $data['chart_fiche_etat_global'] = $this->model_dashboard->countNbFichesEtat();
                $data['chart_fiche_mois_global'] = $this->model_dashboard->countNbFichesMois();
                $data['chart_video_etat_global'] = $this->model_dashboard->countNbVideosEtat();
                $data['chart_video_mois_global'] = $this->model_dashboard->countNbVideosMois();
                
            }
            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/tableau_de_bord');

            
        }else{
            $this->load->view('admin/connex_admin');
        }
        
    }
}
