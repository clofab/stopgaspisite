<?php

class Accueil extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_actualite');
        $this->load->model('model_front');
        $this->load->library('Actu');
    }

    // PAGE GESTION DES ACTUALITES
    public function index()
    {
        $data['actus'] = $this->model_actualite->selectActus(false,null,true);                              //  SELECT ACTUALITE
        $data['pagesInfos'] = $this->model_actualite->selectActus(false,1);                  //  SELECT PAGES INFOS
        $data['sliders'] = $this->model_tools->selectOrder("slider","ordreSlider","asc");    //  SELECT SLIDER
        $data['contenu'] = $this->model_front->selectContenu();

        $data['typeEnergies'] = $this->model_front->selectAllEnergie();


        $this->load->view('site/_head_site');
        $this->load->view('site/_header_section',$data);
        $this->load->view('site/accueil');
        $this->load->view('site/_footer_section');
        $this->load->view('site/_footer_site');
    }

}