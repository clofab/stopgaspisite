<?php

class Slider extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_gestionsite');
    }


    // PAGE GESTION DU SLIDER
    public function indexSlider(){

        if($this->session->userdata("gestionSite") == true) {

            $data['sliders'] = $this->model_tools->selectOrder("slider","ordreSlider","asc");
            $data['typeEnergies'] = $this->model_tools->selectAll('type_energie');

            $this->load->view('admin/_head_admin');
            $this->load->view('admin/_menu_horizontal');
            $this->load->view('admin/_menu_vertical',$data);
            $this->load->view('admin/gestion_slider');
            $this->load->view('admin/_footer_admin');
        } else {
            $this->load->view('admin/connex_admin');
        }

    }

    //  AJOUT D'UN SLIDE
    public function addSlider(){

        if($this->session->userdata("gestionSite") == true) {

            $tools = new Tools();
            $imgSlide= $tools->uploadImg("slider", "file");   //  UPLOAD IMAGE

            // TABLEAU POUR MISE A JOUR OU INSERT
            $tabInsert = array(
                'titreSlider' =>   $this->input->post('titreSlider'),
                'descriptionSlider' =>  $this->input->post('descriptionSlider'),
                'imgSlider' =>  $imgSlide,
                'lienSlider' => $this->input->post('lienSlider'),
                'nomLienSlider' => $this->input->post('nomLienSlider'),
                'ordreSlider' => $this->input->post('numSlider'),
            );

            $lastId = $this->model_tools->insert($tabInsert,'slider');
            echo json_decode($lastId);
        } else {
            $this->load->view('admin/connex_admin');
        }

    }


    // UPDATE ORDRE SLIDE
    public function saveSlider(){

        $tabSlide = json_decode(stripslashes($this->input->post('tabSlide')));
        $i = 1;
        foreach($tabSlide as $slide){
            $tabUpdate = array(
                'ordreSlider' =>   $i,
            );
            $this->model_tools->update($tabUpdate,'idSlider',$slide,'slider');
            $i++;
        }
    }

    //SUPPRESION D'UN SLIDER
    public function deleteSlider(){

        $idSlider=$this->input->post('idSlider');
        $nameImg=$this->input->post('nameImg');

        $tools = new Tools();
        $tools->deleteImg("slider",$nameImg);


        $this->model_tools->delete($idSlider,'idSlider','slider');
    }


}
    