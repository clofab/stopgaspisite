-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
<<<<<<< HEAD
-- Généré le :  Mar 05 Juillet 2016 à 23:35
=======
-- Généré le :  Dim 17 Juillet 2016 à 20:50
>>>>>>> dev
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `stop_gaspi`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

CREATE TABLE IF NOT EXISTS `actualites` (
  `idActu` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `resume` varchar(50) NOT NULL,
  `imageActu` text NOT NULL,
  `description` text NOT NULL,
  `dateCreation` date NOT NULL,
  `etatActu` tinyint(1) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `pageConseil` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idActu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Contenu de la table `actualites`
--

INSERT INTO `actualites` (`idActu`, `titre`, `resume`, `imageActu`, `description`, `dateCreation`, `etatActu`, `idUtilisateur`, `pageConseil`) VALUES
(123, 'Page info 4', 'restttta', 'Koala.jpg', '<p><strong>photo d''un koala</strong></p>', '2016-05-06', 1, 1, 1),
(125, 'la méchante', 's', 'img1.png', '<p>sza</p>', '2016-05-06', 1, 1, NULL),
(126, 'adazd', 'sdazda', 'img2.jpg', '<p>szaazdacbcc</p>', '2016-05-06', 1, 1, NULL),
(127, 'actu 2', 'frfre', 'Siberischer_tiger_de_edit02.jpg', '<p>ferfreferf</p>', '2016-05-25', 1, 1, NULL),
(128, '2e page d''info ', 'test', '8dmX91R5un2TC6ySldLfmUiGC8LGK7BLYYYd3hoyf65GKA2vkc12ZlVZ7vWOKwFp-black-light-dark-figures-2560x1440.png', '<p>dzdazd</p>', '2016-06-25', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `annexe`
--

CREATE TABLE IF NOT EXISTS `annexe` (
  `idAnnexe` int(11) NOT NULL AUTO_INCREMENT,
  `nomAnnexe` varchar(30) NOT NULL,
  `adresseAnnexe` varchar(200) NOT NULL,
  `latitudeAnnexe` text NOT NULL,
  `longitudeAnnexe` text NOT NULL,
  `telAnnexe` varchar(25) NOT NULL,
  `emailAnnexe` varchar(50) NOT NULL,
  `etatAnnexe` int(11) NOT NULL,
  PRIMARY KEY (`idAnnexe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `annexe`
--

INSERT INTO `annexe` (`idAnnexe`, `nomAnnexe`, `adresseAnnexe`, `latitudeAnnexe`, `longitudeAnnexe`, `telAnnexe`, `emailAnnexe`, `etatAnnexe`) VALUES
(3, 'Préfecture de Toulouse', 'Toulouse, France', '43.604652', '1.4442090000000007', '0675587808', 'thomas.dourlens@gmail.com', 1),
(4, 'Préfecture de Lille', 'Lille, France', '50.62925', '3.057256000000052', '0675587808', 'thomas.dourlens@gmail.com', 1),
(5, 'Préfecture de Paris', 'Paris, France', '48.856614', '2.3522219000000177', '011111', 'pompidous@gmail.com', 1),
(6, 'Préfecture de Lyon', 'Lyon, France', '45.764043', '4.835658999999964', '22', 'totos@gmail.com2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `contenu`
--

CREATE TABLE IF NOT EXISTS `contenu` (
  `idContenu` int(11) NOT NULL AUTO_INCREMENT,
  `titreContenu` varchar(150) NOT NULL,
  `descriptionContenu` text NOT NULL,
  `typeContenu` varchar(50) NOT NULL,
  PRIMARY KEY (`idContenu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `contenu`
--

INSERT INTO `contenu` (`idContenu`, `titreContenu`, `descriptionContenu`, `typeContenu`) VALUES
(1, 'aaC', '<p>&nbsp;W&lt;h</p>', 'accueil');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `idEntreprise` int(11) NOT NULL AUTO_INCREMENT,
  `nomEntreprise` varchar(50) NOT NULL,
  `interlocuteur` varchar(100) NOT NULL,
  `telEntreprise` varchar(10) NOT NULL,
  `emailEntreprise` varchar(30) NOT NULL,
  `mdpEntreprise` text NOT NULL,
  `adresseEntreprise` text NOT NULL,
  `longitudeEntreprise` text NOT NULL,
  `latitudeEntreprise` text NOT NULL,
  `etatEntreprise` int(11) NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`idEntreprise`, `nomEntreprise`, `interlocuteur`, `telEntreprise`, `emailEntreprise`, `mdpEntreprise`, `adresseEntreprise`, `longitudeEntreprise`, `latitudeEntreprise`, `etatEntreprise`) VALUES
(24, 'a', 'a', 'a', 'a', 'I2lDicPbKTpuAUYYJDHF/IYobs61q8oRao7+/cLbBrP13i0QxSdzrDlYqyr97FfQaFl8K9rvy1KMCE339wONJg==', 'Augsbourg, Allemagne', '10.897789999999986', '48.3705449', 1),
(25, 'b', 'z', 'z', 'b@hot.fr', 'BYQSQK+jQnkJ8l2stusw0vJoJPaDT2G4v5SscmZioWP3NTEXum9MQy36fW+AtRt8BkTv2pS81BQ2pebFU2f5Rw==', 'Finhan, France', '1.2206379000000425', '43.912619', 1);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE IF NOT EXISTS `etat` (
  `idEtat` int(11) NOT NULL DEFAULT '0',
  `libelleEtat` varchar(150) NOT NULL,
  PRIMARY KEY (`idEtat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etat`
--

INSERT INTO `etat` (`idEtat`, `libelleEtat`) VALUES
(0, 'supprimé'),
(1, 'active'),
(2, 'depublié');

-- --------------------------------------------------------

--
-- Structure de la table `fiche_conseil`
--

CREATE TABLE IF NOT EXISTS `fiche_conseil` (
  `idFiche` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `dateCreation` date NOT NULL,
  `etatFiche` tinyint(1) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY (`idFiche`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Contenu de la table `fiche_conseil`
--

INSERT INTO `fiche_conseil` (`idFiche`, `titre`, `description`, `dateCreation`, `etatFiche`, `idUtilisateur`) VALUES
(73, 'gtg', '<p>gtrgrtg</p>', '2016-05-25', 2, 1),
(74, 'ggh', '<p>ggggg</p>', '2016-05-25', 1, 1),
(75, 'ggh', '<p>ggggg</p>', '2016-05-25', 1, 1),
(76, 'ggh', '<p>gggggdede</p>', '2016-05-25', 1, 1),
(77, 'aaahyh112', '<p><strong>aaaaahy</strong></p>', '2016-05-25', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `fiche_type_energie`
--

CREATE TABLE IF NOT EXISTS `fiche_type_energie` (
  `idFiche` int(11) NOT NULL,
  `idTypeEnergie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fiche_type_energie`
--

INSERT INTO `fiche_type_energie` (`idFiche`, `idTypeEnergie`) VALUES
(73, 1),
(73, 3),
(73, 5),
(74, 2),
(74, 4),
(75, 2),
(75, 4),
(76, 2),
(76, 4);

-- --------------------------------------------------------

--
-- Structure de la table `indisponibilite`
--

CREATE TABLE IF NOT EXISTS `indisponibilite` (
  `idIndispo` int(11) NOT NULL AUTO_INCREMENT,
  `intituleIndispo` varchar(150) NOT NULL,
  `dateDebutIndispo` date NOT NULL,
  `dateFinIndispo` date NOT NULL,
  `heureDebutIndispo` time DEFAULT NULL,
  `heureFinIndispo` time DEFAULT NULL,
  `idAnnexe` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `etatIndispo` int(11) NOT NULL,
  PRIMARY KEY (`idIndispo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `indisponibilite`
--

INSERT INTO `indisponibilite` (`idIndispo`, `intituleIndispo`, `dateDebutIndispo`, `dateFinIndispo`, `heureDebutIndispo`, `heureFinIndispo`, `idAnnexe`, `idUtilisateur`, `etatIndispo`) VALUES
(23, 'jour de congé', '2016-06-15', '2016-06-15', NULL, NULL, 4, 1, 1),
(24, 'jour férie', '2016-06-16', '2016-06-16', NULL, NULL, 0, 0, 1),
(25, 'jour ferié', '2016-06-16', '2016-06-16', NULL, NULL, 0, 0, 1),
(26, 'ferie', '2016-06-16', '2016-06-16', NULL, NULL, 0, 0, 1),
(27, 'jour ferié', '2016-06-16', '2016-06-16', NULL, NULL, 0, 0, 1),
(28, 'sss', '2016-06-16', '2016-06-16', NULL, NULL, 4, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

CREATE TABLE IF NOT EXISTS `planning` (
  `idPlanning` int(11) NOT NULL AUTO_INCREMENT,
  `lundi_matin_deb` time DEFAULT NULL,
  `lundi_matin_fin` time DEFAULT NULL,
  `lundi_midi_deb` time DEFAULT NULL,
  `lundi_midi_fin` time DEFAULT NULL,
  `mardi_matin_deb` time DEFAULT NULL,
  `mardi_matin_fin` time DEFAULT NULL,
  `mardi_midi_deb` time DEFAULT NULL,
  `mardi_midi_fin` time DEFAULT NULL,
  `mercredi_matin_deb` time DEFAULT NULL,
  `mercredi_matin_fin` time DEFAULT NULL,
  `mercredi_midi_deb` time DEFAULT NULL,
  `mercredi_midi_fin` time DEFAULT NULL,
  `jeudi_matin_deb` time DEFAULT NULL,
  `jeudi_matin_fin` time DEFAULT NULL,
  `jeudi_midi_deb` time DEFAULT NULL,
  `jeudi_midi_fin` time DEFAULT NULL,
  `vendredi_matin_deb` time DEFAULT NULL,
  `vendredi_matin_fin` time DEFAULT NULL,
  `vendredi_midi_deb` time DEFAULT NULL,
  `vendredi_midi_fin` time DEFAULT NULL,
  `idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY (`idPlanning`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=99 ;

--
-- Contenu de la table `planning`
--

INSERT INTO `planning` (`idPlanning`, `lundi_matin_deb`, `lundi_matin_fin`, `lundi_midi_deb`, `lundi_midi_fin`, `mardi_matin_deb`, `mardi_matin_fin`, `mardi_midi_deb`, `mardi_midi_fin`, `mercredi_matin_deb`, `mercredi_matin_fin`, `mercredi_midi_deb`, `mercredi_midi_fin`, `jeudi_matin_deb`, `jeudi_matin_fin`, `jeudi_midi_deb`, `jeudi_midi_fin`, `vendredi_matin_deb`, `vendredi_matin_fin`, `vendredi_midi_deb`, `vendredi_midi_fin`, `idUtilisateur`) VALUES
(93, '08:24:00', '11:24:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41),
(95, NULL, NULL, '15:30:00', '17:00:00', '09:00:00', '10:00:00', '14:00:00', '16:00:00', NULL, NULL, '15:20:00', '16:20:00', NULL, NULL, '14:30:00', NULL, '09:30:00', '10:30:00', NULL, NULL, 42),
(98, '09:00:00', '11:00:00', '15:30:00', '17:00:00', '09:00:00', '10:00:00', '14:00:00', '16:00:00', '11:00:00', '13:00:00', NULL, NULL, '09:30:00', '11:30:00', '14:30:00', '16:30:00', '09:30:00', '10:30:00', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rendez_vous`
--

CREATE TABLE IF NOT EXISTS `rendez_vous` (
  `idRdv` int(11) NOT NULL AUTO_INCREMENT,
  `date_deb` date NOT NULL,
  `date_fin` date NOT NULL,
  `etatRdv` int(11) NOT NULL,
  `idConseilleri` int(11) NOT NULL,
  `idEntreprise` int(11) NOT NULL,
  PRIMARY KEY (`idRdv`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `idSlider` int(11) NOT NULL AUTO_INCREMENT,
  `titreSlider` varchar(50) NOT NULL,
  `descriptionSlider` varchar(250) NOT NULL,
  `imgSlider` text NOT NULL,
  `lienSlider` varchar(500) NOT NULL,
  `nomLienSlider` varchar(250) NOT NULL,
  `ordreSlider` int(11) NOT NULL,
  PRIMARY KEY (`idSlider`)
<<<<<<< HEAD
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `slider`
--

INSERT INTO `slider` (`idSlider`, `titreSlider`, `descriptionSlider`, `imgSlider`, `lienSlider`, `nomLienSlider`, `ordreSlider`) VALUES
(16, 'Slide 1', 'Test slide 1', 'Charlotte-Pirroni-Miss-Cote-d-Azur-2014-et-son-petit-ami-Florian-Thauvin_max1024x768.png', 'https://www.google.fr/#gws_rd=ssl', 'google', 1),
(17, 'Slide 2', 'Test 2 ', '8dmX91R5un2TC6ySldLfmUiGC8LGK7BLYYYd3hoyf65GKA2vkc12ZlVZ7vWOKwFp-black-light-dark-figures-2560x1440.png', 'http://localhost/stopGaspi/index.php/Accueil/index', 'Accueil', 2);
>>>>>>> dev

-- --------------------------------------------------------

--
-- Structure de la table `type_energie`
--

CREATE TABLE IF NOT EXISTS `type_energie` (
  `idTypeEnergie` int(11) NOT NULL AUTO_INCREMENT,
  `libelleTypeEnergie` varchar(50) NOT NULL,
  `accrocheTypeEnergie` varchar(150) NOT NULL,
  `descriptionTypeEnergie` varchar(300) NOT NULL,
  `imgTypeEnergie` text NOT NULL,
  `contenuTypeEnergie` text NOT NULL,
  PRIMARY KEY (`idTypeEnergie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `type_energie`
--

INSERT INTO `type_energie` (`idTypeEnergie`, `libelleTypeEnergie`, `accrocheTypeEnergie`, `descriptionTypeEnergie`, `imgTypeEnergie`, `contenuTypeEnergie`) VALUES
(1, 'Thérmique', 'Thérmique 1', 'Thérmique 2', 'etoile.png', 'Thérmique 3'),
(2, 'Carburant ', 'Carburant 1', 'Carburant 2', 'pcFixe.png', 'Carburant 3'),
(3, 'Consommables de bureau', 'Consommables de bureau 1 ', 'Consommables de bureau 2', 'carnet.png', 'Consommables de bureau 3'),
(4, 'Recyclage matériel électronique', 'Recyclage matériel électronique 1', 'Recyclage matériel électronique 2', 'pcFixe1.png', 'Recyclage matériel électronique 3'),
(5, 'Électrique', 'Électrique 1', 'Électrique 2', 'etoile1.png', 'Électrique 3');

-- --------------------------------------------------------

--
-- Structure de la table `type_utilisateur`
--

CREATE TABLE IF NOT EXISTS `type_utilisateur` (
  `idTypeUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `lblTypeUtilisateur` varchar(55) NOT NULL,
  `gestionFiche` tinyint(1) NOT NULL,
  `gestionActu` tinyint(1) NOT NULL,
  `gestionVideo` tinyint(1) NOT NULL,
  `gestionSite` tinyint(1) NOT NULL,
  `droitConseiller` tinyint(1) NOT NULL,
  `gestionRdv` tinyint(1) NOT NULL,
  `gestionUtilisateur` tinyint(1) NOT NULL,
  `gestionAnnexe` tinyint(1) NOT NULL,
  `gestionDroit` tinyint(1) NOT NULL,
  PRIMARY KEY (`idTypeUtilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `type_utilisateur`
--

INSERT INTO `type_utilisateur` (`idTypeUtilisateur`, `lblTypeUtilisateur`, `gestionFiche`, `gestionActu`, `gestionVideo`, `gestionSite`, `droitConseiller`, `gestionRdv`, `gestionUtilisateur`, `gestionAnnexe`, `gestionDroit`) VALUES
(1, 'Super Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'Rédacteur', 1, 1, 1, 1, 0, 0, 0, 0, 0),
(7, 'Conseiller', 0, 0, 0, 0, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nomUtilisateur` varchar(50) NOT NULL,
  `prenomUtilisateur` varchar(50) NOT NULL,
  `emailUtilisateur` varchar(100) NOT NULL,
  `mdpUtilisateur` text NOT NULL,
  `imgUtilisateur` text NOT NULL,
  `etatUtilisateur` int(11) NOT NULL,
  `idTypeUtilisateur` int(11) NOT NULL,
  `idAnnexe` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `nomUtilisateur`, `prenomUtilisateur`, `emailUtilisateur`, `mdpUtilisateur`, `imgUtilisateur`, `etatUtilisateur`, `idTypeUtilisateur`, `idAnnexe`) VALUES
(1, 'Dourlens', 'Thomas', 'a', 'q0Sv5ddBVqRVWTq4qGMIeeybCVHGSEVCb7thA4OC2DWTR8ZdZ/Fn/V/kt5njUFywag5BJOd+97QtoqC3rovYjA==', 'gus_dd.png', 1, 1, 4),
(41, 'Claude', 'Fabien', '1', 'BclE22lKHSXd3ENLBPaARBp2PhjlP6VuvEV6mV21WNuDntw8N2m07I1GRAKbIEOT30vLr5lF7eSGNmqp0ikUxg==', 'avatar_default.png', 1, 7, 4),
(42, 'Castagnet', 'Fabrice', '4', 'rTg2fBVNczpONLyU+GLegFO1QAkhEQrSu2wDMeDwxmigz4Q2Aur0Vo2AkJYhrQNY/7wenLaZ305ELGMsrk19GQ==', 'Siberischer_tiger_de_edit023.jpg', 1, 7, 5),
(43, 'Admin', 'Admin', '55', 'Wil9oBJhzzpd9Z9qu7uah0vvBvqy1+3HJcf1pUdLmTQtpQGnrCyFezxNSiytKGwdsinnBU6acnC4GTW82vugbA==', 'avatar_default.png', 1, 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `idVideo` int(11) NOT NULL AUTO_INCREMENT,
  `titreVideo` varchar(25) NOT NULL,
  `descriptionVideo` text NOT NULL,
  `urlVideo` text NOT NULL,
  `dateCreation` date NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `etatVideo` int(11) NOT NULL,
  PRIMARY KEY (`idVideo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Contenu de la table `video`
--

INSERT INTO `video` (`idVideo`, `titreVideo`, `descriptionVideo`, `urlVideo`, `dateCreation`, `idUtilisateur`, `etatVideo`) VALUES
(29, 'keny arkana', 'efefef', 'https://www.youtube.com/embed/NFTP52_Q6tQ', '2016-05-26', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `video_type_energie`
--

CREATE TABLE IF NOT EXISTS `video_type_energie` (
  `idVideo` int(11) NOT NULL,
  `idTypeEnergie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `video_type_energie`
--

INSERT INTO `video_type_energie` (`idVideo`, `idTypeEnergie`) VALUES
(29, 2),
(29, 3),
(29, 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
