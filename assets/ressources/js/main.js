var urlAssets = "https://stopgaspi-clofab.c9users.io/assets";
var urlApp = "https://stopgaspi-clofab.c9users.io/index.php";



/**************************************************************************
 *                             SUPPRESSION                                *
 *************************************************************************/


var Suppression = (function () {

    var Suppression = {};

    //SUPPRIME UNE DONNEE DANS TABLEAU
    Suppression.delete = function(url,that){
        var table = that.closest('table').DataTable();
        var tr = that.closest('tr');
        var id = parseInt(tr.attr('id'));

        $.ajax({
            method: "POST",
            url: url,
            dataType:"json",
            data: { id: id},
            success:function(){
                table.row(tr).remove().draw(false);
            }
        });
    };

    // SUPPRIME SELON LES CASE COCHES
    Suppression.deleteChecks = function(nameCheck,url){

        var table = $(nameCheck).closest('table').DataTable();
        var idCheck = [];
        var trCheck = [];
        var nameImg = [];

        $(nameCheck+":checked").each(function(){
            idCheck.push(parseInt($(this).closest('tr').attr('id')));
            trCheck.push($(this).closest('tr'));
            //urlImg = $(this).closest('tr').find('img').attr('src').split("/");
            //nameImg.push(urlImg[urlImg.length-1])
        });

        $.ajax({
            method: "POST",
            url: url,
            dataType:"json",
            data: { id: idCheck , tabImg: nameImg },
            success:function(){
                for (i = 0; i < idCheck.length; i++ ){
                    table.row(trCheck[i]).remove().draw(false);
                }
            }
        });
    };

    return Suppression
})();


/**************************************************************************
 *                     PUBLICATION   / DEPUBLICATION                      *
 *************************************************************************/


var Publication = (function () {

    var Publication = {};

    Publication.publishOne  = function(url,checkbox,tab){ // si TAB a true alors a supprimé du tableau
        var etat = 2;
        var id = checkbox.closest('tr').attr('id');
        if(checkbox.is(':checked')){
            etat = 1;
        }
        $.ajax({
            method: "POST",
            url: url,
            dataType:"json",
            data: { id: id, etat:etat },
            success:function(data){

                if(data == true) { // Si pas de probleme

                    if (tab == true) { //Si doit être supprimé d'un tableau

                        var table = checkbox.closest('table').DataTable();
                        var tr = checkbox.closest('tr');
                        var id = parseInt(tr.attr('id'));

                        table.row(tr).remove().draw(false);
                    }

                }else{  // Sinon si l'element supprimé est relié a d'autre tab en bdd on affiche une alerte

                    switch(data) {
                        case "annexe":
                            msg = "Attention des utilisateurs sont relié a cette annexe, changer leur annexe pour pouvoir supprimer cette annexe"
                            break;
                    }
                    afficheAlert('.pageContaint','danger',msg);
                }
            }
        })
    };

    Publication.publishChecks  = function(nameCheck,url,etat){

        var table = $(nameCheck).closest('table').DataTable();
        var idCheck = [];

        $(nameCheck+":checked").each(function(){
            idCheck.push(parseInt($(this).closest('tr').attr('id')))
        });

        $.ajax({
            method: "POST",
            url: url,
            dataType:"json",
            data: { id: idCheck, etat:etat },
            success:function(){

                etat = etat == "1" ? true : false;
                for (var i = 0; i < idCheck.length; i++) {
                    $('table tr#'+idCheck[i]+' .etatPublish').prop('checked',etat)
                }
            }
        })
    };

    return Publication
})();


/**************************************************************************
 *                             FORMULAIRE                                *
 *************************************************************************/

var Form = (function () {

    var Form = {};

    // VERIFICATION CHAMPS
    Form.verifInput = function (el) {

        if (el.val() == "" || el.val() == null) {
            el.closest('.form-group').addClass('has-error');
            return false
        } else {
            el.closest('.form-group').removeClass('has-error');
            return el.val()
        }
    };


    // VERIFICATION CHAMPS
    Form.validForm = function (arrayForm) {

        for (i = 0; i < arrayForm.length; i++ ){

            if(arrayForm[i] == false){
                afficheAlert('.pageContaint','danger',"Attention, Formulaire incomplet")
               return false
            }

        }

    };

    // VERIFIE SI 2 MOT DE PASSE IDENTIQUE
    Form.verifMdp = function(mdp1,mdp2){
        if(mdp1.val() != mdp2.val()){
            afficheAlert('.pageContaint','danger','Attention vos mots de passe ne sont pas identique');
            mdp1.closest('.form-group').addClass('has-error');
            mdp2.closest('.form-group').addClass('has-error');
            return false
        }else{
            mdp1.closest('.form-group').removeClass('has-error');
            mdp2.closest('.form-group').removeClass('has-error')
        }
    };

    // VERIFIE FORMAT EMAIL
    Form.verifEmail = function(email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(re.test(email.val()) == false){
            afficheAlert('.pageContaint','danger',"Attention, Format d'email incorrect");
            email.closest('.form-group').addClass('has-error');
            return false
        }else{
            email.closest('.form-group').removeClass('has-error')
        }
    };

    // RE-INIT FORMULAIRE
    Form.reset = function(){
        $('input').val('');    // remise a zero input
        $('.alert').remove();  // suppression des alerts
    };
    
    // VERIF INT
    Form.verifInt = function(val){
        
        if (isNaN(val)){
            return false
        }else{
            return true
        };
    };

    return Form

})();


/**************************************************************************
 *                             GOOGLE MAP                               *
 *************************************************************************/

var MapGoogle = (function () {

    var MapGoogle = {};

    var geocoder;
    var map;
    var marker;

    // INIT GOOGLE MAP
    MapGoogle.innit = function(innitMap){

        //MAP
        if( $('#longitude').val().length > 0) {
            var latlng = new google.maps.LatLng($('#latitude').val(),$('#longitude').val());
        } else{
            var latlng = new google.maps.LatLng(-34.6037232,-58.38159310000003);
        }

        // Acá agregamos las opciones del mapa
        var options = {
            zoom: 16,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map($("#geoSearch")[0], options);

        //GEOCODIFICACION
        geocoder = new google.maps.Geocoder();

        // Acá agregamos el marker al mapa creado
        marker = new google.maps.Marker({
            map: map,
            // Lo hacemos arrastrable
            draggable: true,
            // Lo posicionamos sobre la lat y long recibida
            position: latlng,
            title:"Si el domicilio que desea agendar no es este, por favor mueva este marcador a la posición correcta."
        });
    };


    MapGoogle.autoComplete = function(){

            $("#address").autocomplete({
                source: function(request, response) {
                    geocoder.geocode( {'address': request.term }, function(results, status) {
                        response($.map(results, function(item) {
                            return {
                                label:  item.formatted_address,
                                value: item.formatted_address,
                                latitude: item.geometry.location.lat(),
                                longitude: item.geometry.location.lng()
                            }
                        }));
                    })
                },

                select: function(event, ui) {
                    $("#latitude").val(ui.item.latitude);
                    $("#longitude").val(ui.item.longitude);
                    var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                    marker.setPosition(location);
                    map.setCenter(location);
                }
            });

    }

    MapGoogle.addMarker = function(){
        //Agregar un listener al marcador para que devuelva la direccion si se lo mueve
        google.maps.event.addListener(marker, 'drag', function() {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                    }
                }
            });
        });
    }

    return MapGoogle

})();

function afficheAlert(el,typeError,msg){

    if($('.alert-danger').length > 0)     $('.alert-danger').remove();  //  SI ALERTE DEJA PRESENTE ALORS ON LA SUPPRIME

    $(el).prepend('<div class="alert alert-'+typeError+' alert-dismissible" role="alert">'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
        '<label>'+msg+'</label>'+
    '</div>')
}


// LIBELLE EN FRANCAIS POUR DATATABLE.JS
var langage = {
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
        "sNext":       "Suivant",
        "sLast":       "Dernier"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
    }
}


$(document).ready(function() {


    if($('.description').length > 0 && $('.no-tiny').length == 0){
        
        // INIT WISIWYG
        tinymce.init({
            selector: '.description',
            height: 500,
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            menubar:false,
            statusbar: false,
            plugins: "image imagetools",
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            }
    
        });
    }

    $('select.selEmpty').prop('selectedIndex', -1); // select vide

    // SELECTIONNE TOUTES LES CHECKBOX
    $('.checkAll').on('change', function () {
        var idTab = $(this).closest('table');

        //SI CASE CHECKER
        if ($(this).is(':checked')) {
            idTab.find('tr>td:first-child input[type=checkbox]').prop('checked', true)
        } else {
            idTab.find('tr>td:first-child input[type=checkbox]').prop('checked', false)
        }

    })

});
