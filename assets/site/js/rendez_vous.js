 
var idConseiller; 
var dateDebut;
 
function getMyHour(myDate){
    var heureDebut;
    var minuteDebut;
    var timeDebut;
    
    heureDebut = myDate.getHours(); 
    minuteDebut = myDate.getMinutes(); 
    
    if (heureDebut  < 10) heureDebut   = "0"+heureDebut;
    if (minuteDebut  < 10) minuteDebut   = "0"+minuteDebut;
    timeDebut = heureDebut+':'+minuteDebut;

    return timeDebut;
}

function getMyDate(debutEvnt){
    dd = debutEvnt.getDate();
    mm = debutEvnt.getMonth()+1; 
    yyyy = debutEvnt.getFullYear();
    hh = debutEvnt.getHours(); 
    mn = debutEvnt.getMinutes(); 

    if(dd<10)  dd ='0'+dd
    if(mm<10)  mm ='0'+mm
    if(hh<10)  hh ='0'+hh;
    if(mn<10)  mn ='0'+mn;

    return yyyy+'-'+mm+'-'+dd+' '+hh+':'+mn+':00';
}
$(document).ready(function(){

    $('#calendar').fullCalendar({
        minTime: "07:00:00",
        maxTime: "19:00:00",
        weekends: false,
        lang: 'fr',

        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        defaultView: 'agendaWeek',
        editable: false,
        eventRender: function(event, element, view)
        {
            if(event.start._d < new Date()){
                return false    
            }
        },
        
        eventClick: function(calEvent, jsEvent, view) {

            var debutEvnt = dateDebut = new Date(calEvent.start._d.setHours(calEvent.start._d.getHours() - 2)); 
            var finEvnt   = new Date(calEvent.end._d.setHours(calEvent.end._d.getHours() - 2));  
            idConseiller = calEvent.idConseiller
            var idAnnexe = $('select#selectVille option:selected').val()
            
            // ON RECUPER LA DATE CONVERTIT AU FORMAT FR

            var dateRdv = getMyDate(debutEvnt);
            var dateFinRdv = getMyDate(finEvnt);
            
            // dateDebut = yyyy+'/'+mm+'/'+dd; //FORMAT POUR BASE DE DONNES SI VALID RDV
            jour = dd+'/'+mm+'/'+yyyy; 
        
            // HEURE DE DEBUT
            timeDebut = getMyHour(debutEvnt)
            timeFin = getMyHour(finEvnt)
          
            $('#cadreRdv').modal('show');
            $('#dateRdv').text(jour);
            $('#heureDeb').text(timeDebut);
            $('#heureFin').text(timeFin);
            $('#conseiller').text(calEvent.title+" "+calEvent.prenomConseiller);
            
            var diff =  (finEvnt - debutEvnt )
            var diffMins = (diff / 60000)
            var nbDispo = Math.round(diffMins / 30) 
            $('#heureDispo option').remove()
            
            
            // VERIFICATION SI DES RDV ONT DEJA ETE PRIS A CETTE DATE
            $.ajax({
                type:"POST",
                dataType:"json",
                url:urlApp+"/Front/verifDispo",
                data:{dateRdv : dateRdv, dateFinRdv : dateFinRdv, idConseiller : idConseiller, idAnnexe : idAnnexe},
                success:function(data) {

                   console.log(data)
                    //$('#heureDispo').append('<option></option>')
                    for(i=0; i < nbDispo; i++){
                        
                        // CREATION DE PLAGE HORRAIRE DE 30mn
                        plageDeb = new Date(debutEvnt.getTime() + (30 * (60 * i)* 1000));
                        plageFin = new Date(debutEvnt.getTime() + (30 * (60 * (i + 1))* 1000));
        
                        plageHeureDebut = getMyHour(plageDeb)
                        plageHeureFin = getMyHour(plageFin)
                        
                        dispo = true
                        
                        // VERIFICATION DES RENDEZ VOUS DISPONIBLE
                        for(y=0; y < data.rdv.length; y++){
                            
                            // SI LA PLAGE HORRAIRE N'EST PLUS DISPONIBLE
                            if(plageHeureDebut+':00' == data.rdv[y].heure_deb)    dispo = false
                            
                        }
                        
                        var run
                        // VERIFICATION SI INDISPO DANS PLAGE HORRAIRE
                        for(y=0; y < data.indispo.length; y++){
                            
                            // SI AU PREALABLE ON A SUPPRIMER UNE PLAGE VIA UNE INDIPO, ON VERIFIE POUR LES SUIVANTES SELON LA FIN DE L'INDISPO
                            if(run == true){
                                debutIndispo = new Date("2016-01-01 "+data.indispo[y].heureDebutIndispo)
                                debutIndispo = new Date(debutIndispo.getTime() + (30 * (60 * 1)* 1000))
                                
                                finIndispo = new Date("2016-01-01 "+data.indispo[y].heureFinIndispo)

                                if(debutIndispo >= finIndispo){
                                   run = false
                                } else{
                                    dispo = false
                                }
                            }
                            
  
                             if(plageHeureDebut+':00' == data.indispo[y].heureDebutIndispo){
                                 console.log(0)
                                 dispo = false
                                 run = true
                             }else{
                                 run = false 
                             }    
    
                        }

                        
                        // SI PLAGE NON BLOQUER PAR INDISPO OU RDV ALORS ON AFFICHE LA PLAGE
                        if (dispo == true){
                            $('#heureDispo').append('<option data-deb="'+plageHeureDebut+'" data-fin="'+plageHeureFin+'">'+plageHeureDebut+' à '+plageHeureFin+'</option>')
                        }
                        
                    }
                    
                   
                    debutEvnt = new Date(calEvent.start._d.setHours(calEvent.start._d.getHours() + 2)); 
                    finEvnt   = new Date(calEvent.end._d.setHours(calEvent.end._d.getHours() + 2));  
                }
            })
        }
        
    
    })


    // SELECT DE L'ANNEXE
    $(document).on('change','#villeAnnexe',function() {
        addEventCalendar()
        $('#calendar').fullCalendar( 'removeEvents').fullCalendar('removeEventSources');
    })

    function addEventCalendar(){

        var idAnnexe = $('#villeAnnexe option:selected').val()
        var newEvent = new Object();
        var numJour = 0

        $.ajax({
            type:"POST",
            dataType:"json",
            url:urlApp+"/Front/getHorraire",
            data:{idAnnexe : idAnnexe},
            success:function(data) {

                //  POUR CHAQUE CONSEILLER DE L'ANNEXE
                for (i = 0; i < data.plage.length; i++) {
                    
                    numJour = 0
                    // ON STOCK LES HORRAIRES DANS UN ARRAY
                    tabJour = {
                        debut: [
                            data.plage[i].lundi_matin_deb,
                            data.plage[i].lundi_midi_deb,
                            data.plage[i].mardi_matin_deb,
                            data.plage[i].mardi_midi_deb,
                            data.plage[i].mercredi_matin_deb,
                            data.plage[i].mercredi_midi_deb,
                            data.plage[i].jeudi_matin_deb,
                            data.plage[i].jeudi_midi_deb,
                            data.plage[i].vendredi_matin_deb,
                            data.plage[i].vendredi_midi_deb,
                        ],
                        fin: [
                            data.plage[i].lundi_matin_fin,
                            data.plage[i].lundi_midi_fin,
                            data.plage[i].mardi_matin_fin,
                            data.plage[i].mardi_midi_fin,
                            data.plage[i].mercredi_matin_fin,
                            data.plage[i].mercredi_midi_fin,
                            data.plage[i].jeudi_matin_fin,
                            data.plage[i].jeudi_midi_fin,
                            data.plage[i].vendredi_matin_fin,
                            data.plage[i].vendredi_midi_fin,
                        ]
                    };

                    // ON PLACE LES HORRAIRES DE BASE DANS LE PLANNING
                    for (y = 0; y <= 10; y++) {

                        // ON AJOUTE UN JOUR
                        if (y % 2 == 0)  numJour++;
                        
                        if (tabJour.debut[y] != null) {
 
                           // newEvent.id = data.plage[i].idPlanning;
                            newEvent.title = data.plage[i].nomUtilisateur;
                            newEvent.start = tabJour.debut[y];
                            newEvent.end = tabJour.fin[y];
                            newEvent.allDay = false;
                            newEvent.color = 'green';
                            newEvent.dow = [numJour]; // Y REPRESENTE LE JOUR [1=lundi,2=mardi...}
                            newEvent.idConseiller =  data.plage[i].idUtilisateur
                            newEvent.prenomConseiller =  data.plage[i].prenomUtilisateur
                           
                            $('#calendar').fullCalendar('renderEvent', newEvent);
                        }
                    }
                }

                var nbJour = 0; var dateDebut; var dateFin; var heureDebut; var heureFin; var idUtilisateur; var nomUtilisateur; var dateDebutFormat; var dateFinFormat
                var jour = 24*60*60*1000;
                
                // ON RECUPERE LES INDISPONIBILITES
                for (i = 0; i < data.indispo.length; i++) {
                    
                    //  POUR CHAQUE INDISPONIBILITE
                    
                    // ON RECUPERE LES ELEMENTS DE L'INDISPONIBILITE
                    dateDebut = data.indispo[i].dateDebutIndispo;
                    dateFin = data.indispo[i].dateFinIndispo;
                    heureDebut = parseInt(data.indispo[i].heureDebutIndispo);
                    heureFin = parseInt(data.indispo[i].heureFinIndispo);
                    idUtilisateur = parseInt(data.indispo[i].idUtilisateur)
                    nomUtilisateur = data.indispo[i].nomUtilisateur
                    
                    
                    // FORMAT DATE  
                    dateDebutFormat = new Date(dateDebut)
                    dateFinFormat = new Date(dateFin)
                    
                    // RECUPERATION DU NB DE JOUR DE L'INDISPONIBILTIE
                    nbJour = Math.round(Math.abs((dateDebutFormat.getTime() - new Date(dateFinFormat).getTime())/(jour)));
                    

                    // ON VERIFIE LES INDISPO POUR CHAQUE JOUR DE L'INDISPO
                    for(y = 0 ; y <= nbJour ; y++)  {  
                        
                        // RECUPERATION DES DONNEES POUR CHAQUE DATE DE CHAQUE JOUR DE L'INDISPONIBILITE  
                        
                        // SI Y = 0 ALORS JOUR MEME, DONC PAS  BESOIN DE RAJOUTER UN JOUR
                        if(y != 0){
                          dateDebutFormat = new Date(dateDebutFormat.setDate(dateDebutFormat.getDate() + 1))
                        }
                      
                        dd = dateDebutFormat.getDate();
                        mm = dateDebutFormat.getMonth()+1; 
                        yyyy = dateDebutFormat.getFullYear();
                        
                        if(dd<10)  dd='0'+dd
                        if(mm<10)  mm='0'+mm

                        today = yyyy+'-'+mm+'-'+dd;

                        // ON RECUPERE L'ELEMENT DANS LE THEAD SELON LA DATE
                        rowDate = $('.fc-day-header[data-date="'+today+'"]')    
                    
                    
                        //  SI INDISPONIBILITE PRESENTE DANS LE PLANNING DE LA SEMAINE 
                        if(rowDate.length > 0 ){
                           
                            // ON RECUPERE L'INDEX (placement du th dans le thead)
                            tr = rowDate.index();      
                            
                            // POUR CHAQUE HEURE
                            for (h = heureDebut; h <= heureFin ; h++){
                                
                                if(h < 10){
                                    heure = '0'+h
                                }else{
                                    heure = h
                                }
                             
                                // ON RECUPERE LA CASE SELON L'HEURE  
                                el =  $('div.fc-content-skeleton:eq(1) table tr td:eq('+tr+') div.fc-time[data-start^="'+heure+'"]')             
                                
                                // SI POUR JUSTE UN CONSEILLER
                                if (!isNaN(idUtilisateur)){
                                     //  ON SUPPRIME LA CASE CONTENANT L'HEURE AVEC LE MÊME NOM CONSEILLER
                                    if(el.next().text() == nomUtilisateur){
                                        el.closest('.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end').remove()
                                    }
                                }else{
                                     //  ON SUPPRIME LA CASE CONTENANT L'HEURE
                                    el.closest('.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end').remove()
                                    
                                }
                                
                            }
                        }
                        
                    }
                    
                }
                
               
                // ON RECUPERE LES RDV
                for (i = 0; i < data.rdv.length; i++){

                    // Date debut de la plage horraire concernant le rdv
                    dateRdv = new Date(data.rdv[i].plageHorraire)
                    
                    
                    // ON RECUPER LA DATE CONVERTIT AU BON FORMAT 
                    dd = dateRdv.getDate();
                    mm = dateRdv.getMonth()+1; 
                    yyyy = dateRdv.getFullYear();
                    hh = dateRdv.getHours(); 
                    mn = dateRdv.getMinutes(); 
                
                    if(dd<10)  dd ='0'+dd
                    if(mm<10)  mm ='0'+mm
                    if(hh<10)  hh ='0'+hh;
                    if(mn<10)  mn ='0'+mn;
        
                    today = yyyy+'-'+mm+'-'+dd;
   
                    // ON RECUPERE L'ELEMENT DANS LE THEAD SELON LA DATE
                    rowDate = $('.fc-day-header[data-date="'+today+'"]')    
                
                    //  SI RDV PRESENTE DANS LE PLANNING DE LA SEMAINE 
                    if(rowDate.length > 0 ){
                       
                        // ON RECUPERE L'INDEX (placement du th dans le thead)
                        tr = rowDate.index();    
                        
                        // ON RECUPERE LA CASE SELON L'HEURE  
                        el =  $('div.fc-content-skeleton:eq(1) table tr td:eq('+tr+') div.fc-time[data-start^="'+hh+'"]')  
                        
                        // ON RECUPERE L'HEURE DE FIN
                        heure_fin = el.attr('data-full').replace(/\s/g, '');
                        heure_fin = heure_fin.split('-')
                        heure_fin = heure_fin[1]
                        
                        heure_min_fin = heure_fin.split(':')
                        
                        // ON RECUPERE LE NOMBRE DE RDV POSSIBLE ENTRE LE DEBUT ET LA FIN (30mn par rdv)
                        date_fin = new Date(yyyy, parseInt(mm-1), dd, heure_min_fin[0], heure_min_fin[1])
                        var diff =  (date_fin - dateRdv);
                        var diffMins = (diff / 60000)
                        var nbDispo = Math.round(diffMins / 30) 
                        
                        // SI TOUTES LES RDV SONT PRIS ALORS ON SUPPRIME LA PLAGE EN QUESTION
                        if (nbDispo == parseInt(data.rdv[i].nbPlage)){
                             el.closest('.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end').remove()
                        }
                       
                    }
                }
            }
        })
    }



    if($('#calendar').length > 0)  addEventCalendar()
   

    $(document).on('click',".fc-button",function(){
        addEventCalendar()
    })
    
    
    // BOUTON PRENDRE RENDEZ VOUS
    $(document).on('click','#validRdv',function(){
        
        var dd = dateDebut.getDate();
        var mm = dateDebut.getMonth()+1; 
        var yyyy = dateDebut.getFullYear();
        var hh = dateDebut.getHours(); 
        var mn = dateDebut.getMinutes(); 
        
        if(dd<10)  dd ='0'+dd
        if(mm<10)  mm ='0'+mm
        if(hh<10)  hh ='0'+hh;
        if(mn<10)  mn ='0'+mn;

        var dateRdv = yyyy+'-'+mm+'-'+dd+' '+hh+':'+mn+':00';
        
        var heureDeb = $('#heureDispo option:selected').attr('data-deb')
        var heureFin = $('#heureDispo option:selected').attr('data-fin')
        
        //VERIFICATION SI POSSEDE DEJA UN RDV
        $.ajax({
            type:"POST",
            dataType:"json",
            url:urlApp+"/Front/verifRdv",
            success:function(data) {

                // SI DEJA UN RENDEZ VOUS DE PREVU
                if(data != ""){

                    var dateProchainRdv = data[0].plageHorraire.substring(0, 10)
                    var heureProchainRdv = data[0].heure_deb
                    dateProchainRdv = dateProchainRdv.split('-').reverse().join('/')
                    $('#alertRdv').removeClass('hide')
                    $('#dateProchainRdv').text(dateProchainRdv+" à "+heureProchainRdv.substring(0, 5));

                }else{
                          
                    //  SINON SI AUCUN RENDEZ VOUS DE PREVU   
                    $.ajax({
                        type:"POST",
                        dataType:"json",
                        url:urlApp+"/Front/saveRdv",
                        data:{idConseiller : idConseiller, dateRdv : dateRdv, heureDeb : heureDeb, heureFin : heureFin },
                        success:function(data) {
                            $('div#confirmRdv').modal('show')
                        }
                    })
                    
                    
                }
               
            }
        })

    })
    
    
    // FERMETURE DU MODAL
    $('#confirmRdv').on('hidden.bs.modal', function () {
    	window.location.href = urlApp;
    })
})