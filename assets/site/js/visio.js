$("#homeVisio button").click(function(){
    $("#homeVisio").hide();
    $("#waitVisio").show();
    var callingTo = "";
    var socket = io.connect('https://stopgaspi-webrtc-server-clofab.c9users.io/'),
    session, duplicateMessages = [];
    roomName = 'stop_gaspi-'+username
	socket.emit('create',roomName);    
    socket.emit('login', username);
    socket.on('connect_failed', function(message){
        console.log('login_error');
    });
    call(false);
	    
    socket.on('login_error', function(message){
        console.log('login_error');
    });

    socket.on('login_successful', function(users){
        console.log('login_successful');
    });

    socket.on('online', function(user){
        console.log(user);
    });
    socket.on('conseiller', function(user){
        callingTo = user;   
        $("#nomVisio").text("Votre conseiller "+callingTo.replace("_"," "));
    });

    socket.on('offline', function(name){
        if(name === callingTo){
            $('#cadreVisio').hide();
            $('#homeVisio').show();
            session.close();
        }
    });

    socket.on('disconnect', function(){
        if(callingTo){
            $('#cadreVisio').hide();
            $('#homeVisio').show();
            session.close();
        }
    });

    function call(isInitiator){
       $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://api.xirsys.com/ice",
            data: {
                    ident: "stopgaspi",
                    secret: "2d2cba28-4361-11e6-b62f-f8cffe0906a6",
                    domain: "stopgaspi-clofab.c9users.io",
                    application: "stopgaspi",
                    room: "stopgaspi",
                    secure: 1
                },
            success: function (data, status) {
                console.log("call started")
                startCall(data.d.iceServers[2], isInitiator);
            },
            async: false
        });
    }

    function startCall(data, isInitiator){
        var config = {
            isInitiator: isInitiator,
            turn: {
                host: data.url,
                username: data.username,
                password: data.credential
            },
            streams: {
                audio: true,
                video: true
            }
        }
        session = Session("using",config, function (data) {
            socket.emit('sendMessage', callingTo, { 
              type: 'phonertc_handshake',
              data: JSON.stringify(data)
            });
        });
        
        setVideoView([{
            container: document.getElementById('remoteVideo'),
            containerParams: {
                position: [0, 50],
                size: [300, 300]
            },
            local: {
                position: [0, 50],
                size: [200, 200]
            }
        }]);
        $("#localVideo").css("position","static");
        $("#localVideo").css("width","100%");
        $("#localVideo").css("height","auto");
        socket.on('messageReceived', onVideoMessageReceived);
        socket.on('sendMessage', function (data) {
        	socket.emit('sendMessage', callingTo, { 
              type: 'phonertc_handshake',
              data: JSON.stringify(data)
            });
        });
        socket.on('camnotfound',function(){
            afficheAlert('#errorVisio', 'danger', "Votre conseiller n'est en état de communiquer.");
            window.location.reload(false); 
        })
        socket.on('answer', function () {
            console.log('answered');
        });
        socket.on('disconnect', function () {
            session.close();
            socket.emit('sendMessage', callingTo, { type: 'ignore' });
        });
        session.call();
        socket.emit('sendMessage', callingTo, {type: 'call'});
    	setTimeout(function(){
             socket.emit('sendMessage', callingTo, { type: 'answer' });
        }, 1500);
    }

    function onVideoMessageReceived(name, message){
    	switch (message.type){
            case 'call':
                callingTo = name;
                $('#waitVisio').hide();
                $('#cadreVisio').show();
                break;
            case 'answer':
                console.log(username + ' he answered');
                break;
            case 'phonertc_handshake':
                console.log("emit");
                if (duplicateMessages.indexOf(message.data) === -1) {
                	session.receiveMessage(JSON.parse(message.data));
                    duplicateMessages.push(message.data);
                }
                if($("#remoteVideo video").css("position") != "static"){
                    $("#remoteVideo video").css("position","static");
                    $("#remoteVideo video").css("width","100%");
                    $("#remoteVideo video").css("height","auto");
                    $("#remoteVideo video").css("width","100%");
                    $("#remoteVideo video").css("height","auto");
                    $("#remoteVideo video").css("top","");
                    $("#remoteVideo video").css("left","");
                    $("#remoteVideo video").css("transform","");
                }
                break;
            case 'ignore':
                /*need to add session close for other cases as well*/
                socket.emit("destroy",roomName);
                socket.emit("disconnect");
                $('#cadreVisio').hide();
                $('#homeVisio').show();
                session.close();
                window.location.reload(false); 
                break;
        }
    }
    $("#cadreVisio button").click(function(){
        socket.emit("destroy",roomName);
        socket.emit("disconnect");
        socket.emit('sendMessage', callingTo, { type: 'ignore' });
        session.close();
        $('#cadreVisio').hide();
        $('#homeVisio').show();
        window.location.reload(false); 
                
    })
})