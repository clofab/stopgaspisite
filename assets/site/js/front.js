


$(document).ready(function(){
    
    // SLIDER PAGE D'ACCUEIL
    if($('.bxslider').length > 0 ){
		$('.bxslider').bxSlider({
			pager:false,
			nextSelector: '#slider-next',
			prevSelector: '#slider-prev',
			nextText: "<i class='glyphicon glyphicon-chevron-left'></i>",
			prevText: "<i class='glyphicon glyphicon-chevron-right'></i>"
		});
    }


    // CARROUSSEL PAGE D'ACCUEIL
    if($('#owl-environnement').length > 0 ){
		$("#owl-environnement").owlCarousel({
			items : 3,
			navigation : true,
			navigationText : ["<i class='glyphicon glyphicon-chevron-left'></i>","<i class='glyphicon glyphicon-chevron-right'></i>"],
		});
    }


    if($("#needMap").length > 0){
        MapGoogle.innit()
        MapGoogle.autoComplete()
        //MapGoogle.addMarker()
    }

    // VERIF FORMULAIRE INSCRIPTION Entreprise
    $('#formEntreprise').on('submit',function(event){
        var nomEntreprise = Form.verifInput($('[name="nomEntreprise"]'));
        var interlocuteur = Form.verifInput($('[name="interlocuteur"]'));
        var telEntreprise = Form.verifInput($('[name="telEntreprise"]'));
        var emailEntreprise = Form.verifInput($('[name="emailEntreprise"]'));
        var adresseEntreprise = Form.verifInput($('[name="adresseEntreprise"]'));
        var mdpEntreprise = Form.verifInput($('[name="mdp"]'));
        var mdpEntreprise2 = Form.verifInput($('[name="mdp2"]'));

        var tabFormulare = [nomEntreprise, interlocuteur, telEntreprise,emailEntreprise,adresseEntreprise, mdpEntreprise, mdpEntreprise2];

        // SI FORMULAIRE NON COMPLET ALORS AFFICHE ALERTE
        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            afficheAlert('#formEntreprise', 'danger', "Attention, Formulaire incomplet");
            event.preventDefault();
            return false
        }
        
        // verifie format email
        if(Form.verifEmail($('[name="emailEntreprise"]')) == false){
            afficheAlert('#formEntreprise', 'danger', "Attention, format d'email non respécté");
            event.preventDefault();
            return false
        }

        // verifie mdp identique
        if(Form.verifMdp($('[name="mdp"]'),$('[name="mdp2"]')) == false){
            afficheAlert('#formEntreprise', 'danger', "Attention, les mots de passe doivent être identique");
            event.preventDefault();
            return false
        }
        
        // verifie longeur mdp
        if(mdpEntreprise.length < 6){
            afficheAlert('#formEntreprise', 'danger', "Attention, votre mot de passe doit contenir 6 caractère minimum");
            $('#mdp').closest('.form-group').addClass('has-error')
            $('#mdp2').closest('.form-group').addClass('has-error')
            event.preventDefault();

            return false
        }else{
            $('#mdp').closest('.form-group').removeClass('has-error')
            $('#mdp2').closest('.form-group').removeClass('has-error')

        }

        // // verifie adresse cohérente google map
        // if($("#latitude").val() == "" && $("#longitude").val() == ""){
        //     afficheAlert('#formEntreprise', 'danger', "Attention, veuillez renseigner une adresse cohérente");
        //     $('#adresseEntreprise').closest('.form-group').addClass('has-error')
        //     event.preventDefault();
        //     return false
        // }else{
        //     $('#adresseEntreprise').closest('.form-group').removeClass('has-error')
        // }
        

    });


    //  CONNEXION ENTREPRISE
    $(document).on('submit','#connexEntreprise',function(event){

        var emailEntreprise = Form.verifInput($('#emailEntreprise'))
        var mdpEntreprise = Form.verifInput($('#mdpEntreprise'))
        var tabFormulare = [emailEntreprise, mdpEntreprise]

        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            afficheAlert('.pageContaint', 'danger', "Attention, Formulaire incomplet");
            event.preventDefault();
        }
    })


    // CHANGEMENT DE PAGE CALCULETTE
    $(document).on('click',".changePageCalcul",function(){
        
        $("#calculetteSuiv").toggleClass('hide');
        $("#annee_prec").toggleClass('hide');
        
        $("#calculettePrec").toggleClass('hide');
        $("#annee_suiv").toggleClass('hide');
       
    })
    
    
    // BTN CALCULER 
    $(document).on('click',"#btnCalculer",function(){
        
        var erreur = false;
        var annee_prec = 0;
        var annee_suiv = 0;

        $(".calc-container input").each(function(){
            
            // SI VIDE OU N'EST PAS UN NOMBRE ALORS ERREUR
            if($(this).val() == "" || Form.verifInt($(this).val()) == false)   erreur = true
            
        })
        
        if (erreur != true){
            
             $("#annee_prec input").each(function(){
               annee_prec += parseInt($(this).val())
             })
             
             $("#annee_suiv input").each(function(){
               annee_suiv += parseInt($(this).val()) 
             })
             
            var montant = (annee_prec - annee_suiv) / 10

            if (montant > 0){// SI MONTANT DANS LE BENEFICE
                msg = "Vous pouvez bénéficier d'une éxonération fiscale de : "+montant+" €"
            }else{
                msg = "Vous ne pouvez bénéficier d'une éxonération fiscale"
            }
            
        }else{
            msg = "Formulaire incorrect"
        }
        
        $('#msgCalculette').text(msg)
    })
    
    
    // TRIE DES VIDEOS PAR DATE
    $(document).on('click','#sort-date',function(event){
        event.preventDefault()
        var elems = $.makeArray($(".video_container"));
        
        // SI DATA ORDER A 0 ALORS RECENT AU PLUS ANCIEN
        if( parseInt($(".video-list").attr('data-order')) == 0){
            elems.sort(function(a, b) {
                return $(a).attr('data-date') < $(b).attr('data-date') ;
            });
            dataOrder= 1
        }else{ // SINON DU ANCIEN AU PLUS RECENT
            elems.sort(function(a, b) {
                return $(a).attr('data-date') > $(b).attr('data-date') ;
            }); 
            dataOrder = 0
        }
    
        $(".video-list").attr('data-order', dataOrder)
        $(".video-list").html(elems);
    })    
    
    
    
    // TRIE DES VIDEOS PAR ORDRE ALPHABETIQUE
    $(document).on('click','#sort-alpha',function(event){
        event.preventDefault()
        var elems = $.makeArray($(".video_container"));
        
        // SI DATA ORDER A 0 ALORS A -> Z 
        if( parseInt($(".video-list").attr('data-order')) == 0){
            elems.sort(function(a, b) {
                 return $(a).find('.title-video').text() > $(b).find('.title-video').text() ;
            });
            dataOrder= 1
        }else{ // SINON Z -> A
            elems.sort(function(a, b) {
                 return $(a).find('.title-video').text() < $(b).find('.title-video').text() ;
            }); 
            dataOrder = 0
        }
        
        $(".video-list").attr('data-order', dataOrder)
        $(".video-list").html(elems);
    })    
    
    
    
    // TRIE DES VIDEOS PAR AUTEUR
    $(document).on('click','#sort-author',function(event){
        event.preventDefault()
        var elems = $.makeArray($(".video_container"));
        
        elems.sort(function(a, b) {
            return $(a).attr('data-author') > $(b).attr('data-author');
        });

        $(".video-list").html(elems);
    })


    //Filtre des fiches conseils par titre
    $('.filterfiche').keyup(function(){
        var search = $(this).val().toUpperCase().trim();
        if(search!=""){
            $(this).parent().parent().parent().children(".row.page").first().children(".fiche-big").hide();
            $(this).parent().parent().parent().children(".row.page").first().children(".fiche-big").filter(function( index ) {
                return $(this).find(".title-fiche").first().text().toUpperCase().trim().indexOf(search) != -1;
            }).show();
        }else{
            $(this).parent().parent().parent().children(".row.page").first().children(".fiche-big").show();
        }
    }) 

    // OUVERTURE MODAL COMPTE RENDU (ESPACE ENTREPRISE)
    $(document).on('click','#openCR',function(){
        var compte_rendu = $(this).parent().attr('data-cr')
        $('div.modalRdv').modal('show')
        $('div.modalRdv #compte_rendu').val(compte_rendu)
    })



})

