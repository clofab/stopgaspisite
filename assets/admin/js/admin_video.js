
$(document).ready(function() {

    /**************************************************************************
     *                              A L'EXECUTION                             *
     *************************************************************************/

        // DATATABLE JS INIT
    $('#tabVideo').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 0, "orderable": false },
            { "targets": 1, "orderable": false },
            { "targets": 5, "orderable": false },
            { "targets": 6, "orderable": false },
        ],
        "language": langage
    });

    var tabVideo = $('#tabVideo').dataTable();

    $("#searchbox").keyup(function() {
        tabVideo.fnFilter(this.value);
    });

    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

    $(document).on('submit','#formVideo',function(event){

        var titreVideo = Form.verifInput($('[name="titreVideo"]'));
        var urlVideo = Form.verifInput($('[name="urlVideo"]'));
        var descriptionVideo = Form.verifInput($('[name="descriptionVideo"]'));

        var tabFormulare = [titreVideo, urlVideo, descriptionVideo];

        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            event.preventDefault();
        }
    })


    /**************************************************************************
     *                            CHARGEMENT VIDEO YOUTUBE                    *
     *************************************************************************/

    $(document).on('change','[name="urlVideo"]',function(){
        $('#areaIframe iframe').remove();

        var url = $(this).val();
        if(url.match(/youtube\.com/)){
            url = url.replace("www.youtube.com/watch?v=","www.youtube.com/embed/");

            $('#areaIframe').append('<iframe class="col-md-6 col-md-offset-3 embed-responsive-item " height="315px" src="'+url+'" frameborder="0" allowfullscreen></iframe>')
        } else {
            alert("Votre lien n'est pas une vidéo youtube")
            $(this).val("")
        }
    })



    /**************************************************************************
     *                             SUPPRESSION                                *
     *************************************************************************/

    //SUPPRIME UNE VIDEO
    $(document).on('click','.deleteVideo',function(){
        url = urlApp+"/Videos/deleteVideo";
        Suppression.delete(url,$(this))
    });

    // SUPPRIME DES ACTUALITES
    $(document).on('click','.deleteAllVideo',function(){
        url = urlApp+"/Videos/deleteVideo";
        Suppression.deleteChecks(".checkVideo",url)
    });



    /**************************************************************************
     *                             PUBLICATION                                *
     *************************************************************************/

        // PUBLICATION - DEPUBLICATION D'UNE ACTU
    $(document).on('click','.publishVideo .checkbox',function(){

        var url = urlApp+"/Videos/publishVideo";
        checkbox = $(this).find('input');

        if(checkbox.is(":checked")){
            checkbox.prop("checked",false)
        }else{
            checkbox.prop("checked",true)
        }

        Publication.publishOne(url,checkbox)

    });

    // PUBLICATION - DEPUBLICATION PLUSIEURS ACTUS
    $(document).on('click','.publishAllVideo',function(){
        var etat = $(this).attr('data-etat');
        var url = urlApp+"/Videos/publishVideo";
        Publication.publishChecks(".checkVideo",url,etat)
    });


     /*************************************************************************
     *                      RÉ-ACTIVATION -- ARCHIVES                         *
     *************************************************************************/

     // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#archiveVideo').on('shown.bs.modal', function (e) {
        var date;
        var libelleTypeEnergie ="";
        var urlVideo;

        $.ajax({
            method: "POST",
            url: urlApp+"/Videos/getArchiveVideo",
            dataType:"json",

            success:function(data) {
                $('#tabArchiveVideo').dataTable().fnClearTable(); // on vide le modal archives

                for (i = 0; i < data.length; i++) {

                    date = data[i].dateCreation.split("-");
                    date = date[2]+"/"+date[1]+"/"+date[0];

                    urlVideo = data[i].urlVideo.split('/')
                    urlVideo = urlVideo[urlVideo.length-1]
                    libelleTypeEnergie ="";
                    // ON RECUPERE TOUS LES TYPES D'ENERGIES PAR Video
                    for (y = 0; y < data[i].typeEnergie.length; y++) {
                        libelleTypeEnergie += "<li>"+data[i].typeEnergie[y].libelleTypeEnergie+"</li>"
                    }

                    var newRow = $('#tabArchiveVideo').dataTable().fnAddData([
                        '<img src="http://img.youtube.com/vi/'+urlVideo+'/0.jpg" alt="" width="80" height="80">',
                        date,
                        data[i].titreVideo,
                        "<ul>"+libelleTypeEnergie+"</ul>",
                        '<button type="button" class="activeVideo btn btn-xs">Activer</button>',
                    ]);

                    var oSettings = $('#tabArchiveVideo').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idVideo); // Ajout attribut sur la ligne crée
                    libelleTypeEnergie ="";
                }
            } 
        })
    });


    // REACTIVATION D'UNE VIDEO
    $(document).on('click','.activeVideo',function(){

        var table = $(this).closest('table').DataTable();
        var tr = $(this).closest('tr');
        var idVideo = parseInt(tr.attr('id'));
        

        // UPDATE EN BDD AFIN DE NE PLUS ETRE ARCHIVER
        $.ajax({
            method: "POST",
            url:urlApp+"/Videos/publishVideo",
            dataType:"json",
            data: { id: idVideo, etat: 1},
            success:function(){
                table.row(tr).remove().draw(false); // delete row archive
               
                
                $.ajax({
                    method: "POST",
                    url: urlApp+"/Videos/getVideo",
                    dataType: "json",
                    data: { idVideo: idVideo},

                    success: function (data) {
                        
                        if (data[0].etatVideo == 1) checked = "checked";
                        date = data[0].dateCreation.split("-");
                        date = date[2]+"/"+date[1]+"/"+date[0]

                        var urlVideo = data[0].urlVideo.split('/')
                        var urlVideo = urlVideo[urlVideo.length-1]
                        
                        libelleTypeEnergie ="";
                        // ON RECUPERE TOUS LES TYPES D'ENERGIES PAR Video
                        for (y = 0; y < data[0].typeEnergie.length; y++) {
                            libelleTypeEnergie += "<li>"+data[0].typeEnergie[y].libelleTypeEnergie+"</li>"
                        }

                        var newRow = $('#tabVideo').dataTable().fnAddData([
                            '<input class="checkVideo" type="checkbox">',
                            '<img src="http://img.youtube.com/vi/'+urlVideo+'/0.jpg" alt="" width="80" height="80">',
                            '<h4>'+data[0].titreVideo+'</h4><br>'+ data[0].descriptionVideo,
                            "<ul>"+libelleTypeEnergie+"</ul>",
                            date,
                            '<div class="checkbox"><input type="checkbox" class="etatPublish" checked><label></label>',

                            '<div class="btn-group" role="group">'+
                            '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"> </span> Action'+
                            '<span class="caret"> </span></button>'+
                            '<ul class="pull-right dropdown-menu">'+
                            '<li><a href="'+urlApp+'/Videos/saisieVideo/'+data[0].idVideo+'">Editer</a></li>'+
                            '<li><a href="#" class="deleteVideo">Archiver</a></li></ul></div>'
                        ]);

                        var oSettings = $('#tabVideo').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $(nTr).attr('id', data[0].idVideo);                      // Ajout attribut sur la ligne crée
                        $('td', nTr)[5].setAttribute( 'class', 'publishVideo' ); // Ajout d'une classe sur le TD de publier
                        libelleTypeEnergie ="";

                    }
                })
            }
        });
    });
});  