
$(document).ready(function(){

    $('#formAccueil').on('submit',function(event){

        var titrePres = Form.verifInput($('[name="titrePres"]'));
        var descriptionPres = Form.verifInput($('[name="descriptionPres"]'));

        // SI FORMULAIRE NON COMPLET ALORS AFFICHE ALERTE
        if(titrePres == false || descriptionPres == false){
            afficheAlert('.pageContaint','danger',"Attention, Formulaire incomplet");
            event.preventDefault();
            return false
        }
    })

})