
// VALIDATION -- EDITION D'UN DROIT
$(document).on('click','.validDroit',function(){
    $('.alert').remove();
    var id = null;
    var tr = $(this).closest('tr');
    var type = parseInt($(this).attr('data-type'))
    var lblTypeUtilisateur = Form.verifInput(tr.find('.lblTypeUtilisateur'));

    if(lblTypeUtilisateur == false) return false;

    // Si on est en modiification, alors on récup l'id
    if(type == 0 ){
        id = tr.attr('id')
    }

    var fiche = tr.find('.checkFiche').prop('checked');
    var actu = tr.find('.checkActu').prop('checked');
    var video = tr.find('.checkVideo').prop('checked');
    var site = tr.find('.checkSite').prop('checked');
    var commercial = tr.find('.checkConseiller').prop('checked');
    var rdv = tr.find('.checkRdv').prop('checked');
    var utilisateur = tr.find('.checkUtilisateur').prop('checked');
    var annexe = tr.find('.checkAnnexe').prop('checked');
    var droit = tr.find('.checkDroit').prop('checked');


    $.ajax({  //function ajax avec verif si meme libelle
        method: "POST",
        url: urlApp+"/Droit/setDroit",
        dataType:"json",
        data: { id:id ,lblTypeUtilisateur:lblTypeUtilisateur ,fiche: fiche, actu: actu,video: video,site: site,commercial: commercial,rdv: rdv,utilisateur: utilisateur,droit: droit,annexe: annexe },
        success:function(data){
            if(parseInt(data) == 0){
                afficheAlert('.pageContaint','danger','Libellé déjà utilisé')
            }else{

                $('.alert').remove();  // suppression des alerts

                if(type == 0 ){ // si modif
                    afficheAlert('.pageContaint','success','Modification effectué avec succes')
                }else{          // sinon creation

                    // Creation nouvelle ligne
                    tr.clone().insertAfter(tr);
                    $('#tabDroit tr:last input').prop('checked',false)
                    $('#tabDroit tr:last input').val('')

                    // modification sur ligne créer
                    tr.attr('id',data)
                    tr.find('td:last').attr('colspan','0')
                    tr.find('td:last button').remove()
                    tr.find('td:last').append('<button type="button" class="btn btn-primary btn-xs col-xs-12 validDroit" data-type="0">Modifier</button>')
                    tr.find('td:last').after('<td><button type="button" class="btn btn-danger btn-xs col-xs-12 deleteDroit" data-type="0">Supprimer</button></td>')



                }
            }
        }
    });


})

// SUPPRESION D'UN DROIT
$(document).on('click','.deleteDroit',function(){
    $('.alert').remove();
    var tr = $(this).closest('tr');
    var id = tr.attr('id');

    //function ajax avec verif si libelle utilisé chez utilisateur
    $.ajax({
        method: "POST",
        url: urlApp + "/Droit/deleteDroit",
        dataType: 'json',
        data: { id: id },
        success: function (data) {

            if(parseInt(data) == 0){
                afficheAlert('.pageContaint','danger','Attention des utilisateurs sont relié a ce groupe, changer leur groupe pour pouvoir supprimer ce groupe')

            }else{
                tr.remove()
                afficheAlert('.pageContaint','success','Suppression bien effectuée')
            }
        }

    })


})