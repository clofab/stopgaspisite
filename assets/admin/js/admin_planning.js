//-- Documentation du DatePicker : https://tarruda.github.io/bootstrap-datetimepicker/ -->

function innitPicker() {

    $('.datePicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'fr-FR',
        pickTime: false
    });


    $('.timePicker').datetimepicker({
        format: 'hh:mm',
        language: 'fr-FR',
        pickDate: false,
        pickSeconds: false,
        startDate: new Date()
    });

}

function setNewHour(el,hour) {
    if (hour != null) {
        var picker = el.data('datetimepicker')
        var hour = hour.split(":")
        picker.setLocalDate(new Date(99,5,24,hour[0],hour[1],00,0))
    }
}

// FUNCTION QUI PERMET LA VERIFICATION DU PLANNING
function verifTime(el){

    var heure = el.val()
    
    if(heure == "") heure = null
    
    if (heure != null){

        var data_time = el.closest('.form-group').attr('data-time');
        var data_period = el.closest('.form-group').attr('data-period');
        var tabHeure = heure.split(":");
        minute = parseInt(tabHeure[1])
        var deb_limite;
        var fin_limite;

        // SI HEURE SAISIE = HEURE LIMITE ON MET LES MINUTES A ZERO
        if((parseInt(tabHeure[0]) == 13 && data_period == "matin" && minute > 0) || (parseInt(tabHeure[0]) == 18 && minute > 0)){
            // heure = tabHeure[0]+":00"+":00"
            // el.val(tabHeure[0]+":00")
            msgError = "Heure non cohérente, l'heure doit être comprise entre 8h à 13h pour le matin et de 13h à 18h pour l'après midi"
            heure = false
        }

        // ON DEFINIT LES HEURES LIMITES DU CALENDAR EN FONCTION DE MATIN OU MIDI
        if(data_period == "matin"){
            deb_limite = 8;
            fin_limite = 13
        }else if(data_period == "midi"){
            deb_limite = 13;
            fin_limite = 18
        }

        // TEST SI HEURE BONNE HEURE PAR RAPPORT A MATIN OU MIDI
        if(parseInt(tabHeure[0]) < deb_limite || parseInt(tabHeure[0]) > fin_limite ){
             msgError = "Heure non cohérente, l'heure doit être comprise entre 8h à 13h pour le matin et de 13h à 18h pour l'après midi"
            heure = false
        }

        // SI HORRAIRE DU MATIN
        if(data_time == "debut"){

            var valFin = el.closest('.form-group').next().find('input').val();
            var heureFin = valFin.split(":");

            // SI LA VALEUR SUIVANTE EST VIDE
            if (valFin == "" ){
                msgError = "Heure suivante vide"
                el.closest('.form-group').next().addClass('has-error')

            // SI VALEUR SUPERIEUR OU EGALE A L'HEURE DE FIN
            }else if(tabHeure[0] > heureFin[0] || (tabHeure[0] == heureFin[0] && tabHeure[1] >= heureFin[1])){
                msgError =  'Heure de fin supérieur ou égale à celle du debut'
                heure = false
            }else{
                el.closest('.form-group').next().removeClass('has-error')
            }

        }else if(data_time == "fin") {// SI HORRAIRE DU MIDI

            var valDebut = el.closest('.form-group').prev().find('input').val();
            var heureDebut = valDebut.split(":");

            // SI LA VALEUR PRECEDANTE EST VIDE
            if (valDebut == "") {
                msgError = "Heure précedente vide ou égale à celle de fin"
                el.closest('.form-group').prev().addClass('has-error')

                // SI VALEUR SUPERIEUR OU EGALE A L'HEURE DE DEBUT
            } else if (tabHeure[0] < heureDebut[0] || (tabHeure[0] == heureDebut[0] && tabHeure[1] <= heureDebut[1])) {
                msgError = 'Heure de debut supérieur ou égale à celle de fin'
                heure = false
            } else {
                el.closest('.form-group').prev().removeClass('has-error')
            }
        }

        // SI IL Y A UNE ERREUR
        if(heure == false){
            el.closest('.form-group').addClass('has-error')
            afficheAlert('div.planning .modal-body','danger',msgError)
        }else{
            $('.alert').remove()
            el.closest('.form-group').removeClass('has-error')
        }
    }

    return heure
}


function remplissageTab(numTab,data,i,btn){
    
    var newRow = $('.tabRdv:eq('+numTab+')').dataTable().fnAddData([
        data.rdv[i].nomUtilisateur,
        data.rdv[i].interlocuteur,
        data.rdv[i].nomEntreprise,
        data.rdv[i].emailEntreprise,
        dateRdv[2]+"/"+dateRdv[1]+"/"+dateRdv[0]+" "+heureRdv,
        btn

    ]);
    
    var oSettings = $('.tabRdv:eq('+numTab+')').dataTable().fnSettings();
    var nTr = oSettings.aoData[newRow[0]].nTr;
    $(nTr).attr('id', data.rdv[i].idRdv);    
    
}

$(document).ready(function(){

    var idConseillerAgenda

    innitPicker();

    // DATATABLE JS INIT
    $('#tabConseiller').dataTable( {
        "info":     false,
        "bSort": false,
        "language": langage,
        "LengthChange" : false,
        "bLengthChange": false,
    });

    $('#tabIndispo').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 5, "orderable": false },

        ],
        "language": langage
    });
    
    // DATATABLE JS INIT
    $('.tabRdv').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 5, "orderable": false },
        ],
        "language": langage
    });

    var tabConseiller = $('#tabConseiller').dataTable();
    $("#searchbox").keyup(function() {
        tabConseiller.fnFilter(this.value);
    });
    
    var tabRdv = $('#tabRdv').dataTable();
    $("#searchRdv").keyup(function() {
        tabRdv.fnFilter(this.value);
    });

    /**************************************************************************
     *                       SELECT   --    DROPBOX                           *
     *************************************************************************/

    //  CHANGEMENT SELECT ANNEXE
    $(document).on('change','#selectAnnexe',function(){
        var idAnnexe = $(this).val();
        var optionSelect = "";

        $.ajax({
            method: "POST",
            url: urlApp + "/Planning/changeAnnexe",
            dataType: 'json',
            data: {idAnnexe: idAnnexe},
            success: function (data) {
                console.log(data)
                
                //******    ON CHANGE LA LISTES DES CONSEILLERS     ******/
                
                $('#tabConseiller').dataTable().fnClearTable(); // on vide le tab Conseiller

                for (i = 0; i < data.annexe.length ; i++ ){

                    var newRow = $('#tabConseiller').dataTable().fnAddData([
                        '<img src="' + urlAssets + '/files/images/utilisateur/' + data.annexe[i].imgUtilisateur + '" height="50" width="50" />',
                        data.annexe[i].nomUtilisateur+' '+data.annexe[i].prenomUtilisateur+'<br>'+data.annexe[i].nomAnnexe,

                        '<div class="btn-group" role="group">'+
                        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                        '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                        '<span class="caret"></span></button>'+
                        '<ul class="pull-right dropdown-menu">'+
                        '<li><a href="#" class="openPlanning">Agenda</a></li>'+
                        '<li><a href="#" class="openIndispo">Indisponibilités</a></li></ul></div>'
                    ]);

                    var oSettings = $('#tabConseiller').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data.annexe[i].idUtilisateur);                      // Ajout attribut sur la ligne crée
                    $(nTr).attr('data-annexe', data.annexe[i].idAnnexe);                  // Ajout attribut annexe

                }
                
                
                 //******    ON CHANGE LA LISTES DES RDV     ******/
                $('.tabRdv:eq(0)').dataTable().fnClearTable(); // on vide le tab Prochain rdv
                $('.tabRdv:eq(1)').dataTable().fnClearTable(); // on vide le tab Finaliser rdv
                $('.tabRdv:eq(2)').dataTable().fnClearTable(); // on vide le tab Rdv Archive
                
                for (i = 0; i < data.rdv.length ; i++ ){
                    
                    dateFormat = data.rdv[i].plageHorraire

                    dateRdv  = dateFormat.substring(0, 10);
                    dateRdv  = dateRdv.split("-");
                    heureRdv = data.rdv[i].heure_deb.substring(0, 5);
                    
                    dateFormat = new Date(dateFormat)
                    dateNow = new Date()
                    etatRdv = parseInt(data.rdv[i].etatRdv)
                    
                    
                    // SI PROCHAIN RDV
                    if(etatRdv == 1  || (etatRdv == 2 && dateFormat > dateNow)){

                      switch (etatRdv) {
                         
                        case 1: 
                             btn ='<button type="button" class="btn btn-primary btn-xs confirmRdv">Confirmer RDV</button> <button type="button" class="col-md-offset-1 btn btn-danger btn-xs refusRdv">Refuser RDV</button>';
                            break;
                        case 2: 
                             btn = '<button type="button" class="btn btn-danger btn-xs annulRdv">Annuler RDV</button>';
                            break;
                        }
                        
                        remplissageTab(0,data,i,btn)
                    }
                    
                    
                    // SI RDV EN ATTENTE
                    if(etatRdv == 2 && dateFormat < dateNow){
                        
                        btn = '<button type="button" class="btn btn-primary btn-xs saisirCompteRendu">Remplir compte rendu</button>';
                        remplissageTab(1,data,i,btn)
                    }           
                    
                    // SI RDV ARCHIVEÉ
                    if(etatRdv == 0){
                        
                        btn = '<button type="button" class="col-md-12 btn btn-primary btn-xs consultCR">Voir compte rendu </button>';
                        remplissageTab(2,data,i,btn)
                    }
                    
                }
            }
        })
    })


    /**************************************************************************
     *                              PLANNING                                  *
     *************************************************************************/


    //  OUVERTURE MODAL HORRAIRE
    $(document).on('click','.openPlanning',function(){
        var idConseiller = $(this).closest('tr').attr('id')
        var espaceConseiller = false;

        idConseillerAgenda = idConseiller // var global pour modification

        if($('#data-espace-cons').length > 0) espaceConseiller = true

        // SI CONSEILLER BIEN SELECTIONNER, ALORS ON PEUT OUVRIR LE MODAL ET AFFICHER SES HORRAIRES
        // OU SI ESPACE CONSEILLER
        if((idConseiller != null && parseInt(idConseiller) != 0) || espaceConseiller == true){

            $('div.planning').modal('show')

            $.ajax({
                method: "POST",
                url: urlApp + "/Planning/getHorraires",
                dataType: 'json',
                data: {idConseiller: idConseiller},
                success: function (data) {

                    // Si il y'a des resultats on les affiches les horraires dans le tableau
                    if (data.length > 0) {

                        //*************************     REMPLISSAGE DU FORMULAIRE          ***********////

                        //  LUNDI -----
                        setNewHour($('div#rowLundi .timePicker:eq(0)'),data[0].lundi_matin_deb)
                        setNewHour($('div#rowLundi .timePicker:eq(1)'),data[0].lundi_matin_fin)
                        setNewHour($('div#rowLundi .timePicker:eq(2)'),data[0].lundi_midi_deb)
                        setNewHour($('div#rowLundi .timePicker:eq(3)'),data[0].lundi_midi_fin)

                        //  MARDI -----
                        setNewHour($('div#rowMardi .timePicker:eq(0)'),data[0].mardi_matin_deb)
                        setNewHour($('div#rowMardi .timePicker:eq(1)'),data[0].mardi_matin_fin)
                        setNewHour($('div#rowMardi .timePicker:eq(2)'),data[0].mardi_midi_deb)
                        setNewHour($('div#rowMardi .timePicker:eq(3)'),data[0].mardi_midi_fin)

                        //  MERCREDI -----
                        setNewHour($('div#rowMercredi .timePicker:eq(0)'),data[0].mercredi_matin_deb)
                        setNewHour($('div#rowMercredi .timePicker:eq(1)'),data[0].mercredi_matin_fin)
                        setNewHour($('div#rowMercredi .timePicker:eq(2)'),data[0].mercredi_midi_deb)
                        setNewHour($('div#rowMercredi .timePicker:eq(3)'),data[0].mercredi_midi_fin)

                        //  MERCREDI -----
                        setNewHour($('div#rowMercredi .timePicker:eq(0)'),data[0].mercredi_matin_deb)
                        setNewHour($('div#rowMercredi .timePicker:eq(1)'),data[0].mercredi_matin_fin)
                        setNewHour($('div#rowMercredi .timePicker:eq(2)'),data[0].mercredi_midi_deb)
                        setNewHour($('div#rowMercredi .timePicker:eq(3)'),data[0].mercredi_midi_fin)

                        //  JEUDI -----
                        setNewHour($('div#rowJeudi .timePicker:eq(0)'),data[0].jeudi_matin_deb)
                        setNewHour($('div#rowJeudi .timePicker:eq(1)'),data[0].jeudi_matin_fin)
                        setNewHour($('div#rowJeudi .timePicker:eq(2)'),data[0].jeudi_midi_deb)
                        setNewHour($('div#rowJeudi .timePicker:eq(3)'),data[0].jeudi_midi_fin)

                        //  VENDREDI -----
                        setNewHour($('div#rowVendredi .timePicker:eq(0)'),data[0].vendredi_matin_deb)
                        setNewHour($('div#rowVendredi .timePicker:eq(1)'),data[0].vendredi_matin_fin)
                        setNewHour($('div#rowVendredi .timePicker:eq(2)'),data[0].vendredi_midi_deb)
                        setNewHour($('div#rowVendredi .timePicker:eq(3)'),data[0].vendredi_midi_fin)


                    }else{ // Sinon pas de données
                        afficheAlert('div.planning .modal-body','danger',"Aucune donnée pour cette agenda")
                    }
                }
            })

        }else{ //   SINON SI PAS DE CONSEILLER ALORS AFFICHE ERREUR
            $('#selectConseiller').closest('.form-group').addClass('has-error')
        }

    })

    //  VALIDATION DU PLANNING CONSEILLER
    $(document).on('click','#validPlanning',function(){
        var idConseiller = idConseillerAgenda
        var lundi =[]; var mardi =[]; var mercredi =[]; var jeudi =[]; var vendredi =[]; var samedi =[];
        var jourVerif;

        for (i = 0; i < 4; i++){
            // POUR CHAQUE JOUR ON VERIFIE QUE LES DATES SOIENT BIEN CONFORME

            jourVerif = verifTime($('#rowLundi input:eq('+i+')'))

            if(jourVerif != false){   lundi.push(jourVerif)       }else{   return false    }

            jourVerif = verifTime($('#rowMardi input:eq('+i+')'))
            if(jourVerif != false){   mardi.push(jourVerif)       }else{   return false    }

            jourVerif = verifTime($('#rowMercredi input:eq('+i+')'))
            if(jourVerif != false){   mercredi.push(jourVerif)    }else{   return false    }

            jourVerif = verifTime($('#rowJeudi input:eq('+i+')'))
            if(jourVerif != false){   jeudi.push(jourVerif)       }else{   return false    }

            jourVerif = verifTime($('#rowVendredi input:eq('+i+')'))
            if(jourVerif != false){   vendredi.push(jourVerif)    }else{   return false    }

        }

        lundi = JSON.stringify(lundi); mardi = JSON.stringify(mardi) ; mercredi = JSON.stringify(mercredi);
        jeudi = JSON.stringify(jeudi); vendredi = JSON.stringify(vendredi); samedi = JSON.stringify(samedi);

        $.ajax({
            method: "POST",
            url: urlApp + "/Planning/savePlanning",
            data: {lundi: lundi,mardi: mardi,mercredi: mercredi,jeudi: jeudi,vendredi: vendredi,samedi: samedi,idConseiller : idConseiller},
            success: function () {
                $('div.planning').modal('hide')
            }
        })

    });

    //  FERMETURE DU MODAL : PLANNING --> input à vide
    $('.planning').on('hidden.bs.modal', function () {
        $('.planning input').val('')
        $('.planning .alert').remove()
        $('.has-error').removeClass('has-error')
    })


    /**************************************************************************
     *                            INDISPONIBILITE                             *
     *************************************************************************/

    //  OUVERTURE MODAL INDISPONIBILITE
    $(document).on('click','.openIndispo',function() {
        $('div.indisponibilite').modal('show');

        var idAnnexe = false;
        var idConseiller = false;
        var annexe;
        var conseiller;
        var trIndispo = "";
        var heureDebutIndispo;
        var heureFinIndispo;
        var dateDebutIndispo;
        var dateFinIndispo;

        var espaceConseiller = false;
        // SI CONSEILLER SUR SON ESPACE
        if ($('#data-espace-cons').length > 0) espaceConseiller = true

        // SI ADMIN
        if (espaceConseiller == false){

            // SI INDISPO SELON ANNEXE
                if ($(this).attr("data-all") == "true") {
                    idAnnexe = parseInt($("#selectAnnexe option:selected").val());
                    idConseiller = 0;
                    annexe = $("#selectAnnexe option:selected").text();
                    conseiller = "Tous les Conseillers";
                } else {
                    //SI INDISPO SELON CONSEILLER
                    idAnnexe = parseInt($(this).closest('tr').attr('data-annexe'));
                    idConseiller = parseInt($(this).closest('tr').attr('id'))
                    annexe = $(this).closest('tr').find('td:eq(2)').text()
                    conseiller = $(this).closest('tr').find('td:eq(1)').text()
                }

            if (idAnnexe != 0) {
                $("#indispoConseiller").text("Conseiller : " + conseiller)
            } else {
                annexe = "Toutes les Annexes";
                $("#indispoConseiller").text("Conseiller : Tous les Conseillers")
            }


            $("#indispoAnnexe").text("Annexe : "+annexe)
            $("#indispoAnnexe").attr('data-annexe',idAnnexe)
            $("#indispoConseiller").attr('data-conseiller',idConseiller)
        }


        $.ajax({
            method: "POST",
            url: urlApp + "/Planning/getIndispo",
            dataType: 'json',
            data: {idConseiller: idConseiller,idAnnexe :idAnnexe},
            success: function (data) {

                $('#tabIndispo').dataTable().fnClearTable(); // on vide le tab indispo
                // Si il y'a des resultats on les affiches le tableau
                if (data.length > 0) {

                    $('#indispoListe').removeClass('hide')

                    for (i = 0; i < data.length ; i++ ){

                        dateDebutIndispo = data[i].dateDebutIndispo.split("-")
                        dateFinIndispo = data[i].dateFinIndispo.split("-")

                        if(data[i].heureDebutIndispo == null){
                            heureDebutIndispo =  ""
                        }else{
                            heureDebutIndispo = data[i].heureDebutIndispo.substring(0,5)
                        }
                        if(data[i].heureFinIndispo == null){
                            heureFinIndispo   =  ""
                        }else{
                            heureFinIndispo = data[i].heureFinIndispo.substring(0,5)
                        }

                        // AJOUT NOUVELLE LIGNE
                        newRow = $('#tabIndispo').dataTable().fnAddData( [
                            data[i].intituleIndispo,
                            dateDebutIndispo[2]+"/"+dateDebutIndispo[1]+"/"+dateDebutIndispo[0],
                            dateFinIndispo[2]+"/"+dateFinIndispo[1]+"/"+dateFinIndispo[0],
                            heureDebutIndispo,
                            heureFinIndispo,
                            // SI ID CONSEILLER != 0 et IdAnnexe == 0 et si droit gestion des agendas alors on fait apparaitre le bouton supprimé
                            "<button type='button' class='btn btn-danger btn-xs deleteIndispo'>Supprimer</button>"
                        ] );

                        // Ajout atribut sur la ligne crée
                        var oSettings =  $('#tabIndispo').dataTable().fnSettings();
                        var nTr = oSettings.aoData[ newRow[0] ].nTr;
                        $(nTr).attr('id', data[i].idIndispo);
                    }
                }else{
                    $('#indispoListe').addClass('hide')
                }
            }
        })

    })

    //  CHECK / DECHECK "TOUTE LA JOURNÉE"
    $(document).on('change',"#heureMax",function(){

        if($(this).is(':checked')){

            $('#heureDebut').prop('disabled',true)
            $('#heureFin').prop('disabled',true)
            $('#heureDebut').val('')
            $('#heureFin').val('')
        }else{

            $('#heureDebut').prop('disabled',false)
            $('#heureFin').prop('disabled',false)
        }
    })

    // VALIDATION FORMULAIRE INDISPONIBILITE
    $(document).on('click','#validIndispo',function(){

        $('.alert').remove()
        var intitule = Form.verifInput($("#intitule"));
        var dateDebut = Form.verifInput($("#jourDebut"));
        var dateFin = Form.verifInput($("#jourFin"));
        var heureDebut = "07:00"
        var heureFin = "18:00"
        var heureMax = false
        var idAnnexe = $("#indispoAnnexe").attr('data-annexe')
        var idConseiller = $("#indispoConseiller").attr('data-conseiller')


        // SI "TOUTE LA JOURNEE" EST COCHER, ALORS ON NE PREND PAS EN COMPTE LES HEURES
        if($('#heureMax').is(':checked')){
            heureMax = true
            $("#heureDebut").closest('.form-group').removeClass('has-error')
            $("#heureFin").closest('.form-group').removeClass('has-error')
        }else{
            // SINON ON RECUPERE LES HEURES DE DEBUT ET DE FIN
            heureDebut = Form.verifInput($("#heureDebut"));
            heureFin = Form.verifInput($("#heureFin"));
        }

        var tabFormulare = [intitule, dateDebut, dateFin, heureDebut, heureFin];

        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            afficheAlert('div.indisponibilite .modal-body','danger',"Formulaire incomplet")
            return false
        }

        var dateMin = new Date(dateDebut+" "+heureDebut+':00')
        var dateMax = new Date(dateFin+" "+heureFin+':00')

        // SI DATE DE FIN BIEN SUPERIEUR A DATE DE DEBUT
        if(dateMin >= dateMax){
            afficheAlert('div.indisponibilite .modal-body','danger',"Date de fin supérieur ou égale à celle du debut")
            return false
        }

        $.ajax({
            method: "POST",
            url: urlApp + "/Planning/saveIndispo",
            data: {intitule: intitule,dateDebut: dateDebut,dateFin: dateFin,heureDebut: heureDebut,heureFin: heureFin,heureMax: heureMax,idAnnexe :idAnnexe, idConseiller:idConseiller},
            success: function (data) {
                
                data = parseInt(data)
                
                // SI RENDEZ VOUS PREVU A LA DATE D'UN RENDEZ VOUS
                if (data == 0){
                    afficheAlert('div.indisponibilite .modal-body','danger',"<strong> Impossible !</strong> Un ou des rendez vous sont prévu à cette date, supprimez le(s) puis recommencez ")
                }else{
                    
                    $('#indispoListe').removeClass('hide')
                    
                    // AJOUT NOUVELLE LIGNE
                    newRow = $('#tabIndispo').dataTable().fnAddData( [
                        intitule,
                        dateDebut,
                        dateFin,
                        heureDebut,
                        heureFin,
                        "<button type='button' class='btn btn-danger btn-xs deleteIndispo'>Supprimer</button>"
                    ] );

                    // Ajout atribut sur la ligne crée
                    var oSettings = $('#tabIndispo').dataTable().fnSettings();
                    var nTr = oSettings.aoData[ newRow[0] ].nTr;
                    $(nTr).attr('id', data);
                        
                        
                    //$('div.indisponibilite ').modal('hide')
                }
                
            }
        })

    })

    //  FERMETURE DU MODAL : INDISPONIBILITE --> input à vide
    $('.indisponibilite').on('hidden.bs.modal', function () {

        $('.indisponibilite input').val('')
        $('.indisponibilite .alert').remove()
        $('.has-error').removeClass('has-error')
        $('.indisponibilite input').prop('checked',false)
        $('.indisponibilite input').prop('disabled',false)
    })

    //  SUPRESSION D'UNE INDISPONIBILITE
    $(document).on('click','.deleteIndispo',function(){
        url = urlApp+"/Planning/deleteIndispo";
        Publication.publishOne(url,$(this),true)
        
    })
    
    
    
    
    /**************************************************************************
     *                      GESTION DES RENDEZ VOUS                           *
     *************************************************************************/
    
    
    // etat 0 : Fini , annulé , refuser
    // etat 1 : en attente d'acceptation
    // etat 2 : accepté, en attente du rdv
    
 
    // CONFIRMATION D'UN RENDEZ VOUS
    $(document).on('click','.confirmRdv', function(){
        
        var idRdv = $(this).closest('tr').attr('id')
        prevBtn = $(this)
        
        $.ajax({
            method: "POST",
            url: urlApp + "/Planning/changeEtatRdv",
            data: {idRdv: idRdv, etatRdv : 1 },
            success: function () {
                
                prevBtn.next().remove() // SUPPRESION DU BTN REFUSER
                btn = $('<button type="button" class="col-md-7 btn btn-danger btn-xs annulRdv">Annuler RDV</button>')
                $(prevBtn).replaceWith(btn); // ON REMPLACE LE BTN PAR LE BTN "ANNULER RDV"
                
                newNbNotif = parseInt($('#notifRdv').text()-1)
                
                if(newNbNotif != 0){
                    $('#notifRdv').text(newNbNotif)
                }else{
                    $('#notifRdv').remove()
                }
             
            }
        })
        
    })
    
    
    var idRdvConcerner; 
    var typeModal;
    
    function openModalRdv(that,titre,titreCR,btnClose, type){
        
        $('div.modalRdv').modal('show')
        $('#validCompteRendu').removeClass("hide")
        $('div.modalRdv #compte_rendu').val("")
        
        idRdvConcerner = that.closest('tr').attr('id')
        
        $('#titleRdv').text(titre)
        $('#titleCompteRendu').text(titreCR)
        $('#annulModalRdv').text(btnClose)
        
        typeModal = type
    }
    
    // REFUS D'UN RENDEZ VOUS
    $(document).on('click','.refusRdv', function(){
        openModalRdv($(this),"Étes vous sûre de vouloir refuser ce rendez vous ?","Cause du refus :","Annuler","refus")
    }) 
            
    // ANNULATION D'UN RENDEZ VOUS
    $(document).on('click','.annulRdv', function(){
        openModalRdv($(this),"Étes vous sûre de vouloir annuler ce rendez vous ?","Cause de l'annulation :","Annuler","annule")
    }) 
    
    // SAISIE DU COMPTE RENDU
    $(document).on('click','.saisirCompteRendu', function(){
        
        openModalRdv($(this),"Saisie du compte rendu","Compte Rendu :","Annuler","saisieCR")
    
    })
    
    // CONSULTATION DU COMPTE RENDU
    $(document).on('click','.consultCR',function(){
        openModalRdv($(this),"Rendez vous archivé","Compte Rendu :","Fermer", null)
        
        var compte_rendu = $(this).closest('td').attr('data-cr')
        $('div.modalRdv #compte_rendu').val(compte_rendu)
        $('#validCompteRendu').addClass("hide")
    })
    

    
    // REMPLISSAGE COMPTE RENDU
    $(document).on('click','#validCompteRendu',function(){

        var compte_rendu = Form.verifInput($("#compte_rendu"));
        
        var tr = $("tr#"+idRdvConcerner)
        var tableRdv = tr.closest('table')
        var thisBtn = $(this)

        // SI COMPTE RENDU BIEN REMPLIT
        if (compte_rendu != false){
            $.ajax({
                method: "POST",
                url: urlApp + "/Planning/changeEtatRdv",
                data: {idRdv: idRdvConcerner, etatRdv : 2 ,compte_rendu : compte_rendu},
                success: function () {
                    
                    $('div.modalRdv').modal('hide')
                    
                    var newRow = $('.tabRdv:eq(2)').dataTable().fnAddData([
                        tr.find('td:eq(0)').text(),
                        tr.find('td:eq(1)').text(),
                        tr.find('td:eq(2)').text(),
                        tr.find('td:eq(3)').text(),
                        tr.find('td:eq(4)').text(),
                        '<button type="button" class="col-md-12 btn btn-primary btn-xs consultCR">Voir compte rendu </button>',
                    ]);

                    var oSettings = $('.tabRdv:eq(2)').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id',idRdvConcerner);
                    $('td', nTr)[5].setAttribute('data-cr',compte_rendu ); // Ajout compte rendu dans attribut
                    
                    //  PUIS ON SUPPRIME LE RDV
                    tableRdv = tableRdv.DataTable()
                    tableRdv.row(tr).remove().draw(false);
                    
                    // SI ON REFUSE, ON DECREMENTE LE NB DE NOTIFICATION
                    if(typeModal == "refus"){
                        
                        newNbNotif = parseInt($('#notifRdv').text()-1)
                    
                        if(newNbNotif != 0){
                            $('#notifRdv').text(newNbNotif)
                        }else{
                            $('#notifRdv').remove()
                        }
                    }
                }
            })
        }
                
    })
    

});