 var socket = io.connect('https://stopgaspi-webrtc-server-clofab.c9users.io/'),
    session, username, callingTo, duplicateMessages = [];
    socket.emit('demandeListeAppels');
	socket.on('listeAppels',function(data){
	    $("#appelsVisio").html("");
	    if(data.length > 0){
	        $("#visioLink").css("color","green");
	        $("#visioLink").css("font-weight","bold");
	        $("#visioLink i.fa-phone").show();
	        clignoter( $("#visioLink i.fa-phone"));
	        $("#appelsVisio").append("<ul class='col-md-6 col-lg-6'></ul>");
	        for (i = 0; i < data.length; ++i) {
                $("#appelsVisio ul").append("<li>"+data[i].split("-")[1].replace("_"," ")+"<button type='button' class='btn btn-primary pull-right' data-room='"+data[i]+"'>Démarrer la conversation</button></li>");
            }
	    }else{
	       $("#appelsVisio").append("<p>Il n'y pas d'appels disponibles.</p>");
	       $("#visioLink").css("color","#435966");
	       $("#visioLink").css("font-weight","normal");
	       $("#visioLink i.fa-phone").hide();
	       $("#visioLink i.fa-phone").stop();
	    }
	})
function clignoter(elt){
     elt.fadeToggle(700, function () {
        clignoter(elt);
    });
}
var roomName,callingTo;
// QUAND ON CHOSIT UN APPEL
$("#appelsVisio").on('click','button',function(){
    roomName = $(this).attr('data-room');
    callingTo = roomName.split("-")[1];
    $("#nomVisio").text(callingTo.replace("_"," "));
    $("#listeVisio").hide();
    $("#cadreVisio").show();
    socket.emit('join',roomName, username);
    socket.emit('login', username);
    online = false;
    socket.on('login_error', function(message){
        console.log('login_error');
        afficheAlert('#errorVisio', 'danger', "Erreur de connexion");
        socket.emit('camnotfound', callingTo);
        //window.location.reload(false); 
    });
    socket.on('login_successful', function(users){
        console.log('login_successful');
        calle(true);
        socket.emit('sendMessage', callingTo, {type: 'call'});
    });
    socket.on('messageReceived', onVideoMessageReceived);
    socket.on('offline', function(name){
        if(name === callingTo){
            socket.emit('confirmationDestroy',roomName);
            $("#listeVisio").show();
            $("#cadreVisio").hide();
            session.close();
            window.location.reload(false); 
        }
    });
    function calle(isInitiator){
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://api.xirsys.com/ice",
            data: {
                    ident: "stopgaspi",
                    secret: "2d2cba28-4361-11e6-b62f-f8cffe0906a6",
                    domain: "stopgaspi-clofab.c9users.io",
                    application: "stopgaspi",
                    room: "stopgaspi",
                    secure: 1
                },
            success: function (data, status) {
                console.log("call started")
                startCall(data.d.iceServers[2], isInitiator);
            },
            async: false
        });
    }
    function startCall(data, isInitiator){
        var config = {
            isInitiator: isInitiator,
            turn: {
                host: data.url,
                username: data.username,
                password: data.credential
            },
            streams: {
                audio: true,
                video: true
            }
        }
        session = Session("using",config, function (data) {
            socket.emit('sendMessage', callingTo, { 
              type: 'phonertc_handshake',
              data: JSON.stringify(data)
            });
        });
        try {
            setVideoView([{
                container: document.getElementById('remoteVideo'),
                containerParams: {
                    position: [0, 50],
                    size: [300, 300]
                },
                local: {
                    position: [0, 50],
                    size: [200, 200]
                }
            }]);
        } catch (e) {
            afficheAlert('#errorVisio', 'danger', "Aucune caméra n'a été détectée par le navigateur.");
            socket.emit('camnotfound', callingTo);
        }
        
        $("#localVideo").css("position","static");
        $("#localVideo").css("width","100%");
        $("#localVideo").css("height","auto");
        socket.on('sendMessage', function (data) {
            socket.emit('sendMessage', callingTo, { 
              type: 'phonertc_handshake',
              data: JSON.stringify(data)
            });
        });
        
        socket.on('answer', function () {
            console.log('answered');
        });
        socket.on('disconnect', function () {
            session.close();
            socket.emit('sendMessage', callingTo, { type: 'ignore' });
            $("#listeVisio").show();
            $("#cadreVisio").hide();
            window.location.reload(false); 
                
        });
        session.call();
    }
    function onVideoMessageReceived(name, message){
        switch (message.type){
            case 'call':
                callingTo = name;
                break;
            case 'answer':
                console.log(username + ' he answered');
                calle(true);
                break;
            case 'phonertc_handshake':
                if (duplicateMessages.indexOf(message.data) === -1) {
                    session.receiveMessage(JSON.parse(message.data));
                    duplicateMessages.push(message.data);
                }
                if($("#remoteVideo video").css("position") != "static"){
                    $("#remoteVideo video").css("position","static");
                    $("#remoteVideo video").css("width","100%");
                    $("#remoteVideo video").css("height","auto");
                    $("#remoteVideo video").css("top","");
                    $("#remoteVideo video").css("left","");
                    $("#remoteVideo video").css("transform","");
                }
               
                break;
            case 'ignore':
                /*need to add session close for other cases as well*/
                $("#listeVisio").show();
                $("#cadreVisio").hide();
                session.close();
                window.location.reload(false); 
                break;
        }
    }
    $(document).ready(function(){
        
         $('#hangupButton').on('click', function(){
            socket.emit('sendMessage', callingTo, { type: 'ignore' });
            socket.emit('destroy', roomName);
            if(session){
                session.close();
            }
            window.location.reload(false); 
            $("#listeVisio").show();
            $("#cadreVisio").hide();
        });
    });
})