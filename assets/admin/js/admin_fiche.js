$(document).ready(function(){

    /**************************************************************************
     *                              A L'EXECUTION                             *
     *************************************************************************/

    // DATATABLE JS INIT
    $('#tabFiche').dataTable( {
        "info":     false,
        "columnDefs": [ 
        { "targets": 0, "orderable": false }, 
        { "targets": 5, "orderable": false }
        ],
        "language": langage
    });

    // DATATABLE ARCHIV
    $('#tabArchiveFiche').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 0, "orderable": false },
            { "targets": 2, "orderable": false },
        ],
        "language": langage
    });

    var tabFiche = $('#tabFiche').dataTable();
    $("#searchbox").keyup(function() {
        tabFiche.fnFilter(this.value);
    });

    var tabArchiveFiche = $('#tabArchiveFiche').dataTable();
    $("#searchArchiveFiche").keyup(function() {
        tabArchiveFiche.fnFilter(this.value);
    });


    
    /**************************************************************************
     *                             SUPPRESSION                                *
     *************************************************************************/

    //SUPPRIME UNE FICHE
    $(document).on('click','.deleteFiche',function(){
        url = urlApp+"/Fiche/deleteFiche";
        Suppression.delete(url,$(this))

    });

    // SUPPRIME DES FICHES
    $(document).on('click','.deleteAllFiche',function(){
        url = urlApp+"/Fiche/deleteFiche";
        Suppression.deleteChecks(".checkFiche",url)
    });

    /**************************************************************************
     *                             PUBLICATION                                *
     *************************************************************************/

    // PUBLICATION - DEPUBLICATION D'UNE ACTU
    $(document).on('click','.publishFiche .checkbox',function(){
        var url = urlApp+"/Fiche/publishFiche";
        checkbox = $(this).find('input');

        if(checkbox.is(":checked")){
            checkbox.prop("checked",false)
        }else{
            checkbox.prop("checked",true)
        }

        Publication.publishOne(url,checkbox)
    });

    // PUBLICATION - DEPUBLICATION PLUSIEURS FICHES
    $(document).on('click','.publishAllFiche',function(){
        var etat = $(this).attr('data-etat');
        var url = urlApp+"/Fiche/publishFiche";
        Publication.publishChecks(".checkFiche",url,etat)
    });
    
    
    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

    // VERIF FORMULAIRE AJOUT FICHE
    $('#formFiche').on('submit',function(event){

        var titreFiche = Form.verifInput($('[name="titreFiche"]'));
        var descriptionFiche = Form.verifInput($('[name="descriptionFiche"]'));

        // SI FORMULAIRE NON COMPLET ALORS AFFICHE ALERTE
        if(titreFiche == false || descriptionFiche == false){
            afficheAlert('.pageContaint','danger',"Attention, Formulaire incomplet");
            event.preventDefault();
            return false
        }
    });


    /**************************************************************************
     *                      RÉ-ACTIVATION -- ARCHIVES                         *
     *************************************************************************/
    
    // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#archivFiche').on('shown.bs.modal', function (e) {
        var date;
       

        $.ajax({
            method: "POST",
            url: urlApp+"/Fiche/getArchiveFiche",
            dataType:"json",

            success:function(data) {
                $('#tabArchiveFiche').dataTable().fnClearTable(); // on vide le modal archives
                var libelleTypeEnergie ="";
                for (i = 0; i < data.length; i++) {

                    date = data[i].dateCreation.split("-");
                    date = date[2]+"/"+date[1]+"/"+date[0];

                    // ON RECUPERE TOUS LES TYPES D'ENERGIES PAR FICHE
                    for (y = 0; y < data[i].typeEnergie.length; y++) {
                        libelleTypeEnergie += "<li>"+data[i].typeEnergie[y].libelleTypeEnergie+"</li>"
                    }

                    var newRow = $('#tabArchiveFiche').dataTable().fnAddData([
                        date,
                        data[i].titre,
                        "<ul>"+libelleTypeEnergie+"</ul>",
                        '<button type="button" class="activFiche btn btn-xs">Activer</button>',
                    ]);

                    var oSettings = $('#tabArchiveFiche').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idFiche); // Ajout attribut sur la ligne crée
                    libelleTypeEnergie = "";
                }
            }
        })
    });
    
    
    
    // LORQUE L'ON RE-ACTIVE UNE FICHE ARCHIVÉ
    $(document).on('click','.activFiche',function(){
        
        var table = $(this).closest('table').DataTable();
        var tr = $(this).closest('tr');
        var idFiche = parseInt(tr.attr('id'));
        var libelleTypeEnergie ="";

        // UPDATE EN BDD AFIN DE NE PLUS ETRE ARCHIVER
        $.ajax({
            method: "POST",
            url:urlApp+"/Fiche/publishFiche",
            dataType:"json",
            data: { id: idFiche, etat: 1},
            success:function(){
                table.row(tr).remove().draw(false); // delete row archive

                $.ajax({
                    method: "POST",
                    url: urlApp+"/Fiche/getFiche",
                    dataType: "json",
                    data: { idFiche: idFiche},

                    success: function (data) {

                        if (data[0].etatFiche == 1) checked = "checked";
                        date = data[0].dateCreation.split("-");
                        date = date[2]+"/"+date[1]+"/"+date[0]

                        // ON RECUPERE TOUS LES TYPES D'ENERGIES PAR FICHE
                        for (y = 0; y < data[0].typeEnergie.length; y++) {
                            libelleTypeEnergie += "<li>"+data[0].typeEnergie[y].libelleTypeEnergie+"</li>"
                        }

                        var newRow = $('#tabFiche').dataTable().fnAddData([
                            '<input class="checkFiche" type="checkbox">',
                            data[0].titre,
                            "<ul>"+libelleTypeEnergie+"</ul>",
                            date,
                            '<div class="checkbox"><input type="checkbox" class="etatPublish" checked><label></label>',

                            '<div class="btn-group" role="group">'+
                            '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                            '<span class="caret"></span></button>'+
                            '<ul class="pull-right dropdown-menu">'+
                            '<li><a href="'+urlApp+'/Fiche/saisieFiche/'+data[0].idFiche+'">Editer</a></li>'+
                            '<li><a href="#" class="deleteFiche">Archiver</a></li></ul></div>'
                        ]);

                        var oSettings = $('#tabFiche').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $(nTr).attr('id', data[0].idFiche);                      // Ajout attribut sur la ligne crée
                        $('td', nTr)[4].setAttribute( 'class', 'publishFiche' ); // Ajout d'une classe sur le TD de publier

                    }
                })
            }
        });
    });
    




});
