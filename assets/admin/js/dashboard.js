

// SI DASHBOARD EST PRESENT
if($('.dashboard').length > 0 ){
    
    try{
        var ctx = $("#chartRdvG");
        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: dataRdvG
        });
    }catch(err) {}
    try{
        var ctx = $("#chartActuEtat");
        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: dataTypeActus
        });
    }catch(err) {}    
    try{
        var ctx = $("#chartActuMois");
        var myPieChart = new Chart(ctx,{
            type: 'bar',
            data: dataPubliActus,
            options: {
                scales: {
                     yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true   
                        }
                    }]
                }
            }
        });
    }catch(err) {}    
    try{
        var ctx = $("#chartFicheEtat");
        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: dataTypeFiches
        });
    }catch(err) {} 
    try{
        var ctx = $("#chartFicheMois");
        var myPieChart = new Chart(ctx,{
            type: 'bar',
            data: dataPubliFiches,
            options: {
                scales: {
                     yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true   
                        }
                    }]
                }
            }
        });
    }catch(err) {}   
   try{
        var ctx = $("#chartVideoEtat");
        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: dataTypeVideos
        });
    }catch(err) {} 
    try{
        var ctx = $("#chartVideoMois");
        var myPieChart = new Chart(ctx,{
            type: 'bar',
            data: dataPubliVideos,
            options: {
                scales: {
                     yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true   
                        }
                    }]
                }
            }
        });
    } catch(err) {}   
    if(dataTypeUti != null){
        var ctx = $("#chartTypeUti");
        var myPieChart = new Chart(ctx,{
            type: 'pie',
            data: dataTypeUti,
           
            
        });
    }
    
    
}