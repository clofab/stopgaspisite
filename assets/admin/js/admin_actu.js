$(document).ready(function(){

    /**************************************************************************
     *                              A L'EXECUTION                             *
     *************************************************************************/

    // DATATABLE JS INIT
    $('#tabActu').dataTable( {
        "info":     false,
        "columnDefs": [ 
        { "targets": 0, "orderable": false }, 
        { "targets": 1, "orderable": false },
        { "targets": 5, "orderable": false }
        ],
        "language": langage
    });

    // DATATABLE ARCHIV
    $('#tabArchiveActu').dataTable( {
        "info":     false,
        "columnDefs": [
        { "targets": 0, "orderable": false },
        { "targets": 3, "orderable": false },
        ],
        "language": langage
    });

    var tabActu = $('#tabActu').dataTable();
    $("#searchbox").keyup(function() {
        tabActu.fnFilter(this.value);
    });

    var tabArchiveActu = $('#tabArchiveActu').dataTable();
    $("#searchArchiveActu").keyup(function() {
        tabArchiveActu.fnFilter(this.value);
    });

    /**************************************************************************
     *                             SUPPRESSION                                *
     *************************************************************************/

    //SUPPRIME UNE ACTUALITE
    $(document).on('click','.deleteActu',function(){
        url = urlApp+"/Actualite/deleteActu";
        Suppression.delete(url,$(this))

    });

    // SUPPRIME DES ACTUALITES
    $(document).on('click','.deleteAllActu',function(){
        url = urlApp+"/Actualite/deleteActu";
        Suppression.deleteChecks(".checkActu",url)
    });




    /**************************************************************************
     *                             PUBLICATION                                *
     *************************************************************************/

    // PUBLICATION - DEPUBLICATION D'UNE ACTU
    $(document).on('click','.publishActu .checkbox',function(){
        var url = urlApp+"/Actualite/publishActu";
        checkbox = $(this).find('input');

        if(checkbox.is(":checked")){
            checkbox.prop("checked",false)
        }else{
            checkbox.prop("checked",true)
        }

        Publication.publishOne(url,checkbox)

    });

    // PUBLICATION - DEPUBLICATION PLUSIEURS ACTUS
    $(document).on('click','.publishAllActu',function(){
        var etat = $(this).attr('data-etat');
        var url = urlApp+"/Actualite/publishActu";
        Publication.publishChecks(".checkActu",url,etat)
    });


    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

    // VERIF FORMULAIRE AJOUT ACTUALITE
    $('#formActu').on('submit',function(event){

        var titreActu = Form.verifInput($('[name="titreActu"]'));
        var resumeActu = Form.verifInput($('[name="resumeActu"]'));
        var fileActu = Form.verifInput($('[name="fileActu"]'));
        var descriptionActu = Form.verifInput($('[name="descriptionActu"]'));
        if(fileActu == false && $('.imgEdit').length > 0){
            fileActu = true;
            $('[name="etatImage"]').val(1)
            $('[name="fileActu"]').closest('.form-group').removeClass('has-error')
        }

        // SI FORMULAIRE NON COMPLET ALORS AFFICHE ALERTE
        if(titreActu == false || resumeActu == false || fileActu == false || descriptionActu == false){
            afficheAlert('.pageContaint','danger',"Attention, Formulaire incomplet");
            event.preventDefault();
            return false
        }
    });


    /**************************************************************************
     *                              ARCHIVES                                  *
     *************************************************************************/

    // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#archivActu').on('shown.bs.modal', function (e) {
        var date;
        var typeActu = $('#typeActu').val()

        $.ajax({
            method: "POST",
            url: urlApp+"/Actualite/getArchiveActu",
            dataType:"json",
            data:{typeActu :typeActu },
            success:function(data) {
                $('#tabArchiveActu').dataTable().fnClearTable(); // on vide le modal archives

                for (i = 0; i < data.length; i++) {

                    date = data[i].dateCreation.split("-");
                    date = date[2]+"/"+date[1]+"/"+date[0]

                    var newRow = $('#tabArchiveActu').dataTable().fnAddData([
                        "<img src='" + urlAssets + "/files/images/actu/" + data[i].imageActu + "' width='32' height='auto' />",
                        data[i].titre,
                        date,
                        '<button type="button" class="activActu btn btn-xs">Activer</button>',
                    ]);

                    var oSettings = $('#tabArchiveActu').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idActu); // Ajout attribut sur la ligne crée
                }
            }
        })
    });



    //  RÉ-ACTIVATION D' ARCHIVES
    $(document).on('click','.activActu',function(){

        var table = $(this).closest('table').DataTable();
        var tr = $(this).closest('tr');
        var idActu = parseInt(tr.attr('id'));
        var type = parseInt($('#typeActu').val())
        var urlSaisie


        // SI PAGES OU ACTUS
        if(type == 0){
            urlSaisie = urlApp+"/Actualite/saisieActu/"
        }else{
            urlSaisie = urlApp+"/GestionSite/saisiePage/"
        }

        // UPDATE EN BDD AFIN DE NE PLUS ETRE ARCHIVER
        $.ajax({
            method: "POST",
            url: urlApp+"/Actualite/publishActu",
            dataType:"json",
            data: { id: idActu, etat: 1},
            success:function(){
                table.row(tr).remove().draw(false); // delete row archive

                $.ajax({
                    method: "POST",
                    url: urlApp+"/Actualite/getActu",
                    dataType: "json",
                    data: { idActu: idActu},

                    success: function (data) {

                        date = data[0].dateCreation.split("-");
                        date = date[2]+"/"+date[1]+"/"+date[0]

                        var newRow = $('#tabActu').dataTable().fnAddData([
                            '<input class="checkActu" type="checkbox">',
                            '<img src="' + urlAssets + '/files/images/actu/' + data[0].imageActu + '" width="32" height="auto" />',
                            data[0].titre,
                            date,
                            '<div class="checkbox"><input type="checkbox" class="etatPublish" checked><label></label>',

                            '<div class="btn-group" role="group">'+
                            '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                            '<span class="caret"></span></button>'+
                            '<ul class="pull-right dropdown-menu">'+
                            '<li><a href="'+urlSaisie+data[0].idActu+'">Editer</a></li>'+
                            '<li><a href="#" class="deleteActu">Archiver</a></li></ul></div>'
                        ]);

                        var oSettings = $('#tabActu').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $(nTr).attr('id', data[0].idActu);                      // Ajout attribut sur la ligne crée
                        $('td', nTr)[4].setAttribute( 'class', 'publishActu' ); // Ajout d'une classe sur le TD de publier

                    }
                })

            }
        });

    });

});
