var idModifWeb = 0;

function resetFormulaire(){
    // CHANGEMENT DES BTN MODIFIER ---> AJOUTER
    $('.form-group').removeClass('has-error')
    $("#addUtilisateur").removeClass("hide");
    $("#annulEditUtilisateur").addClass("hide");
    $("#validEditUtilisateur").addClass("hide");

    // REMISE DU FORMULAIRE A ZERO
    $('input').val('');    // remise a zero input
    $('.alert').remove();  // suppression des alerts
    $('#formUserAdmin select').prop('selectedIndex', -1); // select vide
    $('#cadreAnnexe').addClass('hide');

    idModifWeb = 0
}

$(document).ready(function(){


    // DATATABLE JS INIT
    $('#tabUtilisateur').dataTable( {
        "info":     false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "columnDefs": [
            { "targets": 0, "orderable": false },
            { "targets": 3, "orderable": false },
        ],
        "language": langage
    });

    // DATATABLE ARCHIV
    $('#tabArchiveUtilisateur').dataTable( {
        "info":     false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "columnDefs": [
            { "targets": 0, "orderable": false },
            { "targets": 3, "orderable": false },
        ],
        "language": langage
    });
    
    // DATATABLE LISTE ENTREPRISE
    $('#tabEntreprise').dataTable( {
        "info":     false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "columnDefs": [
            { "targets": 6, "orderable": false },
        ],
        "language": langage
    });

    var tabUti = $('#tabUtilisateur').dataTable();
    $("#searchbox").keyup(function() {
        tabUti.fnFilter(this.value);
    });

    var tabArchiveUtilisateur = $('#tabArchiveUtilisateur').dataTable();
    $("#searchArchiveActu").keyup(function() {
        tabArchiveUtilisateur.fnFilter(this.value);
    });
    
    var tabEntreprise = $('#tabEntreprise').dataTable();
    $("#searchEntreprise").keyup(function() {
        tabEntreprise.fnFilter(this.value);
    });

    /**************************************************************************
     *                             SUPPRESSION                                *
     *************************************************************************/

    //  SUPPRIME UN WEBMASTER
    $(document).on('click','.deleteUtilisateur',function(){
        url = urlApp+"/Utilisateurs/deleteUtilisateur";
        Publication.publishOne(url,$(this),true)
    });

    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

    //AJOUT D'UN UTILISATEUR BACK OFFICE
    $(document).on('click','.addUtilisateur',function(){

        var email = Form.verifInput($('#email'));
        var mdp = Form.verifInput($('#mdp'));
        var mdp2 = Form.verifInput($('#mdp2'));
        var nom = Form.verifInput($('#nom'));
        var prenom = Form.verifInput($('#prenom'));
        var imgUtilisateur = Form.verifInput($('#imgUtilisateur'));
        var typeUtilisateur = Form.verifInput($('#typeUtilisateur'));
        var lastImg = $("#tabUtilisateur tr#"+idModifWeb+" td:first img").attr("alt");

        $('#imgUtilisateur').closest('.has-error').removeClass('has-error')
        var modif = false;
        var formData = new FormData();

        // Si on est en edition de webmaster
        if(idModifWeb != 0) {
            modif = true;
            if (imgUtilisateur == false){

                imgUtilisateur = true;
                formData.append('file',true);
            }
            formData.append('idUtilisateur',idModifWeb);
        }

        var tabFormulare = [email, mdp, mdp2,nom, prenom,typeUtilisateur];

        //Si cadre annexe existe, alors
        if(!$("#cadreAnnexe").hasClass("hide")){
            var annexe = Form.verifInput($('#annexe'));
            tabFormulare.push(annexe);
            formData.append('annexe',annexe)
        }

        if(Form.validForm(tabFormulare) == false) return false ;             // verifie formulaire complet
        if(Form.verifEmail($('#email')) == false) return false ;             // verifie email
        
        if(Form.verifMdp($('#mdp'),$('#mdp2')) == false) return false ;      // verifie mdp identique
        
        // verifie longeur mdp
        if(mdp.length < 6){
            $('#mdp').closest('.form-group').addClass('has-error')
            $('#mdp2').closest('.form-group').addClass('has-error')
            afficheAlert('.pageContaint', 'danger', "Attention, votre mot de passe doit contenir 6 caractère minimum");
            event.preventDefault();
            return false
        }else{
            $('#mdp').closest('.form-group').removeClass('has-error')
            $('#mdp2').closest('.form-group').removeClass('has-error')
        }

        if(imgUtilisateur != true && imgUtilisateur != false ){ //  Si une image est présente
            var fileName = imgUtilisateur.substr(imgUtilisateur.lastIndexOf("\\")+1, imgUtilisateur.length);
            var fileToUpload = $('#imgUtilisateur')[0].files[0];
            if(fileToUpload != 'undefined') {
                formData.append('file',fileToUpload)
            }
        }

        if(imgUtilisateur == false ){ // Si pas d'image en création, alors image par default
            formData.append('file',false);
            fileName = 'avatar_default.png'
        }

        formData.append('email',email);
        formData.append('nom',nom);
        formData.append('prenom',prenom);
        formData.append('mdp',mdp);
        formData.append('typeUtilisateur',typeUtilisateur);
        formData.append('lastImg',lastImg);

        //SI PAS D'ERREUR ALORS ON ENVOIE LE FORMULAIRE
        $.ajax({
            method: "POST",
            url:  urlApp+"/Utilisateurs/addUtilisateur",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,

        })
        .done(function(data) {

            idUtilisateur = parseInt(data.replace(/\s+/g, ''));

            if(idUtilisateur == 0){    //Si l'email existe déja
                afficheAlert('.pageContaint','danger','Cette email est déjà utilisé')
                return false
            }

            if(modif == false){ // Si nous sommes en création,

                // SI INSERT
                var newRow = $('#tabUtilisateur').dataTable().fnAddData( [
                    "<img src='"+urlAssets+"/files/images/utilisateur/"+fileName+"' height='35' width='35' />",
                    nom +" "+prenom,
                    $('#typeUtilisateur option:selected').text(),

                    '<div class="btn-group" role="group">'+
                    '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                    '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                    '<span class="caret"></span></button>'+
                    '<ul class="pull-right dropdown-menu">'+
                    '<li><a href="#" class="editUtilisateur">Editer</a></li>'+
                    '<li><a href="#" class="deleteUtilisateur">Archiver</a></li></ul></div>'

                ] );

                // Ajout atribut sur la ligne crée
                var oSettings =  $('#tabUtilisateur').dataTable().fnSettings();
                var nTr = oSettings.aoData[ newRow[0] ].nTr;
                $(nTr).attr('id', idUtilisateur);

            }else{ // Si nous sommes en modification

                var row = $('#tabUtilisateur tr#'+idModifWeb);
                if(imgUtilisateur != true){

                    var newImg = $('#imgUtilisateur')[0].files[0].name;
                    row.find('td:eq(0) img').attr("alt",newImg);
                    row.find('td:eq(0) img').attr("src",urlAssets+"/files/images/utilisateur/"+newImg)
                }
               
              
                row.find('td:eq(1)').text(nom+" "+prenom);
                row.find('td:eq(2)').text($('#typeUtilisateur option:selected').text())
            }

            resetFormulaire()
        });
    });

    //  EDIT UN WEBMASTER --- > ATTENTE --> EN COURS
    $(document).on('click','.editUtilisateur',function(){
        resetFormulaire()
        idModifWeb = $(this).closest('tr').attr('id');
       // var image = $(this).closest('tr').find('tr:eq(0)')

        $.ajax({
            method: "POST",
            url: urlApp+"/Utilisateurs/getUtilisateur",
            dataType:"json",
            data: { id: idModifWeb },
            success:function(data){

                $("#email").val(data[0].emailUtilisateur);
                $("#nom").val(data[0].nomUtilisateur);
                $("#prenom").val(data[0].prenomUtilisateur);
                $("#typeUtilisateur").val(data[0].idTypeUtilisateur);
                $("#mdp").val(data[0].mdpUtilisateur);
                $("#mdp2").val(data[0].mdpUtilisateur);

                if(data[0].idAnnexe != null){
                    $('#cadreAnnexe').removeClass('hide');
                    $('#annexe').val(data[0].idAnnexe)
                }else{
                    $('#cadreAnnexe').addClass('hide')
                }

                // CHANGEMENT DES BTN AJOUTER ---> MODIFIER
                $("#addUtilisateur").addClass("hide");
                $("#annulEditUtilisateur").removeClass("hide");
                $("#validEditUtilisateur").removeClass("hide");

            }
        })
    });

    //  ANNULATION MODIFICATION UTILISATEUR
    $(document).on("click","#annulEditUtilisateur",resetFormulaire);

    //  AU CHANGEMENT DU SELECT TYPE UTILISATEUR
    $(document).on('change','#typeUtilisateur',function(){
        var dataConseiller = parseInt($("#typeUtilisateur option:selected").attr('data-conseiller'));

        // Si conseiller, alors on affiche les annexes
        if(dataConseiller == 1){
            $('#annexe').prop('selectedIndex', -1); // select vide
            $('#cadreAnnexe').removeClass('hide')
        }else{
            $('#cadreAnnexe').addClass('hide')
        }
    })


    /**************************************************************************
     *                      RÉ-ACTIVATION -- ARCHIVES                         *
     *************************************************************************/

    // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#archiveUtilisateur').on('shown.bs.modal', function (e) {

        $.ajax({
            method: "POST",
            url: urlApp+"/Utilisateurs/getArchiveUtilisateur",
            dataType:"json",

            success:function(data) {
                $('#tabArchiveUtilisateur').dataTable().fnClearTable(); // on vide le modal archives

                for (i = 0; i < data.length; i++) {

                    var newRow = $('#tabArchiveUtilisateur').dataTable().fnAddData([
                        '<img src="' + urlAssets + '/files/images/utilisateur/' + data[0].imgUtilisateur + '" height="35" width="35" />',
                        data[i].nomUtilisateur+" "+data[i].prenomUtilisateur,
                        data[i].lblTypeUtilisateur,
                        '<button type="button" class="activUtilisateur btn btn-xs">Activer</button>',
                    ]);

                    var oSettings = $('#tabArchiveUtilisateur').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idUtilisateur); // Ajout attribut sur la ligne crée
                }
            }
        })
    });

    $(document).on('click','.activUtilisateur',function(){
        resetFormulaire()
        var table = $(this).closest('table').DataTable();
        var tr = $(this).closest('tr');
        var idUtilisateur = parseInt(tr.attr('id'));


        // UPDATE EN BDD AFIN DE NE PLUS ETRE ARCHIVER
        $.ajax({
            method: "POST",
            url:urlApp+"/Utilisateurs/publishUtilisateur",
            dataType:"json",
            data: { id: idUtilisateur, etat: 1},
            success:function(){
                table.row(tr).remove().draw(false); // delete row archive

                $.ajax({
                    method: "POST",
                    url: urlApp+"/Utilisateurs/getUtilisateur",
                    dataType: "json",
                    data: { id: idUtilisateur},

                    success: function (data) {

                        var newRow = $('#tabUtilisateur').dataTable().fnAddData([
                            '<img src="' + urlAssets + '/files/images/utilisateur/' + data[0].imgUtilisateur + '" height="35" width="35" />',
                            data[0].nomUtilisateur+" "+data[0].prenomUtilisateur,
                            data[0].lblTypeUtilisateur,

                            '<div class="btn-group" role="group">'+
                            '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                            '<span class="caret"></span></button>'+
                            '<ul class="pull-right dropdown-menu">'+
                            '<li><a href="#" class="editUtilisateur">Editer</a></li>'+
                            '<li><a href="#" class="deleteUtilisateur">Archiver</a></li></ul></div>'
                        ]);

                        var oSettings = $('#tabUtilisateur').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $(nTr).attr('id', data[0].idUtilisateur);                      // Ajout attribut sur la ligne crée

                    }
                })
            }
        });
    });



    /**************************************************************************
     *                           OUVERTURE LISTE ENTREPRISE                   *
     *************************************************************************/

    // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#listeEntreprise').on('shown.bs.modal', function (e) {

        $.ajax({
            method: "POST",
            url: urlApp+"/Utilisateurs/getEntreprise",
            dataType:"json",

            success:function(data) {
                $('#tabEntreprise').dataTable().fnClearTable(); // on vide le modal archives

                for (i = 0; i < data.length; i++) {
                    etatEntreprise = parseInt(data[i].etatEntreprise)
                    
                    switch (etatEntreprise) {
                        case 0:
                             etatEntreprise = "Email non confirmé"
                             btn = ""
                        break;
                        case 1:
                             etatEntreprise = "Activé"
                             btn = '<button type="button" class="activEntreprise btn btn-danger btn-xs">Bannir</button>'
                        break;
                        case 2:
                             etatEntreprise = "Banni"
                             btn = '<button type="button" class="activEntreprise btn btn-success btn-xs">Activer</button>'
                        break;
                    }

                    var newRow = $('#tabEntreprise').dataTable().fnAddData([
                        data[i].nomEntreprise,
                        data[i].interlocuteur,
                        data[i].telEntreprise,
                        data[i].emailEntreprise,
                        data[i].adresseEntreprise,
                        etatEntreprise,
                        btn,
                    ]);

                    var oSettings = $('#tabEntreprise').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idEntreprise); // Ajout attribut sur la ligne crée
                    $(nTr).attr('data-etat', data[i].etatEntreprise); // Ajout attribut sur la ligne crée
                }
            }
        })
    })
    
    /**************************************************************************
     *                       BANNIR/REACTIVE ENTREPRISE                       *
     *************************************************************************/
   
     $(document).on('click','.activEntreprise',function(e){
         
        var idEntreprise = parseInt($(this).closest('tr').attr('id'))  
        var etatEntreprise = parseInt($(this).closest('tr').attr('data-etat'))
        
        if(etatEntreprise == 2){
            var newEtat = 1
            var txtEtat = "Activer"
            btn = '<button type="button" class="activEntreprise btn btn-danger btn-xs">Bannir</button>'

        }else{
           var newEtat = 2
           var txtEtat = "Banni"
            btn = '<button type="button" class="activEntreprise btn btn-success btn-xs">Activer</button>'
        }
       
       $.ajax({
            method: "POST",
            url:urlApp+"/Utilisateurs/updateEtatEntreprise",
            dataType:"json",
            data: { id: idEntreprise, etat: newEtat},
            success:function(){
                
                $("#tabEntreprise tr#"+idEntreprise).attr("data-etat",newEtat)
                $("#tabEntreprise tr#"+idEntreprise+' td:eq(5)').text(txtEtat)
                $("#tabEntreprise tr#"+idEntreprise+" td:eq(6) button").remove()
                $("#tabEntreprise tr#"+idEntreprise+" td:eq(6)").append(btn)

            }
       })
       
     })
    
    /**************************************************************************
     *                           UPDATE PROFIL                               *
     *************************************************************************/


    $(document).on('submit','#formProfilAdmin',function(e){
       $('.alert').remove()
        var error = false

        if(Form.verifMdp($('[name="mdp"]'),$('[name="mdp2"]')) == false)  error = true;      // verifie mdp identique
        if(Form.verifEmail($('[name="email"]')) == false)  error = true;                   // verifie email

        if (error == true){
            e.preventDefault();
        }
    })
    
    
});
