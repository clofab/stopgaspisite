
$(document).ready(function(){

    // QUAND ON VEUX AJOUTER UN SLIDE
    $(document).on('click','#addSlider',function(){

        var titreSlider = Form.verifInput($('#titreSlider'));
        var descriptionSlider = Form.verifInput($('#descriptionSlider'));
        var nomLienSlider = Form.verifInput($('#nomLienSlider'));
        var lienSlider = Form.verifInput($('#lienSlider'));
        var imgSlider = Form.verifInput($('#imgSlider'));
        var nbSlide = parseInt($('.slide').length) + 1 ;

        var tabFormulare = [titreSlider, descriptionSlider, nomLienSlider,lienSlider, imgSlider];

        if(Form.validForm(tabFormulare) == false) return false ;             // verifie formulaire complet
        var formData = new FormData()

        var fileName = imgSlider.substr(imgSlider.lastIndexOf("\\")+1, imgSlider.length);
        var fileToUpload = $('#imgSlider')[0].files[0];
        if(fileToUpload != 'undefined') {
            formData.append('file',fileToUpload)
        }

        formData.append('titreSlider',titreSlider);
        formData.append('descriptionSlider',descriptionSlider);
        formData.append('nomLienSlider',nomLienSlider)                                                                                                          ;
        formData.append('lienSlider',lienSlider);
        formData.append('numSlider',nbSlide);

        //SI PAS D'ERREUR ALORS ON ENVOIE LE FORMULAIRE
        $.ajax({
            method: "POST",
            url:  urlApp+"/Slider/addSlider",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,

        }).done(function(data) {

            var idSlide = parseInt(data)
            $('#msgSlide').removeClass('hide')
            $('.all-slides').append(
                '<li id="'+idSlide+'" class="slide col-md-12 mt10" draggable="true">'+
                    '<div class="col-md-12 row"><div class="col-md-3 row"><img src="'+urlAssets+'/files/images/slider/'+fileName+'" class="col-md-12" width="100%"><button type="button" class="btn btn-danger col-md-12 delSlider">Supprimer</button></div>'+
                    '<div class="col-md-9">'+
                       '<h4>'+titreSlider+'</h4>'+
                       '<p>'+descriptionSlider+'</p>'+
                    '</div></div>'+


                '</li>')
            
            
            
            $('input').val('')
            $('textarea').val('')

        })

    })

    $(function  () {
        $("ol.all-slides").sortable();
    });
    var adjustment;

    $("ol.all-slides").sortable({

        revert       : true,
        connectWith  : ".all-slides",
        // ON ENREGISTRE APRES CHAQUE DRAG AND DROP
        stop         : function(event,ui){

            var nbSlide = $('.all-slides li').length
            var tabSlide = []

            for (i = 0; i < nbSlide ; i++){
                tabSlide[i] = $('.all-slides li:eq('+i+')').attr('id')
            }

            tabSlide = JSON.stringify(tabSlide);
            $.ajax({
                method: "POST",
                url:  urlApp+"/Slider/saveSlider",
                data: {tabSlide: tabSlide},
            })
        }

    });

    // SUPPRESION D'UN SLIDE --> OUVERTURE DU MODAL DE CONFIRMATION
    var liSlide
    $(document).on('click','.delSlider',function(){

        liSlide = $(this).closest('li')
        $('#confirmSuppr').modal('show');
    })

    //  CONFIRMATION DELETE SLIDE
    $(document).on('click','.confirmDelSlide',function(){

        var idSlider = liSlide.attr('id');
        var pathImg = liSlide.find('img').attr('src')

        var fileNameIndex = pathImg.lastIndexOf("/") + 1;
        var nameImg = pathImg.substr(fileNameIndex);

        $.ajax({
            method: "POST",
            url:  urlApp+"/Slider/deleteSlider",
            data: {idSlider: idSlider,nameImg :nameImg},

        }).done(function(data) {
            $('#confirmSuppr').modal('hide');           //  FERMETURE DU MODAL
            $('ol.all-slides li#'+idSlider).remove()    // SUPPRESION LI SLIDE

            var nbSlide = $('.all-slides li').length
            if(nbSlide == 0)  $('#msgSlide').addClass('hide')
            liSlide = ""

        })

    })




})