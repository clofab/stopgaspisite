


$(document).ready(function() {

    /**************************************************************************
     *                              A L'EXECUTION                             *
     *************************************************************************/

        // DATATABLE JS INIT
    $('#tabAnnexe').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 3, "orderable": false }
        ],
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": langage
    });

    // DATATABLE ARCHIV
    $('#tabArchiveAnnexe').dataTable( {
        "info":     false,
        "columnDefs": [
            { "targets": 0, "orderable": false },
            { "targets": 2, "orderable": false },
        ],
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": langage
    });

    var tabAnnexe = $('#tabAnnexe').dataTable();
    $("#searchbox").keyup(function() {
        tabAnnexe.fnFilter(this.value);
    });

    var tabArchiveAnnexe = $('#tabArchiveAnnexe').dataTable();
    $("#searchArchiveActu").keyup(function() {
        tabArchiveAnnexe.fnFilter(this.value);
    });

    //Si nous sommes sur la page création d'annexe
    if($("#needMap").length > 0){
        MapGoogle.innit()
        MapGoogle.autoComplete()
        MapGoogle.addMarker()
    }

    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

    $(document).on('submit','#formAnnexe',function(event){

        var nomAnnexe = Form.verifInput($('#nomAnnexe'))
        var emailAnnexe = Form.verifInput($('#emailAnnexe'))
        var telAnnexe = Form.verifInput($('#telAnnexe'))
        var address = Form.verifInput($('#address'))
        var latitude = Form.verifInput($('#latitude'))
        var longitude = Form.verifInput($('#longitude'))
        var tabFormulare = [nomAnnexe, emailAnnexe, telAnnexe,address];

        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            event.preventDefault();
        }

        if(latitude == false || longitude == false){            // Si long & latitude = Error
            afficheAlert('.pageContaint','danger',"Attention, Adresse Incorrect");
            event.preventDefault();
        }
    })


    /**************************************************************************
     *                             SUPPRIMER ANNEXES                          *
     *************************************************************************/
    $(document).on('click','.deleteAnnexe',function(){
        $('.alert').remove();  // suppression des alerts
        url = urlApp+"/Annexes/deleteAnnexe";
        Publication.publishOne(url,$(this),true)
    });


    /**************************************************************************
     *                      RÉ-ACTIVATION -- ARCHIVES                         *
     *************************************************************************/

        // LORSQUE L'ON OUVRE LE MODAL D'ARCHIVES, ON RECHARGE LES ARCHIVES
    $('#archiveAnnexe').on('shown.bs.modal', function (e) {

        $.ajax({
            method: "POST",
            url: urlApp+"/Annexes/getArchiveAnnexe",
            dataType:"json",

            success:function(data) {
                $('#tabArchiveAnnexe').dataTable().fnClearTable(); // on vide le modal archives

                for (i = 0; i < data.length; i++) {

                    var newRow = $('#tabArchiveAnnexe').dataTable().fnAddData([
                        data[i].nomAnnexe,
                        data[i].adresseAnnexe,
                        '<button type="button" class="activeAnnexe btn btn-xs">Activer</button>',
                    ]);

                    var oSettings = $('#tabArchiveAnnexe').dataTable().fnSettings();
                    var nTr = oSettings.aoData[newRow[0]].nTr;
                    $(nTr).attr('id', data[i].idAnnexe); // Ajout attribut sur la ligne crée
                }
            }
        })
    });

    $(document).on('click','.activeAnnexe',function(){
        var table = $(this).closest('table').DataTable();
        var tr = $(this).closest('tr');
        var idAnnexe = parseInt(tr.attr('id'));


        // UPDATE EN BDD AFIN DE NE PLUS ETRE ARCHIVER
        $.ajax({
            method: "POST",
            url:urlApp+"/Annexes/publishAnnexe",
            dataType:"json",
            data: { id: idAnnexe, etat: 1},
            success:function(){
                table.row(tr).remove().draw(false); // delete row archive

                $.ajax({
                    method: "POST",
                    url: urlApp+"/Annexes/getAnnexe",
                    dataType: "json",
                    data: { id: idAnnexe},

                    success: function (data) {

                        var newRow = $('#tabAnnexe').dataTable().fnAddData([
                            '<h4>'+data[0].nomAnnexe+'</h4>',
                            '<p>'+data[0].adresseAnnexe+'</p>',
                            data[0].telAnnexe,
                            data[0].emailAnnexe,

                            '<div class="btn-group" role="group">'+
                            '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">'+
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Action'+
                            '<span class="caret"></span></button>'+
                            '<ul class="pull-right dropdown-menu">'+
                            '<li><a href="'+urlApp+'/Annexes/saisieAnnexe/'+data[0].idAnnexe+'">Editer</a></li>'+
                            '<li><a href="#" class="deleteAnnexe">Archiver</a></li></ul></div>'
                        ]);

                        var oSettings = $('#tabAnnexe').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $(nTr).attr('id', data[0].idAnnexe);                      // Ajout attribut sur la ligne crée

                    }
                })
            }
        });
    });



});  