$(document).ready(function() {


    /**************************************************************************
     *                             FORMULAIRE                                *
     *************************************************************************/

        // VERIF FORMULAIRE AJOUT ACTUALITE
    $('#formEnergie').on('submit', function (event) {

        var titreEnergie = Form.verifInput($('[name="titreEnergie"]'));
        var accrocheEnergie = Form.verifInput($('[name="accrocheEnergie"]'));
        var descriptionEnergie = Form.verifInput($('[name="descriptionEnergie"]'));
        var contenuEnergie = Form.verifInput($('[name="contenuEnergie"]'));


        var tabFormulare = [titreEnergie, accrocheEnergie, descriptionEnergie,contenuEnergie];

        // SI FORMULAIRE NON COMPLET ALORS AFFICHE ALERTE
        if(Form.validForm(tabFormulare) == false) {              // verifie formulaire complet
            afficheAlert('.pageContaint', 'danger', "Attention, Formulaire incomplet");
            event.preventDefault();
            return false
        }

    });
})